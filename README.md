# Castor Project

The Castor (North American beaver (*Castor canadensis*)) Project is a collection of Beaver Builder modules built specifically for UConn's web environments.  Currently intended for:

* UConn Aurora - [aurora.uconn.edu](http://aurora.uconn.edu/)
* UConn Health Aurora - [health.uconn.edu](http://health.uconn.edu/)
* Communications - communications0.uconn.edu

Beaver Builder custom module documentation can be found [here](https://www.wpbeaverbuilder.com/custom-module-documentation/).

## Development

The project consists of a few fundamental components:

* **Instance** - This is the instance that the WP env is running.  This essentially is a representation of something like "UConn Aurora".
* **Site** - Self explanatory, but essentially every instance is responsible for one or more sites that can enable modules on a per-site basis.
* **Module** - A beaver builder module.  Each one is given its own directory and registered through the castor register functions.

**IMPORTANT FOR MULTISITE:**

If you want to test multisite, I highly recommend creating another docker-compose file for a separate database.  You will also need to do a few other things to get multisite to work:

* `.htaccess` is different for multisite and single site.  The htaccess is currently configured for single site.  Take a look at the .htaccess in the `www` folder.  Uncomment the bottom section if you are running multisite.  You will also need to edit `www/wp-config.php` to include multisite options.  They are already set for you, you just need to uncomment them.

## Register a Custom Beaver Builder Module

All beaver modules are placed in the `modules/` directory.  A good example is the `random-hero-image` module.  It is set up exactly how beaver builder instructs on their documentation, with a `RandomHeroImage` class in `random-hero-image.php`. Beaver Builder gets access to this class through the custom module class `RandomHeroImageModule` in `module.php`. **You must define a `module.php` file, and a module class of some sort. The names of the two files must be different. The module class must implement the `load_module()` function to register your new module with beaver builder.**

Take a look at `includes/class-castor.php`.  

In the function `load_dependencies()`, we automatically load all module files.

There is a function called `define_castor_modules()`.  In this function, you must register your new function, by calling a new instance of the module (in this case `RandomHeroImageModule`), and registering it with castor.

## Usage
### To get a project running.
```bash
$ composer install # first time only
$ docker-compose up
```
### Accessing containers
To access a particular docker container, find the container name and then enter an interactive terminal.
```bash
$ docker ps # to get the container name
$ docker exec -it container_name /bin/bash
```
### Local development with gulp
If you need to use gulp as part of your development, simply uncomment the appropriate image in `docker-compose` and it will run the included gulpfile automatically.
### Debugging Wordpress
Wordpress debug logs can be found inside the web container at `/etc/httpd/logs/error_log`

## Bitbucket
### Creating releases
Assuming you're using git flow, tag the release with the command `git flow release start {version_number}`. Tags must follow the [semver system](http://semver.org/). Follow these steps to complete a release
```bash
git tag # check the current tags/versions on the project
git flow release start {new_tag}
git flow release finish {new_tag}
# in the first screen add new info or just save the commit message as is
# in the second screen type in the same tag you just used and save.
git push --tags && git push origin master
git checkout develop
``` 
Finally re-run the pipeline build on the [satis repo](https://bitbucket.org/ucomm/composer-repository).
