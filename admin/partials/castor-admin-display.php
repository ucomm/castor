<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://uconn.edu
 * @since      1.0.0
 *
 * @package    Castor
 * @subpackage Castor/admin/partials
 */
