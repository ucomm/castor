<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://uconn.edu
 * @since      1.0.0
 *
 * @package    Castor
 * @subpackage Castor/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Castor
 * @subpackage Castor/admin
 * @author     Brian Kelleher <bk@uconn.edu>
 */
class Castor_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The castor instance belonging to the admin.
	 *
	 * @since	1.0.0
	 * @access	private
	 * @var		Castor_Instance	$castor		The castor instance holding all registered modules.
	 */
	private $castor;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Castor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Castor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/castor-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Castor_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Castor_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/castor-admin.js', array( 'jquery' ), $this->version, false );

	}

	public static function get_settings_page() {
		// include_once(CASTOR_DIR . 'admin/partials/castor-admin-display.php');
	}

	public function register_settings_page() {
		// add_submenu_page(
		// 	'options-general.php', 
		// 	'Castor Settings', 
		// 	'Castor Settings', 
		// 	'manage_options', 
		// 	'castor-settings', 
		// 	array($this, 'get_settings_page')
		// );
	}
}
