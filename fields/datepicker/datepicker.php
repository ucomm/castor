<?php

function castor_datepicker_field($name, $value, $field) {
  echo "<input type='date' class='" . $field['type'] . "' name='" . $name . "' value='" . $value . "' />";
}
add_action('fl_builder_control_castor-datepicker', 'castor_datepicker_field', 1, 3);