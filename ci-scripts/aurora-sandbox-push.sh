#!/bin/bash

user="${AURORA_DEV_USER}"
pass="${AURORA_DEV_PASSWORD}"
host="${AURORA_DEV_HOST}"
remotepath="${AURORA_DEV_PLUGINS_DIR}/${PLUGIN_SLUG}"

lftp sftp://$user:$pass@$host:/$remotepath -e "mirror -Rv --exclude .entrypoint --exclude .vscode --exclude ci-scripts --exclude .git --exclude dev-build --exclude node_modules --exclude www --exclude vendor --delete ./ ;bye" -p 22 22