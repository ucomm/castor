#!/bin/bash

source ./plugin-paths.sh

for i in "${staging_plugin_paths[@]}"
do
	echo "===== Starting rsync to $i ====="

  #   check if PLUGIN_SLUG is non-null/non-zero str && found in dir
  if [ ! -n "$PLUGIN_SLUG" ] || [ ! `ssh deploy@${COMM_STAGING_SERVER} test -d "$i/$PLUGIN_SLUG" && echo exists` ]; then
      # raise error without doing damage
      echo "failure - PROJECT_SLUG unviable"
      exit 1
  fi  

  # oherwise stage path
  rsync -avzO -e ssh --chmod=ugo=rwX --no-perms \
    --exclude "plugin-paths.sh" \
	  --exclude ".git" \
    --delete ./ deploy@${COMM_STAGING_SERVER}:"$i/$PLUGIN_SLUG"
  echo "===== Ending rsync to $i ====="
done