#!/bin/bash

source ./plugin_paths.sh

DRY_RUN=--dry-run

if [ "$RSYNC_DRY_RUN" = "false" ]
  then DRY_RUN=""
fi

for i in "${prod_plugin_paths[@]}"
do
	echo "===== Starting rsync to $i ====="

  #   check if PLUGIN_SLUG is non-null/non-zero str && found in dir
  if [ ! -n "$PLUGIN_SLUG" ] || [ ! `ssh deploy@${COMM_PRODUCTION_SERVER} test -d $"$i/$PLUGIN_SLUG" && echo exists` ]; then
      # raise error without doing damage
      echo "failure - PROJECT_SLUG unviable"
      exit 1
  fi

  # set dry run flags
  if [ "$i" = "$admissions_plugin_path" ]
    then if [ "$ADMISSIONS_RSYNC_DRY_RUN" = "false" ]
      then DRY_RUN=""
      else DRY_RUN=--dry-run
  fi
  elif [ "$RSYNC_DRY_RUN" = "false" ] 
    then DRY_RUN=""
    else DRY_RUN=--dry-run
  fi

  # otherwise prod path
  rsync $DRY_RUN -avzO -e ssh --chmod=ugo=rwX --no-perms \
    --exclude "plugin-paths.sh" \
	  --exclude ".git" \
    --delete ./ deploy@${COMM_PRODUCTION_SERVER}:"$i/$PLUGIN_SLUG"
  echo "===== Ending rsync to $i ====="
done
