# Changelog
## 1.9.1
- added transient/caching to resolve php error that would occur when too many API calls were made in a short amount of time ()
- also fixed bug where "All day" events were showing a 12:00AM start time (as opposed to ALL DAY start time)
## 1.9.0
- modified plugin to account for LW events feed(s)
## 1.8.2
- Deprecated the uc-people module in favor of the `uc-people-compat` plugin
## 1.8.1
- Updated people module to use db prefixes for multisite compatibility.
## 1.8.0
- Added people plugin shortcode compatibility module for uconn-2019 theme.
## 1.7.1
- added customizations to content with overlay module to allow for background color underneath text, in addition to other elementsg
## 1.7.0
- Removed bootstrap dependency from calendar module
## 1.6.2
- Search module was namespaced, as search.php is a reserved module in Beaver Builder.
## 1.6.1
- Updated content with overlay module to handle more advanced text formatting options
## 1.6.0
- Removed find a provider module and admin settings page.
- Added a compressed sidebar menu option
## 1.5.3
- Added "all" counties option to UCToday county request
## 1.5.2
- Added support for UCToday county feeds
- Fixed style bug in sidebar nav menu
## 1.5.1
- Addressed permissions issue with sidebar nav
- Tweaked style for circles
## 1.5.0
- Re-built deadline callout module
- Re-built sidebar menu module
## 1.4.0
- Re-built circular callout module
- Updated composer deps
- Added volume to docker-compose.yml bc I hate doing `docker-compose down` and losing the db (AB)
## 1.3.3
- Updated find a provider sdk
## 1.3.2
- Updated ics-parser and set WP core to @stable
- Added ability to clear find a provider cache from a settings page.
## 1.3.1
- Fixed style bug that was interfering with the UConn banner in the content with overlay module.
- Deprecated health blog module
## 1.3.0
- Significant updates to the content with overlay module
- Updated uhealth-provider-sdk to 1.0.10
- Added fuzzy searching to the search module
- Other tweaks and improvements
## 1.2.9
- Fixed custom text module so that it handles margins/paddings on individual texts correctly
## 1.2.8
- Updated ICS parser dependency
- Removed explicit require of vendor/autoload from calendar module
- Changed default font of custom text module
## 1.2.7
- Added fallback image for health blog module
- Fixed finding `vendor/autoload.php` to avoid conflicts with other dependencies
- Fixed content-with-overlay module to have focus effect
## 1.2.6
- Added syntax highlighter module
## 1.2.5
- Remove unnecessary code from find-provider module, and specialties use the SDK.
## 1.2.4
- Added UConnHealth SDK to the find-provider module, updated dependencies for testing.
## 1.2.3
- Bug fix for title attribute of breadcrumbs on pages.
## 1.2.2
- Changed the breadcrumb module
  - Fixed markup to be more semantic
  - Added improvements to accessibility
  - Updated module styles
## 1.2.1
- Updated typography of 
  - calendar module to handle styles on health site
  - find a provider styles. same reason...
- Added controls for the find a provider module, to hide certain elements if needed.
- Removed global styles manager class. The lobo theme will control footer images from now on.
- Fixed deadline callout so that it displays the correct abbreviations.
## 1.2.0
- Added support for the new combined zoned posts/school & college posts uctoday endpoint.
- Added new custom text module.
## 1.1.18
- Changed style of overlay module to hide text decoration
- Changed the display name of the physician news module
- Added readme to the physician news module.
## 1.1.17
- Added a category option to the physician news module to encompass all categories.
## 1.1.16
- Style updates to several modules.
- Updates to find a provider
## 1.1.15
- Added template support for calendar module
- Fixed style issue for sidebar nav module
- Added breadcrumbs and search modules
## 1.1.10
- Added Physician News module
- Updated Find a Provider and sidebar nav modules
## 1.1.9
Added fields to override/omit the displayed specialty title for find a provider module
## 1.1.4
- Added support for fetching UConn Today posts by: author NetID, campus location, and relevanssi search terms
## 1.1.0
- Added custom number input field
- Added support for fetching UConn Today posts by tag ID
- Added support for fetching calendar events by ID
- Added table module
## 1.0.35
- Addressed issue with calendar. It will now get a range of dates selected by the user.
- Added a datepicker input
- Fixed style bug in deadline callout module.
- Switched jQuery from `$` to `jQuery` in the sidebar nav module to handle an error in production.
## 1.0.34
- Reverted a small part of the fix involving padding for the Firefox random-hero-image issue.
## 1.0.33
- Composer archive works
- Fixed an issue where Firefox had inconsistent rendering of the random-hero-image module.
## 1.0.32
- Falling back to use zip instead of composer archive for bundling.
## 1.0.31
- Fixed a bug in where background colors in the footer controlled by the global style manager weren't being updated.
- Added Lobo as a dev dependency and mapped the vendor folder accordingly.
- Brought this project in line with boilerplate v0.1.0
## 1.0.30
- Added an icon repeater module
- Set the version for ICA Parser dependency at 2.0.6