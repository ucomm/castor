
<?php

$has_row_label = CastorTableModule::has_row_label($settings);

$has_headers = CastorTableModule::has_headers($settings);

$table_classes = "table";

if (filter_var($settings->table_border, FILTER_VALIDATE_BOOLEAN)) {
	$table_classes .= " table-bordered";
}

if (filter_var($settings->table_striped, FILTER_VALIDATE_BOOLEAN)) {
	$table_classes .= " table-striped";
}

?>

<div class="table-wrapper">
	<div class="table-responsive">
		<table class="<?php echo $table_classes; ?>">
		<?php
			// if headings exist, create a <thead> with headers for each column.
			if ($has_headers) {
		?>
			<thead>
				<?php
					foreach ($settings->headers as $header) {
				?>
						<th><?php echo $header; ?></th>
				<?php
					}
				?>
			</thead>
		<?php
			}
		?>
			<tbody>
				<?php
					foreach ($settings->rows as $row) {
				?>
						<tr>
							<?php 
								// create row labels as needed
								if ($has_row_label) {
							?>
									<th scope="row"><?php echo $row->row_label; ?></th>
							<?php
								}
								// create the cells for the table
								foreach ($row->row_cells as $row_cell) {
							?>
									<td><?php echo $row_cell; ?></td>
							<?php
								}
							?>
						</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>