<?php

$is_fixed_width_table = filter_var($settings->cell_fixed_width, FILTER_VALIDATE_BOOLEAN);
$has_bootstrap = filter_var($settings->bootstrap_select, FILTER_VALIDATE_BOOLEAN);

// handle border colors
$header_border = "table.table>thead>tr>th";

$table_borders = ".table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td";

if (filter_var($settings->table_border, FILTER_VALIDATE_BOOLEAN)) {
	echo $header_border . " { border-bottom: 2px solid #" . $settings->table_border_color . "; }";
	echo $table_borders . " { border: 1px solid #" . $settings->table_border_color . "; }";
} else {
	echo $header_border . " { border-bottom: 2px solid transparent; }";
	echo $table_borders . " { border: 1px solid transparent; }";
}
?>

<?php
// handle table stripes
if (filter_var($settings->table_striped, FILTER_VALIDATE_BOOLEAN)) {
?>
	.table-striped>tbody>tr:nth-of-type(odd) {
	background-color: #<?php echo $settings->table_striped_color; ?>;
	}
<?php
}
?>

.table-wrapper table {
	border-collapse: collapse;
<?php if (!empty($settings->table_background_color)) : ?>
	background-color:#<?php echo $settings->table_background_color; ?>;
<?php endif;
if ($is_fixed_width_table) : ?>
	table-layout:fixed;
<?php endif; ?>
	width: 100%;
}

.table-wrapper th {
<?php if (!empty($settings->header_background_color)) : ?>
	background-color:#<?php echo $settings->header_background_color; ?>;
<?php endif; ?>
}

.table-wrapper td {
	min-width: <?php echo $settings->cell_min_width . $settings->cell_min_width_unit; ?>;
	padding-top: <?php echo $settings->cell_padding_top . $settings->cell_padding_unit; ?>;
	padding-right: <?php echo $settings->cell_padding_right . $settings->cell_padding_unit; ?>;
	padding-bottom: <?php echo $settings->cell_padding_bottom . $settings->cell_padding_unit; ?>;
	padding-left: <?php echo $settings->cell_padding_left . $settings->cell_padding_unit; ?>;
	vertical-align: text-top;
	word-break: break-word;
}

.table-wrapper th {
	min-width: <?php echo $settings->cell_min_width . $settings->cell_min_width_unit; ?>;
	padding-top: <?php echo $settings->header_padding_top . $settings->header_padding_unit; ?>;
	padding-right: <?php echo $settings->header_padding_right . $settings->header_padding_unit; ?>;
	padding-bottom: <?php echo $settings->header_padding_bottom . $settings->header_padding_unit; ?>;
	padding-left: <?php echo $settings->header_padding_left . $settings->header_padding_unit; ?>;
	text-align: left;
	vertical-align: text-top;
}

<?php
if (!$has_bootstrap) {
?>
	.table-responsive {
	overflow-x: auto;
	}
<?php
}

if ($is_fixed_width_table) {
?>
	@media screen and (max-width: 767px) {
	div.table-responsive>.table>thead>tr>th, div.table-responsive>.table>tbody>tr>th, div.table-responsive>.table>tfoot>tr>th, div.table-responsive>.table>thead>tr>td, div.table-responsive>.table>tbody>tr>td, div.table-responsive>.table>tfoot>tr>td {
			white-space: normal;
		}
	}
<?php
}
