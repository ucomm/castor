<?php

class Table extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Table', 'castor' ),
            'description'     => __( 'Display an table.', 'castor' ),
            'category'        => __( 'Castor Modules', 'castor' ),
            'dir'             => CASTOR_DIR . 'modules/table/',
            'url'             => CASTOR_URL . 'modules/table/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}