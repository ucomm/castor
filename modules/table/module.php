<?php

class CastorTableModule extends Castor_Module {

    private $version = '1.0.1';

    public function load_module() {
		
        if (class_exists('FLBuilder')){
			
            require_once 'table.php';
			
            FLBuilder::register_module('Table', array(
                'general' => array(
                    'title' => __('General Settings', 'castor'),
                    'sections' => array(
                        'bootstrap' => array(
                            'title' => __('Bootstrap CSS', 'castor'),
                            'fields' => array(
                                'bootstrap_select' => array(
                                    'type' => 'select',
                                    'label' => __('Use Bootstrap?', 'castor'),
                                    'default' => 'true',
                                    'options' => array(
                                        'true' => __('Yes', 'castor'),
                                        'false' => __('No', 'castor')
                                    )
                                )
                            )
                        ),
                        'table_styles' => array(
                            'title' => __('Table Styles', 'castor'),
                            'fields' => array(
                                'table_background' => array(
                                    'type' => 'select',
                                    'label' => __('Use Table Background Color?', 'castor'),
                                    'default' => 'false',
                                    'options' => array(
                                        'true' => __('Yes', 'castor'),
                                        'false' => __('No', 'castor')
                                    ),
                                    'toggle' => array(
                                        'true' => array(
                                            'fields' => array('table_background_color')
                                        )
                                    )
                                ),
                                'table_background_color' => array(
                                    'type' => 'color',
                                    'label' => __('Table Background Color', 'castor'),
                                    'show_reset' => true,
                                    'show_alpha' => true
                                ),
                                'table_border' => array(
                                    'type' => 'select',
                                    'label' => __('Show Table Border?', 'castor'),
                                    'default' => 'true',
                                    'options' => array(
                                        'true' => __('Yes', 'castor'),
                                        'false' => __('No', 'castor')
                                    ),
                                    'toggle' => array(
                                        'true' => array(
                                            'fields' => array( 'table_border_color' )
                                        )
                                    )
                                ),
                                'table_border_color' => array(
                                    'type' => 'color',
                                    'label' => __('Table Border Color', 'castor'),
                                    'default' => 'dddddd',
                                    'show_reset' => true,
                                    'show_alpha' => true
                                ),
                                'table_striped' => array(
                                    'type' => 'select',
                                    'label' => __('Table Striped', 'castor'),
                                    'default' => 'true',
                                    'options' => array(
                                        'true' => __('Yes', 'castor'),
                                        'false' => __('No', 'castor')
                                    ),
                                    'toggle' => array(
                                        'true' => array(
                                            'fields' => array('table_striped_color')
                                        )
                                    )
                                ),
                                'table_striped_color' => array(
                                    'type' => 'color',
                                    'label' => __('Table Stripe Color', 'castor'),
                                    'default' => 'f9f9f9',
                                    'show_reset' => true,
                                    'show_alpha' => true
                                )
                            )
                        ),
                        'header_styles' => array(
                            'title' => __('Header Styles', 'castor'),
                            'fields' => array(
                                'header_background' => array(
                                    'type' => 'select',
                                    'label' => __('Use Header Background Color?', 'castor'),
                                    'default' => 'false',
                                    'options' => array(
                                        'true' => __('Yes', 'castor'),
                                        'false' => __('No', 'castor')
                                    ),
                                    'toggle' => array(
                                        'true' => array(
                                            'fields' => array('header_background_color')
                                        )
                                    )
                                ),
                                'header_background_color' => array(
                                    'type' => 'color',
                                    'label' => __('Table Header Color', 'castor'),
                                    'show_reset' => true,
                                    'show_alpha' => true
                                ),
                                'header_padding' => array(
                                    'type' => 'dimension',
                                    'label' => __('Header Padding', 'castor'),
                                    'default' => 1,
                                    'slider' => array(
                                        'px' => array(
                                            'min' => 0,
                                            'max' => 32,
                                            'step' => 1
                                        ),
                                        'rem' => array(
                                            'min' => 0,
                                            'max' => 3,
                                            'step' => 0.1
                                        )
                                    ),
                                    'units' => array('rem', 'px'),
                                    'default_unit' => 'rem'
                                ),
                            )
                        ),
                        'cells' => array(
                            'title' => __('Cells', 'castor'),
                            'fields' => array(
                                'cell_fixed_width' => array(
                                    'type' => 'select',
                                    'label' => __('Cell Fixed Width', 'castor'),
									'options' => array(
										'Yes' => 'Yes',
										'No' => 'No'
									),
									'default' => 'Yes'
                                ),
                                'cell_min_width' => array(
                                    'type' => 'unit',
                                    'label' => __('Minimum Cell Width', 'castor'),
                                    'default' => 200,
                                    'slider' => array(
                                        'px' => array(
                                            'min' => 0,
                                            'max' => 500,
                                            'step' => 10
                                        ),
                                        'rem' => array(
                                            'min' => 0,
                                            'max' => 32,
                                            'step' => 1
                                        )
                                    ),
                                    'units' => array( 'px', 'rem' ),
                                    'default_unit' => 'px'
                                ),
                                'cell_padding' => array(
                                    'type' => 'dimension',
                                    'label' => __('Cell Padding', 'castor'),
                                    'default' => 1,
                                    'slider' => array(
                                        'px' => array(
                                            'min' => 0,
                                            'max' => 32,
                                            'step' => 1
                                        ),
                                        'rem' => array(
                                            'min' => 0,
                                            'max' => 3,
                                            'step' => 0.1
                                        )
                                    ),
                                    'units' => array( 'rem', 'px' ),
                                    'default_unit' => 'rem'
                                ),
                            )
                        )
                    )
                ),
                'headers' => array(
                    'title' => __( 'Table Headers', 'castor' ),
                    'sections' => array(
                        'headers' => array(
                            'title' => __( 'Column Headers', 'castor' ),
                            'fields' => array(
                                'headers' => array(
                                    'type' => 'text',
                                    'label' => __('Header', 'castor'),
                                    'multiple' => true
                                )
                            )
                        )
                    )
                ),
                'rows' => array(
                    'title' => __( 'Table Rows', 'castor' ),
                    'sections' => array(
                        'rows' => array(
                            'title' => __( 'Row Cells', 'castor' ),
                            'fields' => array(
                                'rows' => array(
                                    'type' => 'form',
                                    'label' => __('Row', 'castor'),
									'form'	=> 'row_form',
									'preview_text' => 'label',
                                    'multiple' => true
                                )
                            )
                        )
                    )
                ),
            ));
			
			FLBuilder::register_settings_form('row_form', array(
                'title' => __('Row Settings', 'castor'),
                'tabs' => array(
                    'rows' => array(
                        'title' => __('Row', 'castor'),
                        'sections' => array(
                            'row' => array(
                                'fields' => array(
                                    'row_label' => array(
                                        'type' => 'text',
                                        'label' => __('Row Label', 'castor')
                                    ),
                                    'row_cells' => array(
                                        'type' => 'textarea',
                                        'label' => __('Cell', 'castor'),
										'rows' => 3,
										'multiple' => true
                                    )
                                )
                            )
                        )
                    )
                )
            ));

        }
    }
	
	public static function has_row_label($settings){
	
		if (!empty($settings->rows)){

			foreach ($settings->rows as $row){

				if (!empty($row->row_label)) return true;

			}

			return false;

		}
	
    }
    
    public static function has_headers($settings) {
        $has_headers = false;
        $filtered = array_filter($settings->headers, function($header) {
            $prepared_header = trim($header);
            if (strlen($prepared_header) > 0) {
                return true;
            }
        });
        if (count($filtered) > 0) {
            $has_headers = true;
        }
        return $has_headers;
    }

	
}
