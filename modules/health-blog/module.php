<?php
/*
*
*   THIS MODULE IS DEPRECATED!
*   Keeping this code for archival purposes only.  
*
*
 */

class HealthBlogModule extends Castor_Module {
  public function load_module() {
    if (class_exists('FLBuilder')) {
      require_once 'health-blog.php';
      FLBuilder::register_module('HealthBlog', array(
        'tab_1' => array(
          'title' => __('Temp', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Temp', 'castor'),
              'fields' => array(
                'post_url' => array(
                  'type' => 'text',
                  'label' => __('Enter the URL of the post', 'castor')
                ),
                'embed' => array(
                  'type' => 'select',
                  'label' => __('Include featured images?'),
                  'default' => 'true',
                  'options' => array(
                    'true' => __('Yes', 'castor'),
                    'false' => __('No', 'castor')
                  )
                )
              )
            )
          )
        )
      ));
    }
  }
}
