<?php
$opts = array(
  'post_url' => $settings->post_url,
  'embed' => $settings->embed
);
$articles = $module->get_articles($opts);

foreach ($articles as $index => $article) {
  $title = $article['title']['rendered'];
  $excerpt = $article['excerpt']['rendered'];
  $link = $article['link'];
  $featured_image = null;
?>
  <div class="hb-container">
    <h3 class="hb-link_header">
      <a class="hb-link" href="<?php echo $link; ?>">
        <?php echo $title; ?>
      </a>
    </h3>
    <?php
    if (!isset($article['_embedded']['wp:featuredmedia'][0]['media_details']['sizes']['medium_large']['source_url'])) {
      $featured_image = $module->get_placeholder_image();
    } else {
      $featured_image = $article['_embedded']['wp:featuredmedia'][0]['media_details']['sizes']['medium_large']['source_url'];
    }
    ?>
    <div aria-hidden="true" class="hb-image_container" 
      style="background-image: url(<?php echo $featured_image; ?>);">
    </div>
    <div class="hb-excerpt_container">
      <?php echo $excerpt; ?>
      <p>
        <a class="hb-link_text" href="<?php echo $link; ?>" 
          aria-label="Read more about <?php echo $title; ?>">
          Read More
        </a>
      </p>
    </div>
  </div>
<?php
}
?>

