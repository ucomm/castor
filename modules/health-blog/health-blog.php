<?php

class HealthBlog extends FLBuilderModule {
  public function __construct() {
    parent::__construct(array(
      'name' => __('Health Blog', 'castor'),
      'description' => __('Display the first post from health.uconn.edu/health-blog', 'castor'),
      'category' => __('Castor Modules', 'castor'),
      'dir' => CASTOR_DIR . 'modules/health-blog/',
      'url' => CASTOR_URL . 'modules/health-blog/',
      'editor_export'   => true, // Defaults to true and can be omitted.
      'enabled'         => true, // Defaults to true and can be omitted.
      'partial_refresh' => false, // Defaults to false and can be omitted.
    ));
  }
  public static function get_articles($opts = array(
    'post_url' => '',
    'embed' => true
  )) {
    $embed = filter_var($opts['embed'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

    $slug_arr = explode('/', $opts['post_url']);

    // break the entered url into pieces and make sure the slug is the last one.
    if ($slug_arr[count($slug_arr) - 1] === '') {
      array_pop($slug_arr);
    }

    $slug = $slug_arr[count($slug_arr) - 1];

    $url = "https://health.uconn.edu/health-blog/wp-json/wp/v2/posts?slug=$slug";

    if ($embed) {
      $url .= "&_embed";
    }

    $response = wp_remote_get($url);
    $data = json_decode(wp_remote_retrieve_body($response), true);
    return $data;
  }
  public static function get_placeholder_image() {
    return CASTOR_URL . 'modules/health-blog/images/uc-health-placeholder.jpg';
  }
}