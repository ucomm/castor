<?php

class RandomHeroImage extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Random Hero Image', 'castor' ),
            'description'     => __( 'Display the UConn Health main site hero image backgrounds.', 'castor' ),
            'category'        => __( 'Castor Modules', 'castor' ),
            'dir'             => CASTOR_DIR . 'modules/random-hero-image/',
            'url'             => CASTOR_URL . 'modules/random-hero-image/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}