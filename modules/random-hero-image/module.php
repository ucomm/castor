<?php

class RandomHeroImageModule extends Castor_Module {

    private $version = '1.0.1';

    public function load_module() {
        if ( class_exists( 'FLBuilder' ) ) {
            require_once 'random-hero-image.php';
            FLBuilder::register_module('RandomHeroImage', array(
                'slides' => array(
                    'title' => __( 'Slides', 'castor' ),
                    'sections' => array(
                        'slides' => array(
                            'title' => __( 'Slides', 'castor' ),
                            'fields' => array(
                                'slides' => array(
                                    'type' => 'form',
                                    'label' => __('Slide', 'castor'),
                                    'form' => 'uhslider_slide_form_1',
                                    'preview_text' => 'content',
                                    'multiple' => true
                                )
                            )
                        )
                    )
                )
            ));


            FLBuilder::register_settings_form('uhslider_slide_form_1', array(
                'title' => __('Edit Slide', 'castor'),
                'tabs' => array(
                    'general' => array(
                        'title' => __('General', 'castor'),
                        'sections' => array(
                            'content' => array(
                                'title' => __('Content', 'castor'),
                                'fields' => array(
                                    'content' => array(
                                        'type' => 'textarea',
                                        'label' => __('Content', 'castor'),
                                        'default' => 'Medicine doesn\'t change until we change it.',
                                        'rows' => '3'
                                    ),
                                    'content_font_size' => array(
                                        'type' => 'unit',
                                        'label' => __('Font Size', 'castor'),
                                        'default' => '32',
                                        'description' => 'px'
                                    ),
                                    'content_text_color' => array(
                                        'type' => 'color',
                                        'label' => __('Content Text Color', 'castor'),
                                        'default' => 'ffffff',
                                        'show_reset' => true
                                    ),
                                    'content_bg_color' => array(
                                        'type' => 'color',
                                        'label' => __('Content Background Color', 'castor'),
                                        'default' => '41bdb2',
                                        'show_reset' => true
                                    ),
                                    'content_placement' => array(
                                        'type' => 'select',
                                        'label' => __('Content Placement', 'castor'),
                                        'default' => 'bottom-left',
                                        'options' => array(
                                            'top-left' => __('Top Left', 'castor'),
                                            'top-center' => __('Top Center', 'castor'),
                                            'top-right' => __('Top Right', 'castor'),
                                            'bottom-left' => __('Bottom Left', 'castor'),
                                            'bottom-center' => __('Bottom Center', 'castor'),
                                            'bottom-right' => __('Bottom Right', 'castor')
                                        )
                                    )
                                )
                            ),
                            'background' => array(
                                'title' => __('Background Image', 'castor'),
                                'fields' => array(
                                    'background_photo' => array(
                                        'type' => 'photo',
                                        'label' => __( 'Background Photo', 'castor' ),
                                        'show_remove' => true
                                    ),
                                    'background_height' => array(
                                        'type' => 'text',
                                        'label' => __( 'Background Height', 'castor' ),
                                        'show_remove' => true
                                    ),
                                )
                            )
                        )
                    )
                )
            ));
        }
    }
}