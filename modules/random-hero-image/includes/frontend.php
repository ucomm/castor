<?php if ( !empty($settings->slides) ) {
    foreach ( $settings->slides as $i => $s ) {     
    ?>
        <div
            id="<?php echo $settings->type; ?>-element-<?php echo $i; ?>"
            class="<?php echo $settings->type; ?>-element"
            style="display: none;"
            data-background-photo-src="<?php echo $s->background_photo_src; ?>"
            data-background-height="<?php echo $s->background_height; ?>"
            data-content-bg-color="<?php echo $s->content_bg_color; ?>"
            data-content-text-color="<?php echo $s->content_text_color; ?>"
            data-content-placement="<?php echo $s->content_placement; ?>"
            data-content="<?php echo $s->content; ?>"
            data-font-size="<?php echo $s->content_font_size; ?>"
        ></div>
    <?php } ?>
    <div class="<?php echo $settings->type; ?>-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <p></p>
                </div>
            </div>
        </div>
    </div>
<?php } else { 
    if ( is_user_logged_in() ) {
        _e('<p>No valid slides set.</p>', 'fl-builder');
        if ( current_user_can('edit_pages') ) {
            _e( '<p>You can edit the slides in the beaver builder module, by clicking "Page Builder" at the top of this page.</p>', 'fl-builder' ); 
        }
    }
}
?>