/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function UHSlide(photo, bgheight, bgcolor, color, placement, content, fontSize) {
    this.photo = photo;
    this.bgheight = bgheight;
    this.bgcolor = bgcolor;
    this.color = color;
    this.placement = placement;
    this.content = content;
    this.fontSize = fontSize;
}

var random_index = getRandomInt(0, <?php echo count($settings->slides) - 1; ?>);
var slide_div = jQuery("#<?php echo $settings->type; ?>-element-" + random_index);
var slide = new UHSlide(
    slide_div.data("background-photo-src"),
    slide_div.data("background-height"),
    slide_div.data("content-bg-color"),
    slide_div.data("content-text-color"),
    slide_div.data("content-placement"),
    slide_div.data("content"),
    slide_div.data("font-size")
);
var slide_wrapper = jQuery(".<?php echo $settings->type; ?>-wrapper");
var slide_container = slide_wrapper.find(".container");
var slide_row = slide_wrapper.find(".row");
var slide_column = slide_wrapper.find(".col-xs-12");
var slide_text = slide_wrapper.find("p");

slide_wrapper.css('background-image', 'url("' + slide.photo + '")');

if (slide.bgheight){
	slide_wrapper.css('height', slide.bgheight);
}

if (jQuery.inArray(slide.placement, ['top-left', 'top-center', 'top-right']) != -1){
	slide_container.addClass('top');
}

if (jQuery.inArray(slide.placement, ['bottom-left', 'bottom-center', 'bottom-right']) != -1){
	slide_container.addClass('bottom');
}

if (jQuery.inArray(slide.placement, ['top-center', 'bottom-center']) != -1){
    slide_column.addClass('col-sm-offset-3');
}

if (jQuery.inArray(slide.placement, ['top-right', 'bottom-right']) != -1) {
    slide_column.addClass('col-sm-offset-6');
}

var box_shadow_property = '10px 0 0 #' + slide.bgcolor + ', -10px 0 0 #' + slide.bgcolor;

slide_text.css({
    backgroundColor: '#' + slide.bgcolor,
    boxShadow: box_shadow_property,
    boxDecorationBreak: 'clone',
    color: '#' + slide.color,
    fontSize: slide.fontSize
});

slide_text.html(slide.content);