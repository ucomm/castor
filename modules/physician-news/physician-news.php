<?php

class PhysicianNews extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'News and Publications', 'castor' ),
            'description'     => __( 'Displays physician news articles.', 'castor' ),
            'category'        => __( 'Castor Modules', 'castor' ),
            'dir'             => CASTOR_DIR . 'modules/physician-news/',
            'url'             => CASTOR_URL . 'modules/physician-news/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}