<?php

class PhysicianNewsModule extends Castor_Module {

    private $version = '1.0';
	public static $categories = array();

	public function __construct(){
		
		add_action('init', array('PhysicianNewsModule', 'get_terms'));
		
	}
	
	public static function get_terms(){
		
		PhysicianNewsModule::$categories = get_terms(array(
			'taxonomy' => 'physician-news-category',
			'hide_empty' => false
		));
		
	}
	
    public function load_module() {
		
        if (class_exists('FLBuilder')){
			
            require_once 'physician-news.php';
			
			$category_names = array(); 
			
			if (is_array(PhysicianNewsModule::$categories)){
			
				foreach (PhysicianNewsModule::$categories as $category){

					$category_names[$category->name] = $category->name;	

				}
				
			}

			// Add a default category for all.
			$category_names['All'] = 'All';
			ksort($category_names);
			
            FLBuilder::register_module('PhysicianNews', array(
                'settings' => array(
                    'title' => __('News', 'castor'),
                    'sections' => array(
                        'settings' => array(
                            'title' => __('Settings', 'castor'),
                            'fields' => array(
								'heading' => array(
									'type' => 'text',
									'label' => __('Heading', 'castor'),
									'default' => 'News'
								),
                                'category' => array(
                                    'type' => 'select',
                                    'label' => __('Category', 'castor'),
									'options' => $category_names
                                ),
                                'columns' => array(
                                    'type' => 'select',
                                    'label' => __('Columns', 'castor'),
									'default' => 3,
									'options' => array(
										1 => 1,
										2 => 2,
										3 => 3,
										4 => 4,
										5 => 5
									)
                                ),
                                'limit' => array(
                                    'type' => 'select',
                                    'label' => __('Limit', 'castor'),
									'default' => 6,
									'options' => array(
										1 => 1,
										2 => 2,
										3 => 3,
										4 => 4,
										5 => 5,
										6 => 6,
										7 => 7,
										8 => 8,
										9 => 9,
										10 => 10,
										11 => 11,
										12 => 12,
										13 => 13,
										14 => 14,
										15 => 15,
										16 => 16,
										17 => 17,
										18 => 18,
										19 => 19,
										20 => 20
									)
                                )
                            )
                        )
                    )
                )
            ));
			
		}
			
    }
	
}
