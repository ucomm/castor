<?php

class LocationCalloutModule extends Castor_Module {
    public function load_module() {
        if ( class_exists( 'FLBuilder' ) ) {
            require_once 'location-callout.php';
            FLBuilder::register_module('LocationCallout', array(
                'general' => array(
                    'title' => __('General', 'castor'),
                    'sections' => array(
                        'general' => array(
                            'fields' => array(
                                'photo' => array(
                                    'type' => 'photo',
                                    'label' => __('Photo', 'castor'),
                                    'show_remove' => true
                                ),
                                'name' => array(
                                    'type' => 'text',
                                    'label' => __('Name', 'castor'),
                                    'default' => 'UConn John Dempsey Hospital'
                                ),
                                'location_link' => array(
                                    'type' => 'link',
                                    'label' => __('Landing Page Link', 'castor')
                                ),
                                'directions_link' => array(
                                    'type' => 'link',
                                    'label' => __('Directions Link', 'castor')
                                ),
                                'content' => array(
                                    'type' => 'editor',
                                    'media_buttons' => false,
                                    'rows' => 8
                                )
                            )
                        )
                    )
                )
            ));
        }
    }
}