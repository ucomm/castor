<?php if ( !isset($settings->photo_src) || !isset($settings->name) ) {
    if ( is_user_logged_in() ) {
        _e('<p>Photo and name not set.  These must be set.</p>');
    }
} else { ?>
    <div class="uh-location-module">
        <img src="<?php echo $settings->photo_src; ?>" alt="<?php echo $settings->name; ?>" class="img-responsive">
        <h4>
            <?php if ( $settings->location_link ) : ?>
                <a href="<?php echo $settings->location_link; ?>"><?php echo $settings->name; ?></a>
            <?php else: ?>
                <?php echo $settings->name; ?>
            <?php endif; ?>
        </h4>
        <?php if ( $settings->content ) {
            echo $settings->content;
        } ?>
        <?php if ( $settings->directions_link ) : ?>
            <p>
                <a href="<?php echo $settings->directions_link; ?>">Directions</a>
            </p>
        <?php endif; ?>
    </div>
<?php } ?>
