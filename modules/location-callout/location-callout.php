<?php

class LocationCallout extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Location Callout', 'castor' ),
            'description'     => __( 'Display a UConn Health main site location element.', 'fl-builder' ),
            'category'        => __( 'Castor Modules', 'castor' ),
            'dir'             => CASTOR_DIR . 'modules/location-callout/',
            'url'             => CASTOR_URL . 'modules/location-callout/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}