<?php

/*
*
* This module will replace the UConn Today plugin. It was built by Shaun Cayabyab.
*
*/

require_once FIELDS_DIR . 'fields/number-input/number-input.php';

class UConnTodayModule extends Castor_Module {
  public function load_module() {
    if (class_exists('FLBuilder')) {
      require_once 'uconn-today.php';
      /**
      * Building the UCToday module for the page builder
      */
      FLBuilder::register_module( 'UConnToday', array(
        
        //Tab for UC-Today shortcode formatting
        'main_tab'		=> array(
          'title'			=> __( 'Main', 'castor' ),
          'sections'		=> array(

            //Post-type shortcode attributes section
            'shortcode_atts'	=> array(
              'title'				=> __( 'Shortcode Attributes', 'castor' ),
              'fields'			=> array(

                //Post-type selector field (main-feed, school-college, category)
                'post_type_select'	=> array(
                  'type'				=> 'select',
                  'label'				=> __( 'Type of Posts', 'castor' ),
                  'default'			=> 'school-college',
                  'options'			=> array(
                    'main-feed'			=> __( 'Main Feed', 'castor' ),
                    'author' => __('Author (by netID)', 'castor'),
                    'campus' => __('Campus', 'castor'),
                    'category'			=> __( 'Category', 'castor' ),
                    'county' => __('County', 'castor'),
                    'full-school-posts' => __('Full School Posts (includes zoned posts)', 'castor'),
                    'school-college'	=> __( 'School/College', 'castor' ),
                    'search' => __('Relevanssi Search', 'castor'),
                    'tag' => __('Tag', 'castor')
                  ),
                  'toggle'			=> array(
                    'main-feed'			=> array(),
                    'school-college'	=> array(
                      'fields'			=> array( 'school_field' )
                    ),
                    'author' => array(
                      'fields' => array('author_field')
                    ),
                    'campus' => array(
                      'fields' => array('campus_field')
                    ),
                    'category'			=> array(
                      'fields'			=> array( 'category_field' )
                    ),
                    'county' => array(
                      'fields' => array('county_field')
                    ),
                    'full-school-posts' => array(
                      'fields' => array('zone_field', 'school_field')
                    ),
                    'search' => array(
                      'fields' => array('search_field')
                    ),
                    'tag' => array(
                      'fields' => array( 'tag_field' )
                    )
                  )
                ),
                //School-college field
                'school_field'		=> array(
                  'type'				=> 'select',
                  'label'				=> __( 'School/College', 'castor' ),
                  'default'			=> 'college-of-agriculture-health-natural-resources',
                  'options'			=> array(
                    'college-of-agriculture-health-natural-resources'		=> __( 'College of Agriculture, Health, and Natural Resources', 'castor' ),
                    'college-of-liberal-arts-and-sciences'					=> __( 'College of Liberal Arts and Sciences', 'castor' ),
                    'neag-school-of-education'								=> __( 'Neag School of Education', 'castor' ),
                    'the-graduate-school'									=> __( 'The Graduate School', 'castor' ),
                    'research' => __('Research', 'castor'),
                    'school-of-business'									=> __( 'School of Business', 'castor' ),
                    'school-of-engineering'									=> __( 'School of Engineering', 'castor' ),
                    'school-of-fine-arts'									=> __( 'School of Fine Arts', 'castor' ),
                    'school-of-law'											=> __( 'School of Law', 'castor' ),
                    'school-of-nursing'										=> __( 'School of Nursing', 'castor' ),
                    'school-of-pharmacy'									=> __( 'School of Pharmacy', 'castor' ),
                    'school-of-social-work'									=> __( 'School of Social Work', 'castor' ),
                    'schools-of-medicine-and-dental-medicine'				=> __( 'Schools of Medicine and Dental Medicine', 'castor' ),
                  )
                ),

                // Author field
                'author_field' => array(
                  'type' => 'text',
                  'label' => __('Author', 'castor')
                ),

                // Campus field
                'campus_field' => array(
                  'type' => 'select',
                  'label' => __('Campus', 'castor'),
                  'default' => 'storrs',
                  'options' => array(
                    'storrs' => __('Storrs', 'castor'),
                    'avery-point' => __('Avery Point', 'castor'),
                    'hartford' => __('Hartford', 'castor'),
                    'stamford' => __('Stamford', 'castor'),
                    'waterbury' => __('Waterbury', 'castor')
                  )
                ),
                //Category field
                'category_field'	=> array(
                  'type'				=> 'select',
                  'label'				=> __( 'Category', 'castor' ),
                  'default'			=> 'arts',
                  'options'			=> array(
                    'arts'							=> __( 'Arts', 'castor' ),
                    'athletics'					=> __( 'Athletics', 'castor' ),
                    'business'						=> __( 'Business', 'castor' ),
                    'campus-life'					=> __( 'Campus Life', 'castor' ),
                    'community-impact'				=> __( 'Community Impact', 'castor' ),
                    'environment'					=> __( 'Environment', 'castor' ),
                    'health'						=> __( 'Health', 'castor' ),
                    'humanities-social-sciences'	=> __( 'Humanities and Social Sciences', 'castor' ),
                    'science'						=> __( 'science', 'castor' ),
                    'the-university'				=> __( 'The University', 'castor' ),
                    'uncategorized'					=> __( 'Uncategorized', 'castor' ),
                  )
                ),
                // County Field
                'county_field' => array(
                  'type' => 'select',
                  'label' => __('County', 'castor'),
                  'default' => 'fairfield-county',
                  'options' => array(
                    'all' => __('All Counties', 'castor'),
                    'fairfield-county' => __('Fairfield County', 'castor'),
                    'hartford-county' => __('Hartford County', 'castor'),
                    'litchfield-county' => __('Litchfield County', 'castor'),
                    'middlesex-county' => __('Middlesex County', 'castor'),
                    'new-haven-county' => __('New Haven County', 'castor'),
                    'new-london-county' => __('New London County', 'castor'),
                    'tolland-county' => __('Tolland County', 'castor'),
                    'windham-county' => __('Windham County', 'castor')
                  )
                ),
                // Zone Posts
                'zone_field' => array(
                  'type' => 'text',
                  'label' => __('Zone', 'castor'),
                  'description' => __('The slug of the zone being requested', 'castor')
                ),
                // Tag fields
                'tag_field' => array(
                  'type' => 'castor-number',
                  'label' => __('Tag ID', 'castor')
                ),

                // Search field
                'search_field' => array(
                  'type' => 'text',
                  'label' => __('Search', 'castor')
                ),
              )
            ),

            //Post formatting section
            'post_formatting'	=> array(
              'title'				=> __( 'Post Formatting', 'castor' ),
              'fields'			=> array(

                //Number of posts field
                'posts_num'			=> array(
                  'type'				=> 'select',
                  'label'				=> __( 'Number of Posts', 'castor' ),
                  'default'			=> '4',
                  'options'			=> array(
                    '1'					=> __( '1', 'castor' ),
                    '2'					=> __( '2', 'castor' ),
                    '3'					=> __( '3', 'castor' ),
                    '4'					=> __( '4', 'castor' ),
                    '5'					=> __( '5', 'castor' ),
                    '6'					=> __( '6', 'castor' ),
                    '7'					=> __( '7', 'castor' ),
                    '8'					=> __( '8', 'castor' ),
                    '9'					=> __( '9', 'castor' ),
                    '10'				=> __( '10', 'castor' ),
                  )
                ),

                //Columns field
                'columns_num'		=> array(
                  'type'				=> 'select',
                  'label'				=> __( 'Columns', 'castor' ),
                  'default'			=> '4',
                  'options'			=> array(
                    '1'					=> __( '1', 'castor' ),
                    '2'					=> __( '2', 'castor' ),
                    '3'					=> __( '3', 'castor' ),
                    '4'					=> __( '4', 'castor' ),
                  )
                ),

                //Pictures field
                'pictures'			=> array(
                  'type'				=> 'select',
                  'label'				=> __( 'Display Pictures', 'castor' ),
                  'default'			=> 'false',
                  'options'			=> array(
                    'true'				=> __( 'true', 'castor' ),
                    'false'				=> __( 'false', 'castor' ),
                  )
                ),	
              )
            )
          )
        ),
        'tab_2' => array(
          'title' => __('Style', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Style', 'castor'),
              'fields' => array(
                'uctoday_link_color' => array(
                  'type' => 'color',
                  'label' => __('Link Color', 'castor'),
                  'show_reset' => true,
                  'default' => '#1540a2'
                ),
                'uctoday_link_hover_color' => array(
                  'type' => 'color',
                  'label' => __('Link Color', 'castor'),
                  'show_reset' => true,
                  'default' => '#b50106'
                )
              )
            ) 
          )
        )
      ));
    }
  }
}