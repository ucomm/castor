.fl-module-content .uctoday-list-item .uctoday-cell-title {
  color: #<?php echo $settings->uctoday_link_color; ?>;
}

.fl-module-content .uctoday-list-item .uctoday-cell-title:hover {
  color: #<?php echo $settings->uctoday_link_hover_color; ?>;
}