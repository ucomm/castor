<?php

class UConnToday extends FLBuilderModule {
  public function __construct(){
    parent::__construct(array(
            'name'            => __( 'UConn Today', 'castor' ),
            'description'     => __( 'Beaverized UCToday Plugin', 'castor' ),
            'category'        => __( 'Castor Modules', 'castor' ),
            'dir'             => CASTOR_DIR . 'modules/uconn-today/',
            'url'             => CASTOR_URL . 'modules/uconn-today/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));

        $shortcode_output;
  }

  /**
  * construct_shortcode
  * ===================
  *
  * Constructs the shortcode needed to interact with the UC-Today plugin
  * based on the settings made in the page builder module.
  */
  public function construct_shortcode(){
    
    //Get the settings from the module
    $settings = $this->settings;

    //Setting the attributes for shortcode constructions
    $post_type;
    $number_of_posts = $settings->posts_num;
    $columns = $settings->columns_num;
    $pictures = $settings->pictures;

    //Depending on the post-type selected, use the apporpriate shortcode attribute
    switch($settings->post_type_select){
      case 'main-feed':
        $post_type = '';
        break;
      case 'school-college':
        $post_type = "school-college='" . $settings->school_field . "'";
        break;
      case 'author':
        $post_type = "author='". $settings->author_field . "'";
        break;
      case 'campus':
      $post_type = "campus='" . $settings->campus_field . "'";
        break;
      case 'category':
        $post_type = "category='" . $settings->category_field . "'";
        break;
      case 'county':
        $post_type = "county='" . $settings->county_field . "'";
        break;
      case 'tag':
        $post_type = "tag='" . $settings->tag_field . "'";
        break;
      case 'search':
        $post_type = "search='" . $settings->search_field . "'";
        break;
      case 'full-school-posts':
        $post_type = "zone='" . $settings->zone_field . "' school-college='" . $settings->school_field . "'";
        break;
       default:
        $post_type = '';
    }
    //Sets the shortcode_output class variable with the given settings
    $this->shortcode_output = "[UC-Today " . $post_type . " number-of-posts='" . $number_of_posts . "' columns='" . $columns . "' pictures='" . $pictures . "']";
  }
}
