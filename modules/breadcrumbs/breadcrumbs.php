<?php

class Breadcrumbs extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Breadcrumbs', 'castor' ),
            'description'     => __( 'Displays breadcrumb trail.', 'castor' ),
            'category'        => __( 'Castor Modules', 'castor' ),
            'dir'             => CASTOR_DIR . 'modules/breadcrumbs/',
            'url'             => CASTOR_URL . 'modules/breadcrumbs/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
	
	public function breadcrumbs($settings) {
		global $post;
		echo '<nav aria-label="breadcrumb navigation"><ol class="breadcrumbs" >';
		if (!is_home()) {
			echo '<li><img class="home-icon" aria-hidden="true" src="' . CASTOR_URL . 'modules/breadcrumbs/img/home.png' . '" /><a href="' . get_option('home') . '">';
			echo (!empty($settings->home_page_text)) ? $settings->home_page_text : get_bloginfo('name');
			echo '</a></li>';
			if (is_category() || is_single()) {
				echo '<li>';
				the_category(' </li><li> ');
				if (is_single()) {
					echo '</li><li>';
					the_title();
					echo '</li>';
				}
			} elseif (is_page()) {
				$title = get_the_title();
				if ($post->post_parent){
					$anc = get_post_ancestors( $post->ID );
					$anc = array_reverse($anc);
					$output = '';
					foreach ( $anc as $ancestor ) {
						$output .= '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li>';
					}
					echo $output;
				} 
				echo "<li class='page-title'><a href='#' aria-current='location'>$title</a></li>";
			}
		}
		elseif (is_tag()) {
			single_tag_title();
		}
		elseif (is_day()) {
			echo "<li>Archive for "; 
			the_time('F jS, Y'); 
			echo '</li>';
		}
		elseif (is_month()) {
			echo "<li>Archive for "; 
			the_time('F, Y'); 
			echo '</li>';
		}
		elseif (is_year()) {
			echo "<li>Archive for "; 
			the_time('Y'); 
			echo '</li>';
		}
		elseif (is_author()) {
			echo "<li>Author Archive</li>";
		}
		elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
			echo "<li>Blog Archives</li>";
		}
		elseif (is_search()) {
			echo "<li>Search Results</li>";
		}
		echo '</ol></nav>';
	}
}