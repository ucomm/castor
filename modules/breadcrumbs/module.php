<?php

class BreadcrumbsModule extends Castor_Module {

    private $version = '1.0';

    public function load_module() {
		
        if (class_exists('FLBuilder')){
			
            require_once 'breadcrumbs.php';
			
            FLBuilder::register_module('Breadcrumbs', array(
                'settings' => array(
                    'title' => __( 'Settings', 'castor' ),
                    'sections' => array(
                        'general' => array(
                            'title' => __( 'General', 'castor' ),
                            'fields' => array(
                                'home_page_text' => array(
                                    'type' => 'text',
                                    'label' => __('Home Page Text', 'castor')
                                )
                            )
                        )
                    )
                ),
            ));
			
		}
			
    }
	
}
