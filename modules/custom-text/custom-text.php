<?php

class CustomText extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Custom Text', 'castor' ),
            'description'     => __( 'Displays custom text.', 'castor' ),
            'category'        => __( 'Castor Modules', 'castor' ),
            'dir'             => CASTOR_DIR . 'modules/custom-text/',
            'url'             => CASTOR_URL . 'modules/custom-text/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
		
    }	
}