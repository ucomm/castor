<?php

class CustomTextModule extends Castor_Module {

    private $version = '1.0';

    public function load_module() {
		
        if (class_exists('FLBuilder')){
			
            require_once 'custom-text.php';
			
            FLBuilder::register_module('CustomText', array(
                'tab_1' => array(
                    'title' => __( 'Settings', 'castor' ),
                    'sections' => array(
						'section_1' => array(
							'title' => __('Texts', 'castor'),
							'fields' => array(
                                'custom_texts' => array(
                                    'type' => 'form',
                                    'label' => __('Text', 'castor'),
									'form'	=> 'custom_text_form',
									'preview_text' => 'custom_text',
                                    'multiple' => true
                                )
                            )
						),
						'section_2' => array(
							'title' => __('Text Alignment', 'castor'),
							'fields' => array(
								'alignment' => array(
									'type' => 'select',
									'label' => __('Text Alignment', 'castor'),
									'default' => 'left',
									'options' => array(
										'left' => __('Left', 'castor'),
										'center' => __('Center', 'castor'),
										'right' => __('Right', 'castor')
									)
								)
							)
						)
					)
                )
            ));
			
			FLBuilder::register_settings_form('custom_text_form', array(
                'title' => __('Text Settings', 'castor'),
                'tabs' => array(
                    'tab_1' => array(
                        'title' => __('Custom Text', 'castor'),
                        'sections' => array(
                            'section_1' => array(
								'title' => __('General', 'castor'),
                                'fields' => array(
                                    'custom_text' => array(
                                        'type' => 'textarea',
                                        'label' => __('Text', 'castor'),
										'rows' => 5
                                    ),
                                    'text_type' => array(
                                        'type' => 'select',
										'label' => __('Text Type', 'castor'),
										'default' => 'p',
										'options' => array(
											'p' => __('Paragraph', 'castor'),
											'blockquote' => __('Blockquote', 'castor')
										),
										'toggle' => array(
											'p' => array(),
											'blockquote' => array(
												'fields' => array(
													'citation'
												),
												'sections' => array('section_4')
											)
										)
									),
									'citation' => array(
										'type' => 'text',
										'label' => __('Quote citation')
									)
								)
							),
							'section_2' => array(
								'title' => __('Link', 'castor'),
								'fields' => array(
                                    'link_url' => array(
                                        'type' => 'text',
										'label' => __('Link URL', 'castor')
                                    ),
                                    'link_aria_label' => array(
                                        'type' => 'textarea',
                                        'label' => __('Aria Label', 'castor'),
										'rows' => 3
									),
									
								)
							),
							'section_3' => array(
								'title' => __('Typography', 'castor'),
								'fields' => array(
									'font_family_and_weight' => array(
										'type' => 'font',
										'label' => __('Font', 'castor'),
										'default' => array(
											'family' => 'Georgia',
											'weight' => '400'
										)
									),
									'font_size' => array(
										'type' => 'unit',
										'label' => __('Font Size', 'castor'),
										'default' => '24',
										'description' => 'px',
										'responsive' => true
									),
									'font_color' => array(
										'type' => 'color',
										'label' => __('Font Color', 'castor'),
										'default' => '000000'
									),
									'font_background_color' => array(
										'type' => 'color',
										'label' => __('Background Color', 'castor'),
										'default' => 'ffffff',
										'show_reset' => true,
										'show_alpha' => true
									),
									'line_height' => array(
                                        'type' => 'unit',
                                        'label' => __('Line Height', 'castor'),
										'default' => '1.4',
										'description' => 'em',
										'responsive' => true
									),
									'link_color' => array(
										'type' => 'color',
										'label' => __('Link Color', 'castor'),
										'default' => '000e2f',
										'show_reset' => true
									),
									'link_hover_color' => array(
										'type' => 'color',
										'label' => __('Link Hover Color', 'castor'),
										'default' => '000e2f',
										'show_reset' => true
									),
                                    'link_text_decoration' => array(
                                        'type' => 'select',
										'label' => __('Link Text Decoration', 'castor'),
										'default' => 'underline',
										'options' => array(
											'none' => 'None',
											'dashed' => 'Dashed',
											'dotted' => 'Dotted',
											'underline' => 'Underline'
										)
									),
									'link_text_hover_decoration' => array(
                                        'type' => 'select',
										'label' => __('Link Text Decoration (Hover)', 'castor'),
										'default' => 'none',
										'options' => array(
											'none' => 'None',
											'dashed' => 'Dashed',
											'dotted' => 'Dotted',
											'underline' => 'Underline'
										)
                                    )
								)
							),
							'section_4' => array(
								'title' => __('Citation Typography', 'castor'),
								'fields' => array(
									'citation_font_family_and_weight' => array(
										'type' => 'font',
										'label' => __('Citation Citation Font', 'castor'),
										'default' => array(
											'family' => 'Georgia',
											'weight' => '400'
										)
									),
									'citation_font_size' => array(
										'type' => 'unit',
										'label' => __('Citation Font Size', 'castor'),
										'default' => '14',
										'description' => 'px'
									),
									'citation_font_color' => array(
										'type' => 'color',
										'label' => __('Citation Font Color', 'castor'),
										'default' => '000000'
									),
									'citation_margin' => array(
										'type' => 'dimension',
										'label' => __( 'Citation Margins', 'fl-builder' ),
										'description' => 'px',
										'default' => '0',
										'preview' => array(
											'type' => 'none'
										),
										'responsive' => true
									),
									'citation_padding' => array(
										'type' => 'dimension',
										'label' => __( 'Citation Padding', 'fl-builder' ),
										'description' => 'px',
										'default' => '0',
										'preview' => array(
											'type' => 'none'
										),
										'responsive' => true
									),
								)
							),
							'section_5' => array(
								'title' => __('Advanced', 'castor'),
								'fields' => array(
									'margin' => array(
										'type' => 'dimension',
										'label' => __( 'Margins', 'fl-builder' ),
										'description' => 'px',
										'default' => '0',
										'preview' => array(
											'type' => 'none'
										),
										'responsive' => true
									),
									'padding' => array(
										'type' => 'dimension',
										'label' => __( 'Padding', 'fl-builder' ),
										'description' => 'px',
										'default' => '0',
										'preview' => array(
											'type' => 'none'
										),
										'responsive' => true
									),
									'border' => array(
										'type' => 'dimension',
										'label' => __('Border Width', 'castor'),
										'description' => 'px',
										'default' => '0',
										'preview' => array(
											'type' => 'none'
										),
										'responsive' => true
									),
									'border_color' => array(
										'type' => 'color',
										'label' => __('Border Color', 'castor'),
										'default' => 'none',
										'show_reset' => true
									),
									'border_style' => array(
										'type' => 'select',
										'label' => __('Border Style', 'castor'),
										'default' => 'none',
										'options' => array(
											'none' => __('None', 'castor'),
											'solid' => __('Solid', 'castor'),
											'dashed' => __('Dashed', 'castor'),
											'dotted' => __('Dotted', 'castor'),
											'double' => __('Double', 'castor'),
											'groove' => __('Groove', 'castor'),
											'hidden' => __('Hidden', 'castor'),
											'inset' => __('Inset', 'castor'),
											'outset' => __('Outset', 'castor'),
											'ridge' => __('Ridge', 'castor')
										)
									)
								)
							),
                        )
                    )
				)
            ));
			
		}
			
    }
	
}
