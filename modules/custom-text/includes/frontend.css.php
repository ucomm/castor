#uc-custom-text-container-<?php echo $id; ?> {
  text-align: <?php echo $settings->alignment; ?>;
}

<?php

$settings_array = array(
  'medium_breakpoint',
  'responsive_breakpoint'
);

$requested_settings_array = Castor_Helpers::get_bb_settings($settings_array);

$responsive_bp = $requested_settings_array['responsive_breakpoint'];
$medium_bp = $requested_settings_array['medium_breakpoint'];

foreach ($settings->custom_texts as $index => $style) {
  $instance = "custom_text-$id-$index";

  $res_values_array = array(
    'border_top' => array(
      'base' => $style->border_top,
      'medium' => $style->border_top_medium,
      'responsive' => $style->border_top_responsive
    ),
    'border_right' => array(
      'base' => $style->border_right,
      'medium' => $style->border_right_medium,
      'responsive' => $style->border_right_responsive
    ),
    'border_bottom' => array(
      'base' => $style->border_bottom,
      'medium' => $style->border_bottom_medium,
      'responsive' => $style->border_bottom_responsive
    ),
    'border_left' => array(
      'base' => $style->border_left,
      'medium' => $style->border_left_medium,
      'responsive' => $style->border_left_responsive
    ),
    'font_size' => array(
      'base' => $style->font_size,
      'medium' => $style->font_size_medium,
      'responsive' => $style->font_size_responsive
    ),
    'line_height' => array(
      'base' => $style->line_height,
      'medium' => $style->line_height_medium,
      'responsive' => $style->line_height_responsive
    ),
    'margin_top' => array(
      'base' => $style->margin_top,
      'medium' => $style->margin_top_medium,
      'responsive' => $style->margin_top_responsive
    ),
    'margin_right' => array(
      'base' => $style->margin_right,
      'medium' => $style->margin_right_medium,
      'responsive' => $style->margin_right_responsive
    ),
    'margin_bottom' => array(
      'base' => $style->margin_bottom,
      'medium' => $style->margin_bottom_medium,
      'responsive' => $style->margin_bottom_responsive
    ),
    'margin_left' => array(
      'base' => $style->margin_left,
      'medium' => $style->margin_left_medium,
      'responsive' => $style->margin_left_responsive
    ),
    'padding_top' => array(
      'base' => $style->padding_top,
      'medium' => $style->padding_top_medium,
      'responsive' => $style->padding_top_responsive
    ),
    'padding_right' => array(
      'base' => $style->padding_right,
      'medium' => $style->padding_right_medium,
      'responsive' => $style->padding_right_responsive
    ),
    'padding_bottom' => array(
      'base' => $style->padding_bottom,
      'medium' => $style->padding_bottom_medium,
      'responsive' => $style->padding_bottom_responsive
    ),
    'padding_left' => array(
      'base' => $style->padding_left,
      'medium' => $style->padding_left_medium,
      'responsive' => $style->padding_left_responsive
    ),
    'citation_margin_top' => array(
      'base' => $style->citation_margin_top,
      'medium' => $style->citation_margin_top_medium,
      'responsive' => $style->citation_margin_top_responsive
    ),
    'citation_margin_right' => array(
      'base' => $style->citation_margin_right,
      'medium' => $style->citation_margin_right_medium,
      'responsive' => $style->citation_margin_right_responsive
    ),
    'citation_margin_bottom' => array(
      'base' => $style->citation_margin_bottom,
      'medium' => $style->citation_margin_bottom_medium,
      'responsive' => $style->citation_margin_bottom_responsive
    ),
    'citation_margin_left' => array(
      'base' => $style->citation_margin_left,
      'medium' => $style->citation_margin_left_medium,
      'responsive' => $style->citation_margin_left_responsive
    ),
    'citation_padding_top' => array(
      'base' => $style->citation_padding_top,
      'medium' => $style->citation_padding_top_medium,
      'responsive' => $style->citation_padding_top_responsive
    ),
    'citation_padding_right' => array(
      'base' => $style->citation_padding_right,
      'medium' => $style->citation_padding_right_medium,
      'responsive' => $style->citation_padding_right_responsive
    ),
    'citation_padding_bottom' => array(
      'base' => $style->citation_padding_bottom,
      'medium' => $style->citation_padding_bottom_medium,
      'responsive' => $style->citation_padding_bottom_responsive
    ),
    'citation_padding_left' => array(
      'base' => $style->citation_padding_left,
      'medium' => $style->citation_padding_left_medium,
      'responsive' => $style->citation_padding_left_responsive
    ),
  );

  $res_values = Castor_Helpers::create_responsive_values($res_values_array);
  $has_link = filter_var($style->link_url, FILTER_VALIDATE_URL);

  switch ($style->text_type) {
    case 'blockquote':
      include($module->dir . 'includes/partials/blockquote.css.php');
      include($module->dir . 'includes/partials/link.css.php');
      break;
    case 'p':
      include($module->dir . 'includes/partials/paragraph.css.php');
      include($module->dir . 'includes/partials/link.css.php');
      break;
    default:
      include($module->dir . 'includes/partials/paragraph.css.php');
      include($module->dir . 'includes/partials/link.css.php');
      break;
  }
}