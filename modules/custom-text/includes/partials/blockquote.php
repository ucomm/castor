<div id="<?php echo $instance; ?>" class="blockquote-container">
  <blockquote class="custom-text">
    <?php
      if (!$has_link) {
        echo $custom_text->custom_text;
      } else {
        include($module->dir . 'includes/partials/link.php');
      }
    ?>
  </blockquote>
  <?php
    if ($custom_text->citation) {
  ?>
      <cite>
        <?php echo $custom_text->citation; ?>
      </cite>
  <?php
    }
  ?>
</div>