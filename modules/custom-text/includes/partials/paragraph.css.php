#<?php echo $instance; ?> {
  <?php
    include($module->dir . 'includes/styles/border-base.css.php');
    include($module->dir . 'includes/styles/margin-base.css.php');
    include($module->dir . 'includes/styles/padding-base.css.php');
    if ($style->font_background_color) echo "background-color: #$style->font_background_color;";
    if ($style->font_color) echo "color: #$style->font_color;";
    if ($style->font_family_and_weight->family) echo "font-family: " . $style->font_family_and_weight->family . ";";
    if ($style->font_family_and_weight->weight) echo "font-weight: " . $style->font_family_and_weight->weight . ";";
    echo "font-size: " . $res_values['font_size']['base'] . "px;";
    if ($style->line_height) echo "line-height: " . $res_values['line_height']['base'] . "em;";
  ?>
}

<?php
if ($has_link && $style->link_text_decoration) {
  include($module->dir . 'includes/partials/link.css.php');
}
?>

@media screen and (max-width: <?php echo $medium_bp; ?>px) {
  #<?php echo $instance; ?> {
    <?php
      include($module->dir . 'includes/styles/border-medium.css.php');
      include($module->dir . 'includes/styles/margin-medium.css.php');
      include($module->dir . 'includes/styles/padding-medium.css.php');
    ?>
    font-size: <?php echo $res_values['font_size']['medium']; ?>px;
    line-height: <?php echo $res_values['line_height']['medium']; ?>em;
  }
}

@media screen and (max-width: <?php echo $responsive_bp; ?>px) {
  #<?php echo $instance; ?> {
    <?php
      include($module->dir . 'includes/styles/border-responsive.css.php');
      include($module->dir . 'includes/styles/margin-responsive.css.php');
      include($module->dir . 'includes/styles/padding-responsive.css.php');
    ?>
    font-size: <?php echo $res_values['font_size']['responsive']; ?>px;
    line-height: <?php echo $res_values['line_height']['responsive']; ?>em;
  }
}
