#<?php echo $instance; ?> a {
  background-color: #<?php echo $style->font_background_color ?>;
  color: #<?php echo $style->link_color; ?>;
  text-decoration: <?php echo $style->link_text_decoration; ?>;
  <?php
    if ($style->font_family_and_weight->family) echo "font-family: " . $style->font_family_and_weight->family . ";";
    if ($style->font_family_and_weight->weight) echo "font-weight: " . $style->font_family_and_weight->weight . ";";
    echo "font-size: " . $res_values['font_size']['base'] . "px;";
    if ($style->line_height) echo "line-height: " . $res_values['line_height']['base'] . "em;";
  ?>
}
#<?php echo $instance; ?> a:hover,
#<?php echo $instance; ?> a:focus {
  background-color: #<?php echo $style->font_background_color ?>;
  color: #<?php echo $style->link_hover_color; ?>;
  text-decoration: <?php echo $style->link_text_hover_decoration; ?>;
}