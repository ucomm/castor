#<?php echo $instance; ?> {
  <?php
    include($module->dir . 'includes/styles/border-base.css.php');
    if ($style->font_background_color) echo "background-color: #$style->font_background_color;";
  ?>
}

#<?php echo $instance; ?> blockquote {
  <?php
    include($module->dir . 'includes/styles/margin-base.css.php');
    include($module->dir . 'includes/styles/padding-base.css.php');
    
    if ($style->font_color) echo "color: #$style->font_color;";
    if ($style->font_family_and_weight->family) echo "font-family: " . $style->font_family_and_weight->family . ";";
    if ($style->font_family_and_weight->weight) echo "font-weight: " . $style->font_family_and_weight->weight . ";";
    echo "font-size: " . $res_values['font_size']['base'] . "px;";
    if ($style->line_height) echo "line-height: " . $res_values['line_height']['base'] . "em;";
    ?>
}

<?php
if ($has_link && $style->link_text_decoration) {
  include($module->dir . 'includes/partials/link.css.php');
}
?>

<?php 
  if ($style->citation) {
?>
    #<?php echo $instance; ?> cite {
      color: #<?php echo $style->font_color; ?>;
      font-family: <?php echo $style->font_family_and_weight->family; ?>;
      font-size: <?php echo $style->citation_font_size; ?>px;
      font-style: normal;
      font-weight: <?php echo $style->font_family_and_weight->weight; ?>;
      <?php
        // begin margin style declarations
        if (isset($res_values['citation_margin_top']['base']) && $res_values['citation_margin_top']['base']) {
          echo "margin-top: " . $res_values['citation_margin_top']['base'] . "px;";
        }
        if (isset($res_values['citation_margin_right']['base']) && $res_values['citation_margin_right']['base']) {
          echo "margin-right: " . $res_values['citation_margin_right']['base'] . "px;";
        }
        if (isset($res_values['citation_margin_bottom']['base']) && $res_values['citation_margin_bottom']['base']) {
          echo "margin-bottom: " . $res_values['citation_margin_bottom']['base'] . "px;";
        }
        if (isset($res_values['citation_margin_left']['base']) && $res_values['citation_margin_left']['base']) {
          echo "margin-left: " . $res_values['citation_margin_left']['base'] . "px;";
        }

        // end margin style declarations.
        // begin padding style declarations
        if (isset($res_values['citation_padding_top']['base']) && $res_values['citation_padding_top']['base']) {
          echo "padding-top: " . $res_values['citation_padding_top']['base'] . "px;";
        }
        if (isset($res_values['citation_padding_right']['base']) && $res_values['citation_padding_right']['base']) {
          echo "padding-right: " . $res_values['citation_padding_right']['base'] . "px;";
        }
        if (isset($res_values['citation_padding_bottom']['base']) && $res_values['citation_padding_bottom']['base']) {
          echo "padding-bottom: " . $res_values['citation_padding_bottom']['base'] . "px;";
        }
        if (isset($res_values['citation_padding_left']['base']) && $res_values['citation_padding_left']['base']) {
          echo "padding-left: " . $res_values['citation_padding_left']['base'] . "px;";
        }

        // end padding style declarations.
      ?>
    }
<?php
  }
?>
@media screen and (max-width: <?php echo $responsive_bp; ?>px) {
  #<?php echo $instance; ?> {
    <?php
      include($module->dir . 'includes/styles/border-responsive.css.php');
    ?>
  }
  #<?php echo $instance; ?> blockquote {
    <?php
      include($module->dir . 'includes/styles/margin-responsive.css.php');
      include($module->dir . 'includes/styles/padding-responsive.css.php');
      ?>
      font-size: <?php echo $res_values['font_size']['responsive']; ?>px;
      line-height: <?php echo $res_values['line_height']['responsive']; ?>em;
  }
  <?php
    if ($style->citation) {
  ?>
      #<?php echo $instance; ?> cite {
         <?php
        // begin margin style declarations
        if (isset($res_values['citation_margin_top']['responsive']) && $res_values['citation_margin_top']['responsive']) {
          echo "margin-top: " . $res_values['citation_margin_top']['responsive'] . "px;";
        }
        if (isset($res_values['citation_margin_right']['responsive']) && $res_values['citation_margin_right']['responsive']) {
          echo "margin-right: " . $res_values['citation_margin_right']['responsive'] . "px;";
        }
        if (isset($res_values['citation_margin_bottom']['responsive']) && $res_values['citation_margin_bottom']['responsive']) {
          echo "margin-bottom: " . $res_values['citation_margin_bottom']['responsive'] . "px;";
        }
        if (isset($res_values['citation_margin_left']['responsive']) && $res_values['citation_margin_left']['responsive']) {
          echo "margin-left: " . $res_values['citation_margin_left']['responsive'] . "px;";
        }

        // end margin style declarations.
        // begin padding style declarations
        if (isset($res_values['citation_padding_top']['responsive']) && $res_values['citation_padding_top']['responsive']) {
          echo "padding-top: " . $res_values['citation_padding_top']['responsive'] . "px;";
        }
        if (isset($res_values['citation_padding_right']['responsive']) && $res_values['citation_padding_right']['responsive']) {
          echo "padding-right: " . $res_values['citation_padding_right']['responsive'] . "px;";
        }
        if (isset($res_values['citation_padding_bottom']['responsive']) && $res_values['citation_padding_bottom']['responsive']) {
          echo "padding-bottom: " . $res_values['citation_padding_bottom']['responsive'] . "px;";
        }
        if (isset($res_values['citation_padding_left']['responsive']) && $res_values['citation_padding_left']['responsive']) {
          echo "padding-left: " . $res_values['citation_padding_left']['responsive'] . "px;";
        }

        // end padding style declarations.
      ?>
      }
  <?php
    }
  ?>
}

@media screen and (max-width: <?php echo $medium_bp; ?>px) {
    #<?php echo $instance; ?> {
    <?php
      include($module->dir . 'includes/styles/border-medium.css.php');
    ?>
    font-size: <?php echo $res_values['font_size']['medium']; ?>px;
    line-height: <?php echo $res_values['line_height']['medium']; ?>em;
  }
  #<?php echo $instance; ?> blockquote {
    <?php
      include($module->dir . 'includes/styles/margin-medium.css.php');
      include($module->dir . 'includes/styles/padding-medium.css.php');
    ?>
    font-size: <?php echo $res_values['font_size']['medium']; ?>px;
    line-height: <?php echo $res_values['line_height']['medium']; ?>em;
  }
  <?php
    if ($style->citation) {
  ?>
      #<?php echo $instance; ?> cite {
         <?php
        // begin margin style declarations
        if (isset($res_values['citation_margin_top']['medium']) && $res_values['citation_margin_top']['medium']) {
          echo "margin-top: " . $res_values['citation_margin_top']['medium'] . "px;";
        }
        if (isset($res_values['citation_margin_right']['medium']) && $res_values['citation_margin_right']['medium']) {
          echo "margin-right: " . $res_values['citation_margin_right']['medium'] . "px;";
        }
        if (isset($res_values['citation_margin_bottom']['medium']) && $res_values['citation_margin_bottom']['medium']) {
          echo "margin-bottom: " . $res_values['citation_margin_bottom']['medium'] . "px;";
        }
        if (isset($res_values['citation_margin_left']['medium']) && $res_values['citation_margin_left']['medium']) {
          echo "margin-left: " . $res_values['citation_margin_left']['medium'] . "px;";
        }

        // end margin style declarations.
        // begin padding style declarations
        if (isset($res_values['citation_padding_top']['medium']) && $res_values['citation_padding_top']['medium']) {
          echo "padding-top: " . $res_values['citation_padding_top']['medium'] . "px;";
        }
        if (isset($res_values['citation_padding_right']['medium']) && $res_values['citation_padding_right']['medium']) {
          echo "padding-right: " . $res_values['citation_padding_right']['medium'] . "px;";
        }
        if (isset($res_values['citation_padding_bottom']['medium']) && $res_values['citation_padding_bottom']['medium']) {
          echo "padding-bottom: " . $res_values['citation_padding_bottom']['medium'] . "px;";
        }
        if (isset($res_values['citation_padding_left']['medium']) && $res_values['citation_padding_left']['medium']) {
          echo "padding-left: " . $res_values['citation_padding_left']['medium'] . "px;";
        }

        // end padding style declarations.
      ?>
      }
  <?php
    }
  ?>
}