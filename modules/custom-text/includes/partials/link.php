<a class="custom-text-link" 
  href="<?php echo $custom_text->link_url; ?>"
  aria-label="<?php echo $custom_text->link_aria_label; ?>">
  <?php echo $custom_text->custom_text; ?>  
</a>