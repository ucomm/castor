<?php
  // begin border style declarations
    if (isset($res_values['border_top']['base']) && $res_values['border_top']['base']) {
      echo "border-top-width: " . $res_values['border_top']['base'] . "px;";
      if ($style->border_style !== 'none') {
        echo "border-top-style: $style->border_style;";
      }
      if ($style->border_color !== '') {
        echo "border-top-color: #$style->border_color;";
      }
    }
    if (isset($res_values['border_right']['base']) && $res_values['border_right']['base']) {
      echo "border-right-width: " . $res_values['border_right']['base'] . "px;";
      if ($style->border_style !== 'none') {
        echo "border-right-style: $style->border_style;";
      }
      if ($style->border_color !== '') {
        echo "border-right-color: #$style->border_color;";
      }
    }
    if (isset($res_values['border_bottom']['base']) && $res_values['border_bottom']['base']) {
      echo "border-bottom-width: " . $res_values['border_bottom']['base'] . "px;";
      if ($style->border_style !== 'none') {
        echo "border-bottom-style: $style->border_style;";
      }
      if ($style->border_color !== '') {
        echo "border-bottom-color: #$style->border_color;";
      }
    }
    if (isset($res_values['border_left']['base']) && $res_values['border_left']['base']) {
      echo "border-left-width: " . $res_values['border_left']['base'] . "px;";
      if ($style->border_style !== 'none') {
        echo "border-left-style: $style->border_style;";
      }
      if ($style->border_color !== '') {
        echo "border-left-color: #$style->border_color;";
      }
    }
    // end border style declarations.
  ?>