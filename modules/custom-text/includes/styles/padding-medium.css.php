<?php
// begin padding style declarations
if (isset($res_values['padding_top']['medium']) && $res_values['padding_top']['medium']) {
  echo "padding-top: " . $res_values['padding_top']['medium'] . "px;";
} else {
  echo "padding-top: 0px;";
}
if (isset($res_values['padding_right']['medium']) && $res_values['padding_right']['medium']) {
  echo "padding-right: " . $res_values['padding_right']['medium'] . "px;";
} else {
  echo "padding-right: 0px;";
}
if (isset($res_values['padding_bottom']['medium']) && $res_values['padding_bottom']['medium']) {
  echo "padding-bottom: " . $res_values['padding_bottom']['medium'] . "px;";
} else {
  echo "padding-bottom: 0px;";
}
if (isset($res_values['padding_left']['medium']) && $res_values['padding_left']['medium']) {
  echo "padding-left: " . $res_values['padding_left']['medium'] . "px;";
} else {
  echo "padding-left: 0px;";
}

// end padding style declarations.