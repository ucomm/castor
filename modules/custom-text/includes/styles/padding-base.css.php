<?php
if (isset($res_values['padding_top']['base']) && $res_values['padding_top']['base']) {
  echo "padding-top: " . $res_values['padding_top']['base'] . "px;";
} else {
  echo "padding-top: 0px;";
}

if (isset($res_values['padding_right']['base']) && $res_values['padding_right']['base']) {
  echo "padding-right: " . $res_values['padding_right']['base'] . "px;";
} else {
  echo "padding-right: 0px;";
}

if (isset($res_values['padding_bottom']['base']) && $res_values['padding_bottom']['base']) {
  echo "padding-bottom: " . $res_values['padding_bottom']['base'] . "px;";
} else {
  echo "padding-bottom: 0px;";
}

if (isset($res_values['padding_left']['base']) && $res_values['padding_left']['base']) {
  echo "padding-left: " . $res_values['padding_left']['base'] . "px;";
} else {
  echo "padding-left: 0px;";
}