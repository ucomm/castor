<?php
// begin margin style declarations
if (isset($res_values['margin_top']['medium']) && $res_values['margin_top']['medium']) {
  echo "margin-top: " . $res_values['margin_top']['medium'] . "px;";
} else {
  echo "margin-top: 0px;";
}
if (isset($res_values['margin_right']['medium']) && $res_values['margin_right']['medium']) {
  echo "margin-right: " . $res_values['margin_right']['medium'] . "px;";
} else {
  echo "margin-right: 0px;";
}
if (isset($res_values['margin_bottom']['medium']) && $res_values['margin_bottom']['medium']) {
  echo "margin-bottom: " . $res_values['margin_bottom']['medium'] . "px;";
} else {
  echo "margin-bottom: 0px;";
}
if (isset($res_values['margin_left']['medium']) && $res_values['margin_left']['medium']) {
  echo "margin-left: " . $res_values['margin_left']['medium'] . "px;";
} else {
  echo "margin-left: 0px;";
}

// end margin style declarations.