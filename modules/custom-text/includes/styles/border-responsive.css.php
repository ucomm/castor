<?php
  // begin border style declarations
  if (isset($res_values['border_top']['responsive']) && $res_values['border_top']['responsive']) {
    echo "border-top-width: " . $res_values['border_top']['responsive'] . "px;";
    if ($style->border_style !== 'none') {
      echo "border-top-style: $style->border_style;";
    }
    if ($style->border_color !== '') {
      echo "border-top-color: #$style->border_color;";
    }
  }

  if (isset($res_values['border_right']['responsive']) && $res_values['border_right']['responsive']) {
    echo "border-right-width: " . $res_values['border_right']['responsive'] . "px;";
    if ($style->border_style !== 'none') {
      echo "border-right-style: $style->border_style;";
    }
    if ($style->border_color !== '') {
      echo "border-right-color: #$style->border_color;";
    }
  }

  if (isset($res_values['border_bottom']['responsive']) && $res_values['border_bottom']['responsive']) {
    echo "border-bottom-width: " . $res_values['border_bottom']['responsive'] . "px;";
    if ($style->border_style !== 'none') {
      echo "border-bottom-style: $style->border_style;";
    }
    if ($style->border_color !== '') {
      echo "border-bottom-color: #$style->border_color;";
    }
  }
  
  if (isset($res_values['border_left']['responsive']) && $res_values['border_left']['responsive']) {
    echo "border-left-width: " . $res_values['border_left']['responsive'] . "px;";
    if ($style->border_style !== 'none') {
      echo "border-left-style: $style->border_style;";
    }
    if ($style->border_color !== '') {
      echo "border-left-color: #$style->border_color;";
    }
  }
  // end border style declarations.