<?php
// begin border style declarations
if (isset($res_values['border_top']['medium']) && $res_values['border_top']['medium']) {
  echo "border-top-width: " . $res_values['border_top']['medium'] . "px;";
  if ($style->border_style !== 'none') {
    echo "border-top-style: $style->border_style;";
  }
  if ($style->border_color !== '') {
    echo "border-top-color: #$style->border_color;";
  }
}

if (isset($res_values['border_right']['medium']) && $res_values['border_right']['medium']) {
  echo "border-right-width: " . $res_values['border_right']['medium'] . "px;";
  if ($style->border_style !== 'none') {
    echo "border-right-style: $style->border_style;";
  }
  if ($style->border_color !== '') {
    echo "border-right-color: #$style->border_color;";
  }
}

if (isset($res_values['border_bottom']['medium']) && $res_values['border_bottom']['medium']) {
  echo "border-bottom-width: " . $res_values['border_bottom']['medium'] . "px;";
  if ($style->border_style !== 'none') {
    echo "border-bottom-style: $style->border_style;";
  }
  if ($style->border_color !== '') {
    echo "border-bottom-color: #$style->border_color;";
  }
}

if (isset($res_values['border_left']['medium']) && $res_values['border_left']['medium']) {
  echo "border-left-width: " . $res_values['border_left']['medium'] . "px;";
  if ($style->border_style !== 'none') {
    echo "border-left-style: $style->border_style;";
  }
  if ($style->border_color !== '') {
    echo "border-left-color: #$style->border_color;";
  }
}
// end border style declarations.