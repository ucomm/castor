<?php

// begin padding style declarations
if (isset($res_values['padding_top']['responsive']) && $res_values['padding_top']['responsive']) {
  echo "padding-top: " . $res_values['padding_top']['responsive'] . "px;";
} else {
  echo "padding-top: 0px;";
}
if (isset($res_values['padding_right']['responsive']) && $res_values['padding_right']['responsive']) {
  echo "padding-right: " . $res_values['padding_right']['responsive'] . "px;";
} else {
  echo "padding-right: 0px;";
}
if (isset($res_values['padding_bottom']['responsive']) && $res_values['padding_bottom']['responsive']) {
  echo "padding-bottom: " . $res_values['padding_bottom']['responsive'] . "px;";
} else {
  echo "padding-bottom: 0px;";
}
if (isset($res_values['padding_left']['responsive']) && $res_values['padding_left']['responsive']) {
  echo "padding-left: " . $res_values['padding_left']['responsive'] . "px;";
} else {
  echo "padding-left: 0px;";
}

// end padding style declarations.