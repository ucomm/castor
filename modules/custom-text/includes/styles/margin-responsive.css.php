<?php

// begin margin style declarations
if (isset($res_values['margin_top']['responsive']) && $res_values['margin_top']['responsive']) {
  echo "margin-top: " . $res_values['margin_top']['responsive'] . "px;";
} else {
  echo "margin-top: 0px;";
}
if (isset($res_values['margin_right']['responsive']) && $res_values['margin_right']['responsive']) {
  echo "margin-right: " . $res_values['margin_right']['responsive'] . "px;";
} else {
  echo "margin-right: 0px;";
}
if (isset($res_values['margin_bottom']['responsive']) && $res_values['margin_bottom']['responsive']) {
  echo "margin-bottom: " . $res_values['margin_bottom']['responsive'] . "px;";
} else {
  echo "margin-bottom: 0px;";
}
if (isset($res_values['margin_left']['responsive']) && $res_values['margin_left']['responsive']) {
  echo "margin-left: " . $res_values['margin_left']['responsive'] . "px;";
} else {
  echo "margin-left: 0px;";
}

// end margin style declarations.