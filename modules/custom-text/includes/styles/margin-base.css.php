<?php
  // begin margin style declarations
  if (isset($res_values['margin_top']['base']) && $res_values['margin_top']['base']) {
    echo "margin-top: " . $res_values['margin_top']['base'] . "px;";
  } else {
    echo "margin-top: 0px;";
  }

  if (isset($res_values['margin_right']['base']) && $res_values['margin_right']['base']) {
    echo "margin-right: " . $res_values['margin_right']['base'] . "px;";
  } else {
    echo "margin-right: 0px;";
  }

  if (isset($res_values['margin_bottom']['base']) && $res_values['margin_bottom']['base']) {
    echo "margin-bottom: " . $res_values['margin_bottom']['base'] . "px;";
  } else {
    echo "margin-bottom: 0px;";
  }
  
  if (isset($res_values['margin_left']['base']) && $res_values['margin_left']['base']) {
    echo "margin-left: " . $res_values['margin_left']['base'] . "px;";
  } else {
    echo "margin-left: 0px;";
  }

  // end margin style declarations.