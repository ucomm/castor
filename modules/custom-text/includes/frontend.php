<div id='uc-custom-text-container-<?php echo $id; ?>' class='uc-custom-text'>
  <?php
  foreach ($settings->custom_texts as $index => $custom_text) {
    $instance = "custom_text-$id-$index";
    $has_link = filter_var($custom_text->link_url, FILTER_VALIDATE_URL) || preg_match('/tel:[\d+()-]([- 0-9]{1,10})*/', $custom_text->link_url);
    
    switch ($custom_text->text_type) {
      case 'blockquote':
        include($module->dir . 'includes/partials/blockquote.php');
        break;
      case 'p':
        include($module->dir . 'includes/partials/paragraph.php');
        break;
      default:
        include($module->dir . 'includes/partials/paragraph.php');
        break;
    }
  }
  ?>
</div>