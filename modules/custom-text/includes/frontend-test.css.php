<?php

$settings_array = array(
  'medium_breakpoint',
  'responsive_breakpoint'
);

$requested_settings_array = Castor_Helpers::get_bb_settings($settings_array);

$responsive_bp = $requested_settings_array['responsive_breakpoint'];
$medium_bp = $requested_settings_array['medium_breakpoint'];

echo "#uc-custom-text-container-$id {
  text-align: $settings->alignment;
}";

foreach ($settings->custom_texts as $index => $style) {
  // echo "<pre>";
  // var_dump($style);
  // echo "</pre>";
  $has_link = filter_var($style->link_url, FILTER_VALIDATE_URL);
  $instance = "custom_text-$id-$index";
  $mod_css = '';
  if ($has_link) {
    $instance .= " a";
  }

  $mod_css = "#$instance {";

  if ($style->font_background_color) {
    $mod_css .= "background-color: #$style->font_background_color;";
  }









  $mod_css .= "}";



  echo $mod_css;


}