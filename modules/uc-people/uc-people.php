<?php
/**
 * @deprecated Replacing this with the uc-people-compat plugin. keeping this code for historical purposes however it isn't loaded.
 */
class UCPeople extends FLBuilderModule {
  public function __construct()
  {
    parent::__construct([
      'name' => __('UConn People', 'castor'),
      'description' => __('Beaverized People Plugin', 'castor'),
      'category' => __('Castor Modules', 'castor'),
      'dir' => CASTOR_DIR . 'modules/uc-people',
      'url' => CASTOR_URL . 'modules/uc-people',
      'editor_export' => true,
      'enabled' => true,
      'partial_refresh' => false
    ]);
  }

  public function doShortcode() {
    $shortcode = "[ucomm-people ";
    $shortcode .= "break_into_groups='" . $this->settings->break_into_groups . "' ";
    $shortcode .= "persons_per_row='" . $this->settings->persons_per_row . "' ";

    switch ($this->settings->type_select) {
      case 'groups':
        $shortcode .= "specific_groups='" . implode(", ", $this->settings->group_select) . "' ";
        break;
      case 'persontags':
        $shortcode .= "specific_tags='" . implode(", ", $this->settings->person_tag_select) . "' ";
        break;
      case 'people':
        $shortcode .= "specific_people='" . implode(", ", $this->settings->people_select) . "' ";
        break;
      default:
        $shortcode .= "specific_groups='" . implode(", ", $this->settings->group_select) . "' ";
        break;
    }

    $shortcode .= "]";

    return do_shortcode($shortcode);
  }
}