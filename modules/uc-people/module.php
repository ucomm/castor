<?php

class UCPeopleModule extends Castor_Module {
  public $groups;
  public $personTags;
  public $people;
  public function __construct()
  {
    $this->groups = $this->getTermsForSetting('group');
    $this->personTags = $this->getTermsForSetting('persontag');
    $this->people = $this->getPeopleForSetting();
  }
  public function load_module()
  {
    if (class_exists('FLBuilder')) {
      require_once 'uc-people.php';

      FLBuilder::register_module('UCPeople', [
        'tab_1' => [
          'title' => __('General', 'castor'),
          'sections' => [
            'section_1' => [
              'title' => __('Selection', 'castor'),
              'fields' => [
                'type_select' => [
                  'type' => 'select',
                  'label' => __('Choose a type to display', 'castor'),
                  'options' => [
                    'groups' => __('Groups', 'castor'),
                    'persontags' => __('Tags', 'castor'),
                    'people' => __('People', 'castor'),
                  ],
                  'default' => 'groups',
                  'toggle' => [
                    'groups' => [ 
                      'fields' => [ 'group_select' ]
                    ],
                    'persontags' => [
                      'fields' => [ 'person_tag_select' ]
                    ],
                    'people' => [
                      'fields' => [ 'people_select' ]
                    ]
                  ]
                ],
                'group_select' => [
                  'type' => 'select',
                  'label' => __('Groups', 'castor'),
                  'default' => '',
                  'options' => $this->populateTerms($this->groups),
                  'multi-select' => true
                ],
                'person_tag_select' => [
                  'type' => 'select',
                  'label' => __('Tags', 'castor'),
                  'default' => '',
                  'options' => $this->populateTerms($this->personTags),
                  'multi-select' => true
                ],
                'people_select' => [
                  'type' => 'select',
                  'label' => __('People', 'castor'),
                  'default' => '',
                  'options' => $this->people,
                  'multi-select' => true
                ]
              ]
            ],
            'section_2' => [
              'title' => __('Layout', 'castor'),
              'fields' => [
                'break_into_groups' => [
                  'type' => 'select',
                  'label' => __('Break into groups?', 'castor'),
                  'description' => __('If enabled, this will separate people into groups by taxonomy. NB - This may result in people being duplicated on the page.'),
                  'default' => 'false',
                  'options' => [
                    'false' => __('No', 'castor'),
                    'true' => __('Yes', 'castor')
                  ]
                ],
                'persons_per_row' => [
                  'type' => 'unit',
                  'label' => __('Persons per row', 'castor'),
                  'default' => 3,
                  'min' => 1,
                  'max' => 6,
                  'slider' => [
                    'min' => 1,
                    'max' => 6
                  ]
                ]
              ]
            ]
          ]
        ]
      ]);
    }
  }

  public function populateTerms(array $terms): array {
    $options = [ '' => __('Select a Group/Tag', 'castor') ];

    foreach ($terms as $t) {
      $options[$t->slug] = __($t->name, 'castor');
    }

    return $options;
  }

  /**
   * Beaver Builder and WP are ridiculous...
   * Direct DB access is required because of when and how BB loads modules.
   * The `uc-people` plugin also doesn't expose its custom taxonomies to the REST API. So you can't get results that way either.
   *
   * @param string $taxonomy
   * @return array
   */
  private function getTermsForSetting(string $taxonomy): array {
    global $wpdb;

    $terms = [];

     $results = $wpdb->get_results(
      $wpdb->prepare(
        "SELECT {$wpdb->prefix}terms.term_id, {$wpdb->prefix}terms.name, {$wpdb->prefix}terms.slug FROM {$wpdb->prefix}terms
          JOIN {$wpdb->prefix}term_taxonomy ON ({$wpdb->prefix}term_taxonomy.term_id = {$wpdb->prefix}terms.term_id)
          WHERE 1 = 1
          AND {$wpdb->prefix}term_taxonomy.taxonomy = %s
          ORDER BY {$wpdb->prefix}terms.slug ASC",
          $taxonomy
      )
    );

    if (null !== $results) {
      $terms = $results;
    }

    return $terms;
  }

  private function getPeopleForSetting(): array {
    $options = [ '' => __('Select People', 'castor' )];
    $people = get_posts([
      'post_type' => 'person',
      'posts_per_page' => -1,
      'meta_key' => 'last_name',
      'orderby' => 'meta_value',
      'order' => 'ASC'
    ]);
    $names = wp_list_pluck($people, 'post_title');
    foreach ($names as $n) {
      $options[$n] = __($n, 'castor');
    }
    return $options;
  }
}