<?php

$icon_array = $settings->uconn_ir_repeater;
$columns = 12 / count($icon_array);
if (!empty($icon_array)) {
?>
  <ul id="uc_ir_list<?php echo $id; ?>" class="uc-ir-list">
    <?php
      foreach ($icon_array as $index => $icon_item) {
        $instance = $id . "_" . $index;
    ?>
        <li id="uc_ir_list_item_<?php echo $instance; ?>" class="uc-ir-list-item col-md-<?php echo $columns; ?>">
          <a href="<?php echo $icon_item->uconn_ir_item_link ?>" id="uc_ir_link_<?php echo $instance; ?>" class="uc-ir-link">
            <i id="uc_ir_icon_<?php echo $instance; ?>" class="<?php echo $icon_item->uconn_ir_item_icon; ?> uc-ir-icon"></i>
            <p id="uc_ir_link_text_<?php echo $instance; ?> class="uc-ir-link-text"><?php echo $icon_item->uconn_ir_item_text; ?></p>
          </a>
        </li>
    <?php
      }
    ?>
  </ul>
<?php  
}