<?php

$icon_array = $settings->uconn_ir_repeater;

if (!empty($icon_array)) {
  foreach ($icon_array as $index => $icon_item) {
    $instance = $id . "_" . $index;
?>
    #uc_ir_link_<?php echo $instance; ?> {
      color: #<?php echo $icon_item->uconn_ir_color ?>;
    }
    #uc_ir_link_<?php echo $instance; ?>:hover {
      color: #<?php echo $icon_item->uconn_ir_hover_color ?>;
    }
<?php
  }
}
?>

.uc-ir-icon {
  font-size: <?php echo $settings->uconn_ir_size; ?>px;
  line-height: 1.5;
}