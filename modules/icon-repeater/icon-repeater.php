<?php

class IconRepeater extends FLBuilderModule {
  public function __construct() {
    parent::__construct(array(
      'name' => __('Icon Repeater', 'castor'),
      'description' => __('Display Icons with text and optional link', 'castor'),
      'category' => __('Castor Modules', 'castor'),
      'dir' => CASTOR_DIR . 'modules/icon-repeater/',
      'url' => CASTOR_URL . 'modules/icon-repeater/',
      'editor_export' => true, // Defaults to true and can be omitted.
      'enabled' => true, // Defaults to true and can be omitted.
      'partial_refresh' => false, // Defaults to false and can be omitted.
    ));
  }
}