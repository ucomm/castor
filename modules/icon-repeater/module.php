<?php

class IconRepeaterModule extends Castor_Module {
  public function load_module() {
    if (class_exists('FLBuilder')) {
      require_once 'icon-repeater.php';
      FLBuilder::register_module('IconRepeater', array(
        'tab_1' => array(
          'title' => __('Icon Repeater', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Icons', 'castor'),
              'fields' => array(
                'uconn_ir_repeater' => array(
                  'type' => 'form',
                  'label' => __('Icon', 'castor'),
                  'form' => 'uconn_ir_form',
                  'preview_text' => 'uconn_ir_item_text',
                  'multiple' => true
                )
              )
            )
          )
        ),
        'tab_2' => array(
          'title' => __('Style', 'castor'),
          'sections' => array(
            
            'section_2' => array(
              'title' => __('Structure', 'castor'),
              'fields' => array(
                'uconn_ir_size' => array(
                  'type' => 'text',
                  'label' => __('Icon Size', 'castor'),
                  'default' => '50',
                  'maxlength' => '3',
                  'size' => '4',
                  'description' => 'px'
                ),
                'uconn_ir_alignment' => array(
                  'type'          => 'select',
                  'label'         => __('Alignment', 'fl-builder'),
                  'default'       => 'center',
                  'options'       => array(
                    'center'        => __('Center', 'fl-builder'),
                    'left'          => __('Left', 'fl-builder'),
                    'right'         => __('Right', 'fl-builder')
                  )
                )
              )
            )
          )
        )
      ));

      FLBuilder::register_settings_form('uconn_ir_form', array(
        'title' => __('Edit Icon Item', 'castor'),
        'tabs' => array(
          'tab_1' => array(
            'title' => __('Icon Item', 'castor'),
            'sections' => array(
              'section_1' => array(
                'title' => __('Icon Item', 'castor'),
                'fields' => array(
                  'uconn_ir_item_icon' => array(
                    'type' => 'icon',
                    'label' => __('Icon', 'castor'),
                    'show_remove' => true
                  ),
                  'uconn_ir_item_link' => array(
                    'type' => 'link',
                    'label' => __('Item Link', 'castor')
                  ),
                  'uconn_ir_item_text' => array(
                    'type' => 'text',
                    'maxlength' => '100',
                    'default' => 'Description',
                    'label' => __('Description', 'castor')
                  )
                )
              )
            )
          ),
          'tab_2' => array(
            'title' => __('Style', 'castor'),
            'sections' => array(
              'section_1' => array(
                'title' => __('Colors', 'castor'),
                'fields' => array(
                  'uconn_ir_color' => array(
                    'type' => 'color',
                    'label' => __('Color', 'castor'),
                    'show_reset' => true,
                    'default' => '000000'
                  ),
                  'uconn_ir_hover_color' => array(
                    'type' => 'color',
                    'label' => __('Hover Color', 'castor'),
                    'show_reset' => true,
                    'default' => '777777'
                  ),
                  'uconn_ir_bg_color' => array(
                    'type' => 'color',
                    'label' => __('Background Color', 'castor'),
                    'show_reset' => true,
                    'default' => 'ffffff'
                  ),
                  'uconn_ir_bg_hover_color' => array(
                    'type' => 'color',
                    'label' => __('Background Hover Color', 'castor'),
                    'show_reset' => true,
                    'default' => 'ffffff'
                  )
                )
              )
            )
          )
        )
      ));
    }
  }
}