<?php

class SyntaxHighlighterModule extends Castor_Module {
  public function load_module() {
    if (class_exists('FLBuilder')) {
      require_once 'syntax-highlighter.php';
      FLBuilder::register_module('SyntaxHighlighter', array(
        'tab_1' => array(
          'title' => __('Code', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Language', 'castor'),
              'fields' => array(
                'language' => array(
                  'type' => 'select',
                  'label' => __('Language', 'castor'),
                  'default' => 'markup',
                  'options' => array(
                    'apache' => __('apache', 'castor'),
                    'bash' => __('bash', 'castor'),
                    'css' => __('CSS', 'castor'),
                    'docker' => __('Docker', 'castor'),
                    'git' => __('git', 'castor'),
                    'graphql' => __('GraphQL', 'castor'),
                    'javascript' => __('javascript', 'castor'),
                    'json' => __('JSON', 'castor'),
                    'less' => __('LESS', 'castor'),
                    'markdown' => __('markdown', 'castor'),
                    'markup' => __('HTML', 'castor'),
                    'nginx' => __('NGINX', 'castor'),
                    'php' => __('PHP', 'castor'),
                    'react-jsx' => __('react', 'castor'),
                    'sass' => __('Sass', 'castor'),
                    'sql' => __('sql', 'castor'),
                    'yaml' => __('yaml', 'castor')
                  )
                ),
                'size' => array(
                  'type' => 'select',
                  'label' => __('Size', 'castor'),
                  'description' => __('Use this to set the width of the highlighter depending on how large the example is', 'castor'),
                  'default' => '37.5em',
                  'options' => array(
                    '37.5em' => __('Small', 'castor'),
                    '62em' => __('Medium', 'castor'),
                    '75em' => __('Large', 'castor')
                  )
                )
              )
            ),
            'section_2' => array(
              'title' => __('Code', 'castor'),
              'fields' => array(
                'code' => array(
                  'type' => 'code',
                  // leave the editor property empty for now...
                  // the only choices are html, css, and javascript, but that will break the module.
                  'editor' => '',
                  'label' => __('Code Example', 'castor')
                )
              )
            )
          )
        ),
        'tab_2' => array(
          'title' => __('Style', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Style', 'castor'),
              'fields' => array(
                'theme' => array(
                  'type' => 'select',
                  'label' => __('Highlighter Theme', 'castor'),
                  'default' => 'uconn',
                  'options' => array(
                    'default' => __('Default', 'castor'),
                    'okaida' => __('Okaida', 'castor'),
                    'solarized-dark' => __('Solarized Dark', 'castor'),
                    'solarized-light' => __('Solarized Light', 'castor'),
                    'uconn' => __('UConn', 'castor')
                  )
                ),
              )
            )
          )
        )
      ));
    }
  }
}


