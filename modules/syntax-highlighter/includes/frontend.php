<div id="syntax-highlighter-<?php echo $id; ?>" class="bb-syntax-highlighter">
    <pre class="line-numbers">
        <code class="language-<?php echo $settings->language; ?>">
          <?php echo htmlspecialchars( $settings->code ); ?>
        </code>
    </pre>
</div>