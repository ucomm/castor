<?php

class SyntaxHighlighter extends FLBuilderModule {
  public function __construct() {
    parent::__construct(array(
      'name' => __('Syntax Highlighter', 'castor'),
      'description' => __('Creates a syntax highlighted code block', 'castor'),
      'category' => __('Castor Modules', 'castor'),
      'dir' => CASTOR_DIR . 'modules/syntax-highlighter/',
      'url' => CASTOR_URL . 'modules/syntax-highlighter/',
      'editor_export'   => true, // Defaults to true and can be omitted.
      'enabled'         => true, // Defaults to true and can be omitted.
      'partial_refresh' => false, // Defaults to false and can be omitted.
    ));

    // enqueue the default prism js.
    $this->add_js('prism', $this->url . 'js/prism/prism.js', array(), null, true);
  }
  public function enqueue_scripts() {

    if (!$this->settings) return;

    // set the theme styles
    switch ($this->settings->theme) {
      case 'default':
        $this->add_css('prism-default', $this->url . 'css/themes/default.css');
        break;
      case 'okaida':
        $this->add_css('prism-okaida', $this->url . 'css/themes/okaida.css');
        break;
      case 'solarized-light':
        $this->add_css('prism-solarized-light', $this->url . 'css/themes/solarized-light.css');
        break;
      case 'solarized-dark':
        $this->add_css('prism-solarized-dark', $this->url . 'css/themes/solarized-dark.css');
        break;
      default:
        $this->add_css('prism-uconn', $this->url . 'css/themes/uconn.css');
        break;
    }

    // set the language
    if ($this->settings->language) {
      $this->add_js('prism-' . $this->settings->language . '-language', $this->url . 'js/prism/languages/' . $this->settings->language . '.js', array('prism'), null, true);
    }
  }
}