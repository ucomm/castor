<?php

/*
*
* A module to display a basic callout with title and information.
*
*/

class CircularCalloutModule extends Castor_Module {
  public function load_module() {
    if (class_exists('FLBuilder')) {
      require_once 'circular-callout.php';
      FLBuilder::register_module('CircularCallout', array(
        'tab_1' => array(
          'title' => __('Circles', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Circles', 'castor'),
              'fields' => array(
                'uconn_cc_info' => array(
                  'type' => 'form',
                  'label' => __('Circle', 'castor'),
                  'form' => 'uconn_cc_form',
                  'preview_text' => 'uconn_cc_item_title',
                  'multiple' => true
                )
              )
            )
          )
        ),
        'tab_2' => array(
          'title' => __('Circle Position', 'castor'),
          'description' => __('IMPORTANT - Set the margins the same for every module of this type. The advanced tab settings will not separate the circles from each other.', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Alignment', 'castor'),
              'fields' => array(
                'uconn_cc_alignment' => array(
                  'type' => 'select',
                  'label' => __('Starting Position', 'castor'),
                  'default' => 'flex-start',
                  'options' => array(
                    'flex-start' => __('Left', 'castor'),
                    'center' => __('Center', 'castor'),
                    'flex-end' => __('Right', 'castor')
                  )
                )
              )
            ),
            'section_2' => array(
              'title' => __('Margins', 'castor'),
              'fields' => array(
                'uconn_cc_margins' => array(
                  'type' => 'dimension',
                  'label' => 'Circle Margins',
                  'description' => 'px',
                  'slider' => array(
                    'min' => 0,
                    'max' => 100,
                    'step' => 1
                  )
                )
              )
            )
          )
        )
      ));

      FLBuilder::register_settings_form('uconn_cc_form', array(
        'title' => __('Edit Item', 'castor'),
        'tabs' => array(
          'general' => array(
            'title' => __('General', 'castor'),
            'sections' => array(
              'title' => array(
                'title' => __('Content', 'castor'),
                'fields' => array(
                  'uconn_cc_item_title' => array(
                    'type' => 'text',
                    'maxlength' => '40',
                    'default' => 'Title',
                    'label' => __('Title', 'castor'),
                    'description' => __('The main idea for the callout')
                  ),
                )
              ),
              'content' => array(
                'title' => __('Content', 'castor'),
                'fields' => array(
                  'uconn_cc_item' => array(
                    'type' => 'editor',
                    'label' => __('Item text', 'castor'),
                  ),
                )
              )
            )
          ),
          'tab_2' => array(
            'title' => __('Style', 'castor'),
            'sections' => array(
              'general' => array(
                'title' => __('Module style', 'castor'),
                'fields' => array(
                  'uconn_cc_size' => array(
                    'type' => 'text',
                    'label' => __('Module size', 'castor'),
                    'maxlength' => '4',
                    'default' => '250',
                  ),
                  'uconn_cc_background_color' => array(
                    'type' => 'color',
                    'label' => __('Background Color', 'castor'),
                    'default' => '#ffffff',
                    'show_reset' => true
                  ),
                  'uconn_cc_background_opacity' => array(
                    'type' => 'text',
                    'label' => __('Background Opacity', 'castor'),
                    'maxlength' => '4',
                    'default' => '0.5',
                    'description'=> __('Set the opacity for the background', 'castor')
                  ),
                  'uconn_cc_content_vertical_position' => array(
                    'type' => 'unit',
                    'label' => __('Vertical Position', 'castor'),
                    'description' => __('Tweak the vertical position of the content', 'castor'),
                    'slider' => array(
                      'min' => 0,
                      'max' => 100,
                      'step' => 1
                    )
                  )
                  // 'uconn_cc_content_vertical_position' => array(
                  //   'type' => 'text',
                  //   'label' => __('Vertical Position', 'castor'),
                  //   'description' => __('Set the position of the text within the circle. Negative values move text up, positive values move text down.', 'castor'),
                  //   'maxlength' => '4',
                  //   'default' => '-50'
                  // )
                )
              ),
              'typography' => array(
                'title' => __('Typography', 'castor'),
                'fields' => array(
                  'uconn_cc_alignment' => array(
                    'type' => 'align',
                    'label' => __('Text Alignment', 'castor'),
                    'default' => 'center',
                  ),
                  'uconn_cc_title_size' => array(
                    'type' => 'unit',
                    'label' => __('Title font size', 'castor'),
                    'description' => 'px',
                    'default' => '30'
                  ),
                  'uconn_cc_title_font' => array(
                    'type' => 'font',
                    'label' => __('Title font', 'castor'),
                    'default' => array(
                      'family' => 'Georgia',
                      'weight' => 300
                    )
                  ),
                  'uconn_cc_title_color' => array(
                    'type' => 'color',
                    'label' => __('Title Color', 'castor'),
                    'default' => 'ffffff',
                    'show_reset' => true
                  ),
                  'uconn_cc_item_size' => array(
                    'type' => 'unit',
                    'label' => __('Content font size', 'castor'),
                    'description' => 'px',
                    'default' => '30'
                  ),
                  'uconn_cc_item_font' => array(
                    'type' => 'font',
                    'label' => __('Content font', 'castor'),
                    'default' => array(
                      'family' => 'Georgia',
                      'weight' => 300
                    )
                  ),
                  'uconn_cc_item_color' => array(
                    'type' => 'color',
                    'label' => __('Content Color', 'castor'),
                    'default' => '000000',
                    'show_reset' => true
                  )  
                )
              )
            )
          )
        )      
      ));
    }
  }
}
