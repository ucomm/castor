<?php

$settings_array = array(
  'row_width',
  'medium_breakpoint',
  'responsive_breakpoint'
);

$requested_settings_array = Castor_Helpers::get_bb_settings($settings_array);

$global_row_width = $requested_settings_array['row_width'];
$medium_breakpoint = $requested_settings_array['medium_breakpoint'];
$responsive_breakpoint = $requested_settings_array['responsive_breakpoint'];

?>

#uc_cc_list_<?php echo $id; ?> {
  justify-content: <?php echo $settings->uconn_cc_alignment; ?>;
}

.uc-cc-list-item {
  margin-top: <?php echo $settings->uconn_cc_margins_top; ?>px;
  margin-right: <?php echo $settings->uconn_cc_margins_right; ?>px;
  margin-bottom: <?php echo $settings->uconn_cc_margins_bottom; ?>px;
  margin-left: <?php echo $settings->uconn_cc_margins_left; ?>px;
}



<?php
// Apply differnt styles to each circle.

$circular_callout_array = $settings->uconn_cc_info;

if (!empty($circular_callout_array)) {
  foreach ($circular_callout_array as $index => $circle) {
    $instance = $id . "_" . $index;

?>

    #uc_info_container_<?php echo $instance ?> {
      height: <?php echo $circle->uconn_cc_size; ?>px;
      position: relative;
      width: <?php echo $circle->uconn_cc_size; ?>px;
    }

    #uc_info_container_<?php echo $instance ?>:before {
      content: '';
      background-color: rgba(<?php echo Castor_Helpers::convert_hex_to_rgb($circle->uconn_cc_background_color) . ', ' . $circle->uconn_cc_background_opacity; ?>);
      position: absolute;
      top: 0;
      right: 0;
      left: 0;
      bottom: 0;
    }


    #uc_info_content_<?php echo $instance ?> {
      max-width: <?php echo ($circle->uconn_cc_size - 50); ?>px;
      text-align: <?php echo $circle->uconn_cc_alignment; ?>;
      top: <?php echo $circle->uconn_cc_content_vertical_position; ?>px;
    }




    #uc_info_title_<?php echo $instance ?> {
      color: #<?php echo $circle->uconn_cc_title_color; ?>;
      font-family: <?php echo $circle->uconn_cc_title_font->family; ?>;
      font-weight: <?php echo $circle->uconn_cc_title_font->weight; ?>;
      font-size: <?php echo $circle->uconn_cc_title_size; ?>px;
    }
    #uc_item_content_container_<?php echo $instance; ?> * {
      color: #<?php echo $circle->uconn_cc_item_color; ?>;
      font-family: <?php echo $circle->uconn_cc_item_font->family; ?>;
      font-weight: <?php echo $circle->uconn_cc_item_font->weight; ?>;
      font-size: <?php echo $circle->uconn_cc_item_size; ?>px;
    }
<?php
  }
}
?>

@media screen and (max-width: <?php echo $medium_breakpoint; ?>px) {
  .fl-module-circular-callout > .fl-module-content {
    text-align: center;
  }
}

@media screen and (max-width: <?php echo $responsive_breakpoint; ?>px) {
  .uc-cc-list-item {
    margin-right: 0;
    margin-left: 0;
  }
}
