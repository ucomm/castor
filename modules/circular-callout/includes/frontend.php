<?php
// A callout module with info that can be different sizes.

$circular_callout_array = $settings->uconn_cc_info;
if (!empty($circular_callout_array)) {
?>
  <ul id="uc_cc_list_<?php echo $id; ?>" class="uc-cc-list">
  
    <?php
      foreach ($circular_callout_array as $index => $circle) {
        $instance = $id . "_" . $index;
    ?>
      <li id="uc_cc_list_item_<?php echo $instance; ?>" class="uc-cc-list-item">
        <div id="uc_info_container_<?php echo $instance; ?>" class="uc-info-container">
          <div id="uc_info_content_<?php echo $instance; ?>" class="uc-info-content">
            <p>
              <span id="uc_info_title_<?php echo $instance; ?>" class="uc-info-title"><?php echo $circle->uconn_cc_item_title; ?></span>
            </p>
            <div class="uc-item-content-container" id="uc_item_content_container_<?php echo $instance; ?>">
              <?php echo $circle->uconn_cc_item; ?> 
            </div>
          </div>
        </div>
      </li>
    <?php
      }
    ?>
  </ul> 
<?php
  }
?>

