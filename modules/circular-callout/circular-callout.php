<?php

class CircularCallout extends FLBuilderModule {
  public function __construct() {
    parent::__construct(array(
      'name' => __('Circular Callout', 'castor'),
      'description' => __('Display a callout with title and info.', 'castor'),
      'category' => __('Castor Modules', 'castor'),
      'dir' => CASTOR_DIR . 'modules/circular-callout/',
      'url' => CASTOR_URL . 'modules/circular-callout/',
      'editor_export'   => true, // Defaults to true and can be omitted.
      'enabled'         => true, // Defaults to true and can be omitted.
      'partial_refresh' => true, // Defaults to false and can be omitted.
    ));
  }
}