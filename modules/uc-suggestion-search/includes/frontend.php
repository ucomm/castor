<?php

wp_localize_script('main', 'settings', array(
	'keywords' => $settings->keywords
));

?>
<form id="search-form" action="#" method="post">
	<label for="search-text" class="visually-hidden">Search:</label>
	<input type="text" id="search-text" name="search-text" placeholder="<?php echo $settings->placeholder; ?>" autocomplete="off" aria-owns="search-results" aria-expanded="false" aria-autocomplete="both" aria-activedescendant="" />
	<label for="search-results-button-open" class="visually-hidden">Show Results:</label>
	<button id="search-results-button-open">
		<div>
			<div class="arrow-up"></div>
			<div class="arrow-down"></div>
		</div>
	</button>
	<ul id="search-results"><li id="dymText"><a id="dymSuggestion" href="#" onclick="suggestionClick()"></a></li></ul>
	<!-- <p id="dymText">Did you mean <a id="dymSuggestion" href="#" onclick="suggestionClick()">...</a>?</p> -->
</form>