<?php

class UCSuggestionSearch extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'UC Suggestion Search', 'castor' ),
            'description'     => __( 'Displays a search dropdown.', 'castor' ),
            'category'        => __( 'Castor Modules', 'castor' ),
            'dir'             => CASTOR_DIR . 'modules/search/',
            'url'             => CASTOR_URL . 'modules/search/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
		
		$this->add_js('main', CASTOR_URL . 'modules/search/js/main.js', array('jquery'), null, true);
		
    }

	
	
}