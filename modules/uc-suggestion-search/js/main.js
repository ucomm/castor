var suggestion = {word: '', distance: 0,entry: ''};
var suggestionTimer;
var dymHTML = jQuery('<li id="dymText"><a id="dymSuggestion" href="#" onclick="suggestionClick()"></a></li>');
var haveSuggestion = true;
var prevSearch = '';

// document.addEventListener('DOMContentLoaded', function() {
// 	FillSearchResults();
// })

jQuery('#search-text').keydown(function(){
	var searchText = jQuery("#search-text").val();
	clearTimeout(suggestionTimer);
	var searchText = jQuery("#search-text").val();
	if(searchText != prevSearch || searchText == suggestion.word){
		suggestion.word = '';
		suggestion.distance = 0;
	}
		
})

jQuery('#search-text').keyup(function(){
	var searchText = jQuery("#search-text").val();
	if(searchText != prevSearch){
		suggestion.word = '';
		suggestion.distance = 0;
		document.getElementById("dymSuggestion").innerHTML = '';
		document.getElementById("dymText").innerHTML = '<a id="dymSuggestion" href="#" onclick="suggestionClick()"></a>';
		prevSearch = searchText;
	}
	clearTimeout(suggestionTimer);
	if (searchText.length >= 3)
	{
		suggestionTimer = setTimeout(DidYouMean,0500);
	}

	FillSearchResults();
	jQuery('#search-results').show();
});

jQuery('#search-results-button-open').click(function(){
	if (jQuery('#search-results').css('display') == 'none'){
		jQuery('#search-results').show();
	}
	else {
		jQuery('#search-results').hide();
	}
	return false;
});

jQuery(document).click(function(event){
	if (jQuery(event.target).closest('#search-results').length == 0){
		jQuery('#search-results').hide();
	}
});

jQuery(document).keydown(function(event){
	
	if (jQuery('#search-results').css('display') == 'block'){
		
		var key = event.which;

		/* Up Arrow Key */
		if (key == 38){
			if (jQuery('#search-results a:focus').length == 0){
				jQuery('#search-results a:first').focus();
			}
			else {
				jQuery('#search-results a:focus').closest('li').prev().children('a').focus();
			}
			return false;
		}

		/* Down Arrow Key */
		else if (key == 40){

			if (jQuery('#search-results a:focus').length == 0){

				jQuery('#search-results a:first').focus();

			}
			else {

				jQuery('#search-results a:focus').closest('li').next().children('a').focus();

			}
			return false;
		}
	}
});

function FillSearchResults(){
	var searchText = jQuery("#search-text").val();
	var searchResults = [];
	jQuery.each(settings.keywords, function(index, keyword){
		var re = new RegExp(searchText, 'i');
		if (keyword.keyword.search(re) != -1) searchResults.push(keyword);
		//Add keyword to search results if the regular expression of the search text was in keyword.keyword.search
		jQuery.each(keyword.aliases, function(index, alias){
			if (alias.search(re) != -1){
				var keyword_copy = jQuery.extend({}, keyword);
				keyword_copy.searchedAlias = alias;
				searchResults.push(keyword_copy);
				//If the reg. exp. of the search text matched an alias of a keyword, add that keyword to search results
			}
		})
	})
	jQuery('#search-results').empty(); //empty the html element #search-results, the dropdown list of results
	jQuery('#search-results').append(dymHTML); //Add the "did you mean" suggestion as the first item in the list
	if (haveSuggestion){
		document.getElementById("dymSuggestion").innerHTML = suggestion.word;
	}
	if (searchResults.length > 0){
		var searchResultsKeywordsUsed = [];
		jQuery.each(searchResults, function(index, result){
			if (jQuery.inArray(result.keyword, searchResultsKeywordsUsed) == -1){
				var listItem = jQuery('<li><a href="' + result.url + '">' + result.keyword + '</a></li>');
				searchResultsKeywordsUsed.push(result.keyword);
				//if (result.searchedAlias) listItem.children('a').append('<span> (' + result.searchedAlias + ')</span>');
				jQuery('#search-results').append(listItem);
			}
		});
	}
	

}

function suggestionClick() {
	document.getElementById("search-text").value = document.getElementById("dymSuggestion").innerHTML;
	FillSearchResults();
	jQuery('#search-results').show();
}

function DidYouMean(){
	var searchText = jQuery("#search-text").val();
	
	suggestion.entry = searchText;
	suggestion.word = '';
	suggestion.distance=0;
	if (haveSuggestion){
		document.getElementById("dymSuggestion").innerHTML = '';
	}
	jQuery.each(settings.keywords, function(index, keyword){
		if(suggestion.word == ''){
			suggestion.word = keyword.keyword;
			suggestion.distance = levenshtein(searchText,keyword.keyword);
		}
		else{
			keyDist = levenshtein(searchText,keyword.keyword);
			if(keyDist < suggestion.distance) {
				suggestion.word = keyword.keyword;
				suggestion.distance = keyDist
			}
		}
	})
	if (suggestion.distance <= (Math.min(searchText.length,suggestion.word.length)+2) * 0.8){
		if(suggestion.word !== searchText && suggestion.distance != 0){
			haveSuggestion = true;
			document.getElementById("dymText").innerHTML = 'Did you mean:<a id="dymSuggestion" href="#" onclick="suggestionClick()"></a>';
			document.getElementById("dymSuggestion").innerHTML = suggestion.word;
		}
		else{
			document.getElementById("dymText").innerHTML = '<a id="dymSuggestion" href="#" onclick="suggestionClick()"></a>';
			document.getElementById("dymSuggestion").innerHTML = '';
		}
	}
	else{
		haveSuggestion = false;
		document.getElementById("dymText").innerHTML = 'Sorry, no results found <a id="dymSuggestion" href="#" onclick="suggestionClick()"></a>';
	}
}

//This version of DidYouMean() searches through aliases and keywords, rather than just keywords, to find a suggestion:
// function DidYouMean(){
// 	var searchText = jQuery("#search-text").val();
// 	suggestion.entry = searchText;
// 	suggestion.word = '';
// 	suggestion.distance=0;
// 	if (haveSuggestion){
// 		document.getElementById("dymSuggestion").innerHTML = '';
// 	}
// 	jQuery.each(settings.keywords, function(index, keyword){
// 		jQuery.each(keyword.aliases, function(index,alias){
// 			if(suggestion.word == ''){
// 				suggestion.word = alias;
// 				suggestion.distance = levenshtein(searchText,alias);
// 			}
// 			else{
// 				keyDist = levenshtein(searchText,alias);
// 				if(keyDist < suggestion.distance) {
// 					suggestion.word = alias;
// 					suggestion.distance = keyDist
// 				}
// 			}
// 		})
// 	})
// 	if (suggestion.distance <= 7){
// 		if(suggestion.word !== searchText && suggestion.distance != 0){
// 			haveSuggestion = true;
// 			document.getElementById("dymText").innerHTML = 'Did you mean:<a id="dymSuggestion" href="#" onclick="suggestionClick()"></a>';
// 			document.getElementById("dymSuggestion").innerHTML = suggestion.word;
// 		}
// 		else{
// 			document.getElementById("dymText").innerHTML = '<a id="dymSuggestion" href="#" onclick="suggestionClick()"></a>';
// 			document.getElementById("dymSuggestion").innerHTML = '';
// 		}
// 	}
// 	else{
// 		haveSuggestion = false;
// 		document.getElementById("dymText").innerHTML = "Sorry, no results found";
// 	}
// 	}
// }

function levenshtein(a,b) {
	if(a.length === 0) return b.length;
	if(b.length ===0) return a.length;
	var mat = [];
	var i;
	for(i=0; i <= b.length; i++){
		mat[i] = [i];
	}
	var j;
	for(j=0; j <= a.length; j++){
		mat[0][j] = j;
	}
	for(i=1; i <= b.length; i++){
		for(j=1; j <= a.length; j++){
			if(b[i-1] == a[j-1]){
				mat[i][j] = mat[i-1][j-1];
			}
			else{
				mat[i][j] = Math.min(mat[i-1][j-1]+1, Math.min(mat[i][j-1]+1, mat[i-1][j]+1));
			}
		}
	}
	return mat[b.length][a.length];
}