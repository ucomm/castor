<?php

class UCSuggestionSearchModule extends Castor_Module {

    private $version = '1.0';

    public function load_module() {
		
        if (class_exists('FLBuilder')){
			
            require_once 'uc-suggestion-search.php';
			
            FLBuilder::register_module('UC Suggestion Search', array(
                'settings' => array(
                    'title' => __( 'Settings', 'castor' ),
                    'sections' => array(
                        'general' => array(
                            'title' => __( 'General', 'castor' ),
                            'fields' => array(
								'placeholder' => array(
									'type' => 'text',
									'label' => __('Placeholder', 'castor')
								)
							)
						),
						'keywords' => array(
							'title' => __('Keywords', 'castor'),
							'fields' => array(
                                'keywords' => array(
                                    'type' => 'form',
                                    'label' => __('Keyword', 'castor'),
									'form'	=> 'keyword_form',
									'preview_text' => 'keyword',
                                    'multiple' => true
                                )
                            )
                        )
                    )
                ),
            ));
			
			FLBuilder::register_settings_form('keyword_form', array(
                'title' => __('Keyword Settings', 'castor'),
                'tabs' => array(
                    'rows' => array(
                        'title' => __('Keyword', 'castor'),
                        'sections' => array(
                            'general' => array(
								'title' => __('General', 'castor'),
                                'fields' => array(
                                    'keyword' => array(
                                        'type' => 'text',
                                        'label' => __('Keyword', 'castor')
                                    ),
                                    'url' => array(
                                        'type' => 'text',
                                        'label' => __('URL', 'castor')
                                    )
								)
							),
							'aliases' => array(
								'title' => __('Aliases', 'castor'),
								'fields' => array(
                                    'aliases' => array(
                                        'type' => 'text',
                                        'label' => __('Alias', 'castor'),
										'multiple' => true
                                    )
                                )
                            )
                        )
                    )
                )
            ));
			
		}
			
    }
	
}
