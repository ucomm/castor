<?php
/*
*
*  A module to display a large hero image with quotation.
*
 */

class HeroImageWithQuoteModule extends Castor_Module {
  public function load_module() {
    if (class_exists('FLBuilder')) {
      require_once 'hero-image-with-quote.php';
      FLBuilder::register_module('HeroImageWithQuote', array(
        'tab_1' => array(
          'title' => __('Background', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Background', 'castor'),
              'fields' => array(
                'uconn_hwq_image' => array(
                  'type' => 'photo',
                  'label' => __('Hero Image', 'castor'),
                  'show_remove' => true,
                  'description' => __('The image to display as a background', 'castor'),
                  'class' => 'uc-hero'
                ),
                'uconn_hwq_image_height' => array(
                  'type' => 'text',
                  'label' => __('Image Height', 'castor'),
                  'maxlength' => '5',
                  'description' => __('Set the height of the image', 'castor')
                ),
                'uconn_hwq_image_height_unit' => array(
                  'type' => 'select',
                  'label' => __('Height Unit', 'castor'),
                  'default' => 'px',
                  'options' => array(
                    'px' => __('Pixels', 'castor'),
                    'rem' => __('REM', 'castor'),
                    'em' => __('EM', 'castor'),
                    '%' => __('%', 'castor')
                  )
                )
              )
            )
          )
        ),
        'tab_2' => array(
          'title' => __('Content', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Quote and Attribution', 'castor'),
              'fields' => array(
                'uconn_hwq_quote' => array(
                  'type' => 'editor',
                  'label' => __('Quote', 'castor'),
                  'default' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
                ),
                'uconn_hwq_quote_color' => array(
                  'type' => 'color',
                  'label' => __('Quote Color', 'castor'),
                  'default' => '000000',
                  'show_reset' => true
                ),
                'uconn_hwq_quote_font_size' => array(
                  'type' => 'text',
                  'label' => __('Font Size', 'castor'),
                  'maxlength' => '5',
                  'description' => __('Set the font size of the quote in pixels.', 'castor')
                ),
                'uconn_hwq_attribution' => array(
                  'type' => 'text',
                  'label' => __('Attribution', 'castor'),
                  'default' => 'Sam Smith',
                  'class' => 'uc-quote-attribution'
                ),
                'uconn_hwq_attribution_color' => array(
                  'type' => 'color',
                  'label' => __('Attribution Color', 'castor'),
                  'default' => '000000',
                  'show_reset' => true
                ),
              )
            ),
            // 'section_2' => array(
            //   'title' => __('Layout', 'castor'),
            //   'fields' => array(
            //     'uconn_hwq_quote_position' => array(
            //       'type' => 'select',
            //       'label' => __('Quote position', 'castor'),
            //       'default' => 'top_left',
            //       'options' => array(
            //         'top_left' => __('Top Left', 'castor'),
            //         'top_right' => __('Top Right', 'castor'),
            //         'center' => __('Center', 'castor'),
            //         'center_left' => __('Center - Left Justified', 'castor'),
            //         'center_right' => __('Center - Right Justified', 'castor'),
            //         'bottom_left' => __('Bottom Left', 'castor'),
            //         'bottom_right' => __('Bottom Right', 'castor'),
            //       )
            //     ),
            //   )
            // )
          )
        )
      ));
    }
  }
}
