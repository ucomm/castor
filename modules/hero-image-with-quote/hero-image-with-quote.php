<?php

class HeroImageWithQuote extends FLBuilderModule {
  public function __construct() {
    parent::__construct(array(
      'name' => __('Hero Image With Quote', 'castor'),
      'description' => __('Display a single large hero image with quotation.', 'castor'),
      'category' => __('Castor Modules', 'castor'),
      'dir' => CASTOR_DIR . 'modules/hero-image-with-quote/',
      'url' => CASTOR_URL . 'modules/hero-image-with-quote/',
      'editor_export'   => true, // Defaults to true and can be omitted.
      'enabled'         => true, // Defaults to true and can be omitted.
      'partial_refresh' => false, // Defaults to false and can be omitted.
    ));
  }
}
