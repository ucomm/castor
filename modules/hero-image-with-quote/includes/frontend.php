<?php
// The html for the hero image.
?>
<div class="hero-wrapper" id="hero_wrapper_<?php echo $id ?>">
  <div class="hero" id="hero_<?php echo $id ?>">
    <div class="fl-row-fixed-width hero-text-container" id="hero_text_container_<?php echo $id ?>">

        <blockquote class="uc-quote-text col-md-6" id="uc_quote_text_<?php echo $id ?>"><?php echo $settings->uconn_hwq_quote ?></blockquote>
        <div class="uc-attribution-wrapper col-md-6" id="uc_attribution_wrapper_<?php echo $id; ?>">
          <p class="uc-quote-attribution" id="uc_quote_attribution_<?php echo $id ?>">&mdash; <?php echo $settings->uconn_hwq_attribution ?></p>
        </div>
    </div>
  </div>
</div>
