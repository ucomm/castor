#hero_wrapper_<?php echo $id ?> {
  height: <?php echo $settings->uconn_hwq_image_height . $settings->uconn_hwq_image_height_unit ?>;
}

#hero_<?php echo $id ?> {
  display: flex;
  display: -webkit-flex;
  display: -ms-flexbox;
  background-image: url(<?php echo $settings->uconn_hwq_image_src ?>);
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  height: inherit
}

#hero_text_container_<?php echo $id ?> {
  margin: auto;
  padding: 0 32px;
  height: 100%;
  display: flex;
  display: -webkit-flex;
  display: -ms-flexbox;
  flex-direction: column;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  justify-content: center;
  -webkit-justify-content: center;
  -ms-justify-content: center;
}

#uc_quote_text_<?php echo $id ?> {
  margin: 100px 0 0 0;
  padding: 0;
  border: none;
  color: #<?php echo $settings->uconn_hwq_quote_color ?>;
  font-size: <?php echo $settings->uconn_hwq_quote_font_size; ?>px;
}

#uc_attribution_wrapper_<?php echo $id ?> {
  text-align: center;
}

#uc_quote_attribution_<?php echo $id ?> {
  color: #<?php echo $settings->uconn_hwq_attribution_color ?>;
  text-transform: uppercase;
}
