<?php
// four accross styles
?>

.fa-event-start {
  color: #<?php echo $settings->uconn_c_date_color; ?>;
}

.fa-event-link {
  color: #<?php echo $settings->uconn_c_link_color; ?>;
}

.fa-event-link:hover, 
.fa-event-link:focus {
  color: #<?php echo $settings->uconn_c_link_hover_color; ?>;
}

.fa-event-location {
  color: #<?php echo $settings->uconn_c_location_color; ?>;
}

<?php
// UITS classic styles
?>

.uc-event-date .uc-event-month-wrapper {
  background-color: #<?php echo $settings->uc_month_bg_color; ?>;
}

.uc-event-date .uc-event-month {
  color: #<?php echo $settings->uc_month_font_color; ?>;
}

.uc-event-date .uc-event-day {
  background-color: #<?php echo $settings->uc_day_bg_color ?>;
  color: #<?php echo $settings->uc_day_font_color; ?>;
}

.uc-event-subject {
  color: #<?php echo $settings->uc_subject_font_color; ?>;
}

.uc-event-time,
.event-is-public {
  color: #<?php echo $settings->uc_time_font_color; ?>;
}

<?php
// modal styles
?>

.event-info-modal {
  color: #<?php echo $settings->uconn_c_modal_description_color; ?>;
}

.modal-close {
  background-color: #<?php echo $settings->uconn_c_modal_close_bg_color; ?>;
  color: #<?php echo $settings->uconn_c_modal_close_color; ?>;
}

.btn.modal-close:hover, 
.btn.modal-close:focus {
  background-color: #<?php echo $settings->uconn_c_modal_close_bg_hover_color; ?>;
  color: #<?php echo $settings->uconn_c_modal_close_hover_color; ?>;
}