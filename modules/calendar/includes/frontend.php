<?php

/*
*	Date as APR 28, 2017 
* Start time as 12:00 AM (if all day) else start time
*	Subject - truncated as needed?
*	Specific Location (e.g. Gampel Pavilion)
*	Campus (e.g. storrs)
*/

$no_events_message = '';
$safe_fallback = filter_var($settings->uconn_c_fallback, FILTER_VALIDATE_BOOLEAN);

$start_time_stamp = date('Y-m-d H:i:s T');
$end_time_stamp = date('Y-m-d H:i:s T', strtotime('+1 year'));

$start_time_stamp_arr = explode(' ', $start_time_stamp);
$end_time_stamp_arr = explode(' ', $end_time_stamp);

$today = $start_time_stamp_arr[0];
$end_date = $end_time_stamp_arr[0];

$calendar = $settings->uconn_c_feed;

// the transient will be based on the particular calendar being requested.
$transient = "uc_calendar_" . md5($calendar);

// Based on the input, determine what type of RSS URL to create. 
// Numerical input = query by calendar id (legacy)
// string input = query by group name (LiveWhale (LW) approach ;  LW does not support calendar IDs)

if ($calendar == 'All Events') { 
	$calendar = '42'; 
	}

if (is_numeric($calendar)) {
	$queryBy = 'calendar%5B%5D'; 
} else { 
	$queryBy = 'group';
}
$rss_url = "https://events.uconn.edu/api/rss/?$queryBy=$calendar&start=$today&end=$end_date";
$master_rss_url = "https://events.uconn.edu/api/rss/?calendar%5B%5D=42&start=$today&end=$end_date";

$calendar_events = $module->get_rss_feed($rss_url, $transient);
$locations = $module->create_event_locations($calendar_events);

if (isset($_GET['cal-search'])) {
	$filtered = [];

	$searchKeyword = strtolower($_GET['cal-search']['cal-keyword']);
	$searchLocation = strtolower($_GET['cal-search']['cal-location']);
	$searchDate = $_GET['cal-search']['cal-date']; // 2021-08-31

	if ('' !== $searchKeyword) {
		$filtered = array_filter($calendar_events, function ($event) use ($searchKeyword) {
			$title = strtolower($event->title);
			$description = strtolower($event->description);
			if (false !== strpos($title, $searchKeyword) || false !== strpos($description, $searchKeyword)) {
				return $event;
			}
		});
	}

	if ('' !== $searchLocation && count($filtered) > 0) {
		$filtered = array_filter($filtered, function ($event) use ($searchLocation) {
			$location = strtolower($event->location);
			$room = strtolower($event->buildingroom);
			if (false !== strpos($location, $searchLocation) || false !== strpos($room, $searchLocation)) {
				return $event;
			}
		});
	} else if ('' !== $searchLocation) {
		$filtered = array_filter($calendar_events, function ($event) use ($searchLocation) {
			$location = strtolower($event->location);
			$room = strtolower($event->buildingroom);
			if (false !== strpos($location, $searchLocation) || false !== strpos($room, $searchLocation)) {
				return $event;
			}
		});
	} else if ('' === $searchLocation) {
		$filtered = $calendar_events;
	}

	if ('' !== $searchDate && count($filtered) > 0) {
		$filtered = array_filter($filtered, function ($event) use ($searchDate) {
			$eventDate = new DateTime($event->date);
			$formattedEventDate = $eventDate->format('Y-m-d');
			if ($searchDate === $formattedEventDate) {
				return $event;
			}
		});
	} else if ('' !== $searchDate) {
		$filtered = array_filter($calendar_events, function ($event) use ($searchDate) {
			$eventDate = new DateTime($event->date);
			$formattedEventDate = $eventDate->format('Y-m-d');
			if ($searchDate === $formattedEventDate) {
				return $event;
			}
		});
	}
	$calendar_events = $filtered;
}

if (!$safe_fallback && $calendar_events === 0) {
	$no_events_message = $settings->uconn_c_fallback_message;
	$events = [];
} else if ($safe_fallback && $calendar_events < 4) {
	$master_events = $module->get_rss_feed($master_rss_url);
	$combined_events = array_merge($calendar_events, $master_events);
	$events = array_slice($combined_events, 0, 4);
} else {
	$events = array_slice($calendar_events, 0, 4);
}

switch ($settings->uconn_c_layout) {
	case 'four-across':
		include(CASTOR_DIR . 'modules/calendar/partials/four-across.php');
		break;
	case 'uits-classic':
		include(CASTOR_DIR . 'modules/calendar/partials/uits-classic.php');
		break;
	default:
		include(CASTOR_DIR . 'modules/calendar/partials/four-across.php');
		break;
}
