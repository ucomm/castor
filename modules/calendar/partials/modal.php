<div class='cal-modal' id='event-<?php echo $index ?>' tabindex='-1' role='dialog' aria-labelledby='event-cal-modal-title-<?php echo $index ?>'>
  <div class='cal-modal-dialog event-cal-modal' role='document'>
    <div class='cal-modal-content'>
      <div class='cal-modal-header'>
        <h4 class='cal-modal-title' id='event-cal-modal-title-<?php echo $index; ?>'><?php echo $event->title; ?></h4>
      </div>
      <div class='cal-modal-body'>
        <p class='event-info-cal-modal event-time-cal-modal'>
          <?php echo $formatted_date; ?>
        </p>
        <p class='event-info-cal-modal event-location-cal-modal'><?php echo $event->location . " - " . $event->buildingroom; ?></p>
        <div class='event-info-cal-modal event-description'><?php echo $event->description; ?></div>
      </div>
      <div class='cal-modal-footer'>
        <button type='button' class='btn cal-modal-close' data-dismiss='cal-modal'>Close</button>
      </div>
    </div>
  </div>
  </div>