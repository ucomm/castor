<section id="event-calendar-<?php echo $module->node; ?>" aria-label="upcoming events at UConn">
	<?php
	// start the loop

	if (($no_events_message !== '' && (is_array($events) && count($events) === 0)) || is_null($events)) {
		include(CASTOR_DIR . 'modules/calendar/partials/fallback-message.php');
	} else if (is_array($events)) {
	?>
		<div class="fa-events-wrapper">
			<?php
				foreach ($events as $index => $event) {
					$formatted_date = $module->create_formatted_date($event->start_time, $event->date);
			?>
					<!-- create the event. -->
					<div class='fa-event-container'>
						<div>
							<p class='fa-event-start'><?php echo $formatted_date; ?></p>
							<p>
								<a href="#" data-target='#event-<?php echo $index ?>' class='fa-event-title fa-event-link castor-event-link'><?php echo $event->title; ?></a>
							</p>
							<p class='fa-event-location fa-event-location-specific'><?php echo $event->buildingroom; ?></p>
							<p class='fa-event-location fa-event-location-general'><?php echo $event->location; ?></p>
							<?php

							$module->is_open_to_the_public($event->description, $settings->uconn_c_open_to_public);

							?>
						</div>
					</div>

			<?php
				}
			?>
		</div>
		<div class="fa-events-modal-wrapper">
			<?php
			foreach ($events as $index => $event) {
				// create a modal for each event and end the loop
				include CASTOR_DIR . 'modules/calendar/partials/modal.php';
			}
			?>
		</div>
	<?php
	}
	?>
</section>