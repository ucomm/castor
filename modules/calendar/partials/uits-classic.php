<ul class="uc-events-list" aria-label="upcoming events at UConn">

	<?php

	if (($no_events_message !== '' && (is_array($events) && count($events) === 0))) {
		include(CASTOR_DIR . 'modules/calendar/partials/fallback-message.php');
	} else if (is_array($events)) {
		// start the loop
		foreach ($events as $index => $event) {

			$month = date('M', strtotime($event->date));
			$full_month = date('F', strtotime($event->date));
			$day = date('j', strtotime($event->date));
			$formatted_date = $module->create_formatted_date($event->start_time, $event->date);

	?>
			<!-- Create the event -->
			<li id="uc-event-item-<?php echo $index; ?>" class="uc-event-item" data-toggle='modal'>
				<div class="uc-event-wrapper">
					<div class="uc-event-date">
						<div class="uc-event-month-wrapper">
							<abbr title=<?php echo $full_month; ?> class="uc-event-month"><?php echo $month; ?></abbr>
						</div>
						<p class="uc-event-day"><?php echo $day; ?></p>
					</div>
					<div class="uc-event-detail">
						<a href="#" class="uc-event-subject castor-event-link" data-target='#event-<?php echo $index ?>'><?php echo $event->title; ?></a>
						<p class="uc-event-time">
							<?php echo $formatted_date; ?>
						</p>
						<?php
						$module->is_open_to_the_public($event->description, $settings->uconn_c_open_to_public);
						?>
					</div>
				</div>
			</li>
		<?php
			// create a modal for each event and end the loop
			include CASTOR_DIR . 'modules/calendar/partials/modal.php';
		}
		?>
		<a aria-label="All events - visit the main uconn calendar" href="https://events.uconn.edu/">All Events</a>
	<?php
	}
	?>
</ul>