document.addEventListener('DOMContentLoaded', () => {

  const eventLinks = document.querySelectorAll('.castor-event-link');
  const modals = document.querySelectorAll('.cal-modal')
  
  eventLinks.forEach(link => {
    link.onclick = function(evt) {
      evt.preventDefault();
      
      const modalID = evt.target.getAttribute('data-target')
      const modal = document.querySelector(modalID)

      modal.classList.add('modal-open')
    }
  })
  
  modals.forEach(modal => {
    modal.onclick = function(evt) {
      if (evt.target.localName !== 'button' && !evt.target.classList.contains('cal-modal-close')) {
        return
      }
      modal.classList.remove('modal-open')
    }
  })
})

document.addEventListener('keydown', (evt) => {
  const modals = document.querySelectorAll('.cal-modal')
  const hasOpenModals = document.querySelectorAll('.modal-open').length ? true : false

  if (hasOpenModals && evt.code === 'Escape') {
    modals.forEach(modal => {
      modal.classList.remove('modal-open')
    })
  }
})