<?php
/*
*
* A module to display a list of upcoming events from the UConn calendar. 
* Draws an ics file at events.uconn.edu. If > 4 events are available, this module will get them from the master calendar.
*
  */

require_once FIELDS_DIR . 'fields/number-input/number-input.php';

class UCCalendarModule extends Castor_Module {
  public function load_module() {
    if (class_exists('FLBuilder')) {
      require_once 'calendar.php';
      FLBuilder::register_module('UCCalendar', array(
        'tab_1' => array(
          'title' => __('Calendar Settings', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Settings', 'castor'),
              'fields' => array(
                'uconn_c_feed' => array(
                  'type' => 'text', 
                  'label' => __('Input a calendar name', 'castor'),
                  'description' => __("<br />The main calendar names are:
                    <ul>
                      <li>All Events</li>
                      <li>UConn Avery Point</li>
                      <li>UConn Hartford</li>
                      <li>UConn Stamford</li>
                      <li>UConn Waterbury</li>
                    </ul><br />
                  For additional calendar names, please visit <a href='http://events.uconn.edu' style='cursor: pointer; text-decoration: underline;'>events.uconn.edu</a> 
                  and reference the names in the Calendar dropdown. Please note that calendar names must be entered <strong>exactly</strong> as they appear in the dropdown, including all spaces and special symbols", 'castor'),
                  'default' => 'All Events'
                ),
                'uconn_c_layout' => array(
                  'type' => 'select',
                  'label' => __('Calendar Layout', 'castor'),
                  'default' => 'four-across',
                  'options' => array(
                    'four-across' => __('Four Across', 'castor'),
                    'uits-classic' => __('UITS Classic', 'castor')
                  ),
                  'toggle' => array(
                    'four-across' => array(
                      'sections' => array('fa_style') 
                    ),
                    'uits-classic' => array(
                      'sections' => array('uc_style')
                    )
                  )
                ),
                'uconn_c_fallback' => array(
                  'type' => 'select',
                  'label' => __('Fallback Policy', 'castor'),
                  'default' => 'true',
                  'options' => array(
                    'true' => __('Include outside events', 'castor'),
                    'false' => __('Exclude outside events', 'castor')
                  ),
                  'toggle' => array(
                    'false' => array(
                      'fields' => array('uconn_c_fallback_message', 'uconn_c_fallback_link')
                    )
                  ),
                  'description' => __('Set whether or not to include events from the main UConn calendar. If there are no events, a message will be displayed.', 'castor')
                ),
                'uconn_c_fallback_message' => array(
                  'type' => 'text',
                  'label' => __('Fallback Message', 'castor'),
                  'default' => __('No events are available at this time. Please see the UConn calendar for events in other departments.', 'castor'),
                  'description' => __('A message which will be linked to the <a href="https://events.uconn.edu">UConn Events page</a>.', 'castor')
                ),
                'uconn_c_fallback_link' => array(
                  'type' => 'text',
                  'label' => __('Fallback Link', 'castor'),
                  'default' => __('https://events.uconn.edu', 'castor'),
                  'description' => __('A link to a set of relevant events', 'castor')
                ),
                'uconn_c_open_to_public' => array(
                  'type' => 'text',
                  'label' => __('Search text', 'castor'),
                  'description' => __('Use this field to search the description for text that indicates the event is open to the public')
                )
              )
            )
          )
        ),
        'tab_2' => array(
          'title' => __('Style', 'castor'),
          'sections' => array(
            'fa_style' => array(
              'title' => __('Four Across Styles', 'castor'),
              'fields' => array(
                'uconn_c_date_color' => array(
                  'type' => 'color',
                  'label' => __('Date Color argle bargle', 'castor'),
                  'default' => '1540a2',
                  'show_reset' => true
                ),
                'uconn_c_link_color' => array(
                  'type' => 'color',
                  'label' => __('Link Color', 'castor'),
                  'default' => '000000',
                  'show_reset' => true
                ),
                'uconn_c_link_hover_color' => array(
                  'type' => 'color',
                  'label' => __('Link Hover Color', 'castor'),
                  'default' => '999999',
                  'show_reset' => true
                ),
                'uconn_c_location_color' => array(
                  'type' => 'color',
                  'label' => __('Location Color', 'castor'),
                  'default' => '4e4e4e',
                  'show_reset' => true
                ),
              )
            ),
            'uc_style' => array(
              'title' => __('UTIS Classic Styles', 'castor'),
              'fields' => array(
                'uc_month_bg_color' => array(
                  'type' => 'color',
                  'label' => __('Month Background Color', 'castor'),
                  'default' => 'ffffff',
                  'show_reset' => true
                ),
                'uc_month_font_color' => array(
                  'type' => 'color',
                  'label' => __('Month Font Color', 'castor'),
                  'default' => '000000',
                  'show_reset' => true
                ),
                'uc_day_bg_color' => array(
                  'type' => 'color',
                  'label' => __('Day Background Color', 'castor'),
                  'default' => 'ffffff',
                  'show_reset' => true
                ),
                'uc_day_font_color' => array(
                  'type' => 'color',
                  'label' => __('Day Font Color', 'castor'),
                  'default' => '000000',
                  'show_reset' => true
                ),
                'uc_subject_font_color' => array(
                  'type' => 'color',
                  'label' => __('Subject Font Color', 'castor'),
                  'default' => '000000',
                  'show_reset' => true
                ),
                'uc_time_font_color' => array(
                  'type' => 'color',
                  'label' => __('Time Font Color', 'castor'),
                  'default' => '000000',
                  'show_reset' => true
                ),
              )
            ),
            'section_2' => array(
              'title' => __('Modal styles', 'castor'),
              'fields' => array(
                'uconn_c_modal_description_color' => array(
                  'type' => 'color',
                  'label' => __('Modal Description Font Color', 'castor'),
                  'default' => '000e2f',
                  'show_reset' => true
                ),
                'uconn_c_modal_close_bg_color' => array(
                  'type' => 'color',
                  'label' => __('Modal Close Background Color', 'castor'),
                  'default' => '000000',
                  'show_reset' => true
                ),
                'uconn_c_modal_close_bg_hover_color' => array(
                  'type' => 'color',
                  'label' => __('Modal Close Background Hover Color', 'castor'),
                  'default' => '000000',
                  'show_reset' => true
                ),
                'uconn_c_modal_close_color' => array(
                  'type' => 'color',
                  'label' => __('Modal Close Font Color', 'castor'),
                  'default' => '999999',
                  'show_reset' => true
                ),
                'uconn_c_modal_close_hover_color' => array(
                  'type' => 'color',
                  'label' => __('Modal Close Font Hover Color', 'castor'),
                  'default' => '999999',
                  'show_reset' => true
                ),
              )
            )
          )
        )
      ));
    }
  }
}