<?php

class UCCalendar extends FLBuilderModule {
  public function __construct() {
    parent::__construct(array(
      'name' => __('UConn Calendar', 'castor'),
      'description' => __('Display a list of upcoming events', 'castor'),
      'category' => __('Castor Modules', 'castor'),
      'dir' => CASTOR_DIR . 'modules/calendar/',
      'url' => CASTOR_URL . 'modules/calendar/',
      'editor_export'   => true, // Defaults to true and can be omitted.
      'enabled'         => true, // Defaults to true and can be omitted.
      'partial_refresh' => false, // Defaults to false and can be omitted.
    ));
  }

  public function is_all_day_event(string $event_start): bool {
    return trim(strtoupper($event_start)) === 'ALL DAY' ? true : false; 
  }

  public function create_formatted_date($event_start, $event_date): string {
    $is_all_day = $this->is_all_day_event($event_start);

    $formatted_date = date('M j, Y', strtotime($event_date));
    $formatted_start = !$is_all_day ?
      date('g:i A', strtotime($event_start)) :
      'ALL DAY';

    return $formatted_date . ' | ' . $formatted_start;
  }

  public function truncate_event_description($event_description, $event_url) {
    if (strlen($event_description) > 140) {
      $event_description = substr($event_description, 0, 140);
      $event_description .= "... ";
    } 
    return "$event_description <a class='event-link' href=$event_url>Continue on the event page</a>";
  }
  public function is_open_to_the_public($description_string, $search_string) {
    $search_position = stripos($description_string, $search_string);
    // only display the notice if the text is present and is the first thing in the description.
    if ($search_position !== false && $search_position === 0) {
      echo "<p class='event-location event-is-public'>Open to the public</p>";
    }
  }
  public function cache_response($transient, $response) {
    $response_body = $response['body'];
    if (!get_transient($transient)) {
      // set a fairly short transient so that not to many events get missed.
      set_transient($transient, $response_body, HOUR_IN_SECONDS);
    } else {
      $response_body = get_transient($transient);
    }
    return $response_body;
  }
  public function get_rss_feed(string $url, $transient) {
    $result = [];
    $response = wp_remote_get($url, array( 'timeout' => 10));

    if( is_wp_error( $response ) ) {
      return [
        'error' => $response->get_error_message()
      ];
    } else { 
      $data = $this->cache_response($transient, $response);
    }
    
    //$data     = wp_remote_retrieve_body($response);

    $xml = simplexml_load_string($data, null, LIBXML_NOCDATA);

    if (!$xml) {
      return [ 'error' => true ];
    }

    foreach ($xml->channel->item as $event) {
      array_push($result, $event);
    }

    return $result;
  }

  public function create_event_locations(array $events): array {
    $locations = array_reduce($events, function ($acc, $curr) {
            
      if (false === array_search((string) $curr->location, $acc)) {
        array_push($acc, (string) $curr->location);
      }
  
      return $acc;
    }, []);


    sort($locations);

    return $locations;
  }
}