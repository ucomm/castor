<?php

class SidebarNavigation extends FLBuilderModule {
  public function __construct() {
    parent::__construct(array(
      'name' => __('Sidebar Navigation', 'castor'),
      'description' => __('Adds a navigation menu to use in sidebars. Only the current page and it\'s children will be visible', 'castor'),
      'category' => __('Castor Modules', 'castor'),
      'dir' => CASTOR_DIR . 'modules/sidebar-navigation/',
      'url' => CASTOR_URL . 'modules/sidebar-navigation/',
      'editor_export'   => true, // Defaults to true and can be omitted.
      'enabled'         => true, // Defaults to true and can be omitted.
      'partial_refresh' => false, // Defaults to false and can be omitted.
    ));
  }
  public function get_opacity(string $opacity) {
    return intval($opacity) / 100.0;
  }
}