<?php

class SidebarNavigationModule extends Castor_Module {

  /**
   * Populate the choices for the module
   *
   * @return array
   */
  private function get_menu_options(): array {
    $options = array();
    $menus = wp_get_nav_menus();

    foreach ($menus as $menu) {
      $options[$menu->slug] = $menu->name;
    }

    return $options;
  }

  /**
   * Only allow admins to adjust the menu chosen for the sidebar
   *
   * @return array
   */
  private function manage_menu_selector(): array {
    $tab_1 = array(
      'title' => 'Admins Only',
    );
    if (current_user_can('edit_users')) {
      $description = __('Select a menu to display', 'castor');
      $tab_1 = array_merge($tab_1, array(
        'sections' => array(
          'section_1' => array(
            'fields' => array(
              'uconn_sn_menus' => array(
                'title' => __('Menu', 'castor'),
                'description' => __('Choose a menu to display in the sidebar'),
                'type' => 'select',
                'options' => $this->get_menu_options()
              ),
              'uconn_sn_behavior' => array(
                'title' => __('Menu Behavior', 'castor'),
                'description' => __('Choose if the sidebar should always display deeply nested items'),
                'type' => 'select',
                'default' => 'classic',
                'options' => array(
                  'classic' => __('Classic Menu/Regional Style', 'castor'),
                  'deep' => __('Show Deeply Nested Items', 'castor'),
                  'compact' => __('Compact Menu', 'castor')
                )
              )
            )
          ) 
        )
      ));
    }
    return $tab_1;
  }

  public function load_module() {
    if (class_exists('FLBuilder')) {
      require_once 'sidebar-navigation.php';
      FLBuilder::register_module('SidebarNavigation', array(
        'tab_1' => $this->manage_menu_selector(),
        'tab_2' => array(
          'title' => __('Style', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Style', 'castor'),
              'fields' => array(
                'uconn_sn_font_color' => array(
                  'type' => 'color',
                  'label' => __('Font color', 'castor'),
                  'default' => '000000',
                  'show_reset' => true
                ),
                'uconn_sn_font_hover_color' => array(
                  'type' => 'color',
                  'label' => __('Font hover color', 'castor'),
                  'default' => 'ffffff',
                  'show_reset' => true
                ),
                'uconn_sn_background' => array(
                  'type' => 'color',
                  'label' => __('Background color', 'castor'),
                  'default' => 'ffffff',
                  'show_reset' => true
                ),
                'uconn_sn_background_opacity' => array(
                  'type' => 'unit',
                  'label' => __('Background opacity', 'castor'),
                  'description' => __('%', 'castor'),
                  'default' => '0',
                  'slider' => array(
                    'min' => 0,
                    'max' => 100,
                    'step' => 1
                  )
                ),
                'uconn_sn_background_hover' => array(
                  'type' => 'color',
                  'label' => __('Background hover color', 'castor'),
                  'show_reset' => true
                ),
                'uconn_sn_background_hover_opacity' => array(
                  'type' => 'unit',
                  'label' => __('Background hover opacity', 'castor'),
                  'description' => __('%', 'castor'),
                  'default' => '0',
                  'slider' => array(
                    'min' => 0,
                    'max' => 100,
                    'step' => 1
                  )
                ),
                'uconn_sn_current_page_background' => array(
                  'type' => 'color',
                  'label' => __('Current page background', 'castor'),
                  'default' => 'ffffff',
                  'show_reset' => true
                ),
                'uconn_sn_current_page_background_opacity' => array(
                  'type' => 'unit',
                  'label' => __('Current page background opacity', 'castor'),
                  'description' => __('%', 'castor'),
                  'default' => '0',
                  'slider' => array(
                    'min' => 0,
                    'max' => 100,
                    'step' => 1
                  )
                ),
                'uconn_sn_border' => array(
                  'type' => 'color',
                  'label' => __('Border color', 'castor'),
                  'show_reset' => true
                )
              )
            )
          )
        )
      ));
    }
  }
}