<?php 

$background_opacity = $module->get_opacity($settings->uconn_sn_background_opacity);
$background_hover_opacity = $module->get_opacity($settings->uconn_sn_background_hover_opacity);
$current_page_opacity = $module->get_opacity($settings->uconn_sn_current_page_background_opacity);

?>

#sidebar_navigation_list_<?php echo $id; ?>:before {
  background-color: #<?php echo $settings->uconn_sn_background; ?>;
  opacity: <?php echo $background_opacity; ?>;
}

#sidebar_navigation_list_<?php echo $id; ?> li {
  border-top: 1px solid #<?php echo $settings->uconn_sn_border; ?>;
}

#sidebar_navigation_list_<?php echo $id; ?> a {
  color: #<?php echo $settings->uconn_sn_font_color; ?>;
}

#sidebar_navigation_list_<?php echo $id; ?> a:before {
  background-color: #<?php echo $settings->uconn_sn_background; ?>;
}

#sidebar_navigation_list_<?php echo $id; ?> a:hover,
#sidebar_navigation_list_<?php echo $id; ?> a:focus {
  color: #<?php echo $settings->uconn_sn_font_hover_color; ?>;
}

#sidebar_navigation_list_<?php echo $id; ?> a:hover:before,
#sidebar_navigation_list_<?php echo $id; ?> a:focus:before {
  background-color: #<?php echo $settings->uconn_sn_background_hover; ?>;
  opacity: <?php echo $background_hover_opacity; ?>;
}

#sidebar_navigation_list_<?php echo $id; ?> <?php echo Castor_Helpers::set_current_page_class(); ?> > a {
  color: #<?php echo $settings->uconn_sn_font_hover_color; ?>;
}

#sidebar_navigation_list_<?php echo $id; ?> <?php echo Castor_Helpers::set_current_page_class(); ?> > a:hover,
#sidebar_navigation_list_<?php echo $id; ?> <?php echo Castor_Helpers::set_current_page_class(); ?> > a:focus {
  color: #<?php echo $settings->uconn_sn_font_hover_color; ?>;
}

#sidebar_navigation_list_<?php echo $id; ?> <?php echo Castor_Helpers::set_current_page_class(); ?> > a:before {
  background-color: #<?php echo $settings->uconn_sn_current_page_background; ?>;
  opacity: <?php echo $current_page_opacity; ?>;
}

<?php 
if ($settings->uconn_sn_behavior === 'classic') {
?>  
  
.sidebar-navigation-list .current-menu-item > .sub-menu .menu-item .sub-menu {
  display: none;
}

<?php
} elseif ($settings->uconn_sn_behavior === 'compact') {
?>

.sidebar-navigation-list > li li:not(.current-menu-item):not(.current-page-ancestor) > .sub-menu {
  display: none;
}

<?php
}
?>