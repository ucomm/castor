<?php

$args = array(
  'menu' => $settings->uconn_sn_menus,
  'menu_class' => 'sidebar-navigation-list',
  'menu_id' => 'sidebar_navigation_list_' . $id,
  'container' => 'nav',
  'container_class' => 'sidebar-navigation-container',
  'container_id' => 'sidebar_navigation_container_' . $id
);

wp_nav_menu($args);

?>