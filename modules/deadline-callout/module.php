<?php
/*
*
* A module that displays a callout with title info and date.
*
*/

class DeadlineCalloutModule extends Castor_Module {
  public function load_module() {
    if (class_exists('FLBuilder')) {
      require_once 'deadline-callout.php';
      FLBuilder::register_module('DeadlineCallout', array(
        'tab_1' => array(
          'title' => __('Content', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Content', 'castor'),
              'fields' => array(
                'uconn_dc_deadlines' => array(
                  'type' => 'form',
                  'label' => __('Deadline', 'castor'),
                  'form' => 'uconn_dc_form',
                  'preview_text' => 'uconn_dc_title',
                  'multiple' => true
                ),
              )
            )
          )
        ),
        'tab_2' => array(
          'title' => __('Style', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Style', 'castor'),
              'fields' => array(
                'uconn_dc_background_color' => array(
                  'type' => 'color',
                  'label' => __('Background Color', 'castor'),
                  'default' => '#ffffff',
                  'show_reset' => true
                ),
                'uconn_dc_background_opacity' => array(
                  'type' => 'text',
                  'label' => __('Background Opacity', 'castor'),
                  'default' => '0.5',
                  'maxlength' => '4'
                ),
                'uconn_dc_border_color' => array(
                  'type' => 'color',
                  'label' => __('Border Color', 'castor'),
                  'default' => '#ffffff',
                  'show_reset' => true
                ),
                'uconn_dc_font_color' => array(
                  'type' => 'color',
                  'label' => __('Font Color', 'castor'),
                  'default' => '#ffffff',
                  'show_reset' => true
                ),
                'uconn_dc_title_typography' => array(
                  'type' => 'typography',
                  'label' => __('Title Typography', 'castor'),
                  'responsive' => true,
                  'preview' => array(
                    'type' => 'css',
                    'selector' => '.deadline-title'
                  )
                ),
                'uconn_dc_info_typography' => array(
                  'type' => 'typography',
                  'label' => __('Info Typography', 'castor'),
                  'responsive' => true,
                  'preview' => array(
                    'type' => 'css',
                    'selector' => '.deadline-info *'
                  )
                ),
                'uconn_dc_date_typography' => array(
                  'type' => 'typography',
                  'label' => __('Date Typography', 'castor'),
                  'responsive' => true,
                  'preview' => array(
                    'type' => 'css',
                    'selector' => '.deadline-date-container *'
                  )
                ),
              )
            )
          )
        )
      ));

      FLBuilder::register_settings_form('uconn_dc_form', array(
        'title' => __('Edit Deadlines', 'castor'),
        'tabs' => array(
          'general' => array(
            'title' => __('General', 'castor'),
            'sections' => array(
              'info' => array(
                'title' => __('Deadline', 'castor'),
                'fields' => array(
                  'date' => array(
                    'type' => 'date',
                    'label' => __('Deadline Date', 'castor'),
                    'min' => '2010-01-01'
                  ),
                  'title' => array(
                    'type' => 'text',
                    'label' => __('Title', 'castor'),
                    'maxlength' => '140',
                    'default' => 'Important Deadline'
                  ),
                  'info' => array(
                    'type' => 'editor',
                    'label' => __('Deadline Item', 'castor')
                  ),
                )
              )
            )
          )
        )
      ));
    }
  }
}