<?php

FLBuilderCSS::typography_field_rule(array(
  'settings' => $settings,
  'setting_name' => 'uconn_dc_title_typography',
  'selector' => '.fl-node-' . $id . ' .deadline-title'
));

FLBuilderCSS::typography_field_rule(array(
  'settings' => $settings,
  'setting_name' => 'uconn_dc_info_typography',
  'selector' => '.fl-node-' . $id . ' .deadline-info *'
));

FLBuilderCSS::typography_field_rule(array(
  'settings' => $settings,
  'setting_name' => 'uconn_dc_date_typography',
  'selector' => '.fl-node-' . $id . ' .deadline-date-container *'
));


foreach ($settings->uconn_dc_deadlines as $index => $style) {
  $instance = "#deadline_callout_" . $id . "_" . $index;
?>
  <?php echo $instance; ?> {
    border: 1px solid #<?php echo $settings->uconn_dc_border_color; ?>;
  }

  <?php echo $instance; ?> {
    border: 1px solid #<?php echo $settings->uconn_dc_border_color; ?>;
  }

  <?php echo $instance; ?>:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background-color: #<?php echo $settings->uconn_dc_background_color; ?>;
    opacity: <?php echo $settings->uconn_dc_background_opacity; ?>;
  }
  #deadline_title_<?php echo $id . "_" . $index; ?>,
  #deadline_month_<?php echo $id . "_" . $index; ?>, 
  #deadline_date_<?php echo $id . "_" . $index; ?>,
  #deadline_date_container_<?php echo $id . "_" . $index; ?>,
  #deadline_info_container_<?php echo $id . "_" . $index; ?> {
    color: #<?php echo $settings->uconn_dc_font_color; ?>;
  }
<?php
}
?>