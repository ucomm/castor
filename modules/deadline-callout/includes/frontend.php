<ol class="uc-deadline-list">

<?php

$deadlines = $settings->uconn_dc_deadlines;

foreach ($deadlines as $index => $deadline) {
  $instance = $id . "_" . $index;

  if ($deadline->date === '') {
    echo "<p><strong>Please pick a deadline date</strong></p>";
  } else {

    $date = DateTime::createFromFormat('Y-m-d', $deadline->date);
    $day = $date->format('d');
    $full_month = $date->format('F');
    $month = $date->format('M');

  ?>
    <li id="deadline_callout_<?php echo $instance; ?>" class="deadline-callout">
      <div id="deadline_info_container_<?php echo $instance; ?>" class="deadline-info-container">
        <p id="deadline_title_<?php echo $instance; ?>" class="deadline-title">
          <?php echo $deadline->title; ?>
        </p>
        <div class="deadline-info">
          <?php echo $deadline->info; ?>
        </div>
      </div>
      <div id="deadline_date_container_<?php echo $instance; ?>" class="deadline-date-container">
        <abbr title="<?php echo $full_month; ?>" id="deadline_month_<?php echo $instance; ?>" class="deadline-month">
          <?php echo $month; ?>
        </abbr>
        <p id="deadline_date_<?php echo $instance; ?>" class="deadline-date">
          <?php echo $day; ?>
        </p>
      </div>
    </li>
  <?php
  }
}
?>

</ol>