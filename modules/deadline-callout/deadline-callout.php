<?php

class DeadlineCallout extends FLBuilderModule {
  public function __construct() {
    parent::__construct(array(
      'name' => __('Deadline Callout', 'castor'),
      'description' => __('Display a callout that shows a date', 'castor'),
      'category' => __('Castor Modules', 'castor'),
      'dir' => CASTOR_DIR . 'modules/deadline-callout/',
      'url' => CASTOR_URL . 'modules/deadline-callout',
      'editor_export'   => true, // Defaults to true and can be omitted.
      'enabled'         => true, // Defaults to true and can be omitted.
      'partial_refresh' => false, // Defaults to false and can be omitted.  
    ));
  }
}