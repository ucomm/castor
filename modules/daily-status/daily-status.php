<?php

class DailyStatus extends FLBuilderModule {
  public function __construct() {
    parent::__construct(array(
      'name' => __('Daily Status', 'castor'),
      'description' => __('A module to show information related to the daily alert status', 'castor'),
      'category' => __('Castor Modules', 'castor'),
      'dir' => CASTOR_DIR . 'modules/daily-status/',
      'url' => CASTOR_URL . 'modules/daily-status/',
      'editor_export' => true,
      'enabled' => true,
      'partial_refresh' => true
    ));
  }
}