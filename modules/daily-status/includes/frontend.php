<<?php echo $settings->status_title_tag; ?> 
  id="daily-status-title-<?php echo $id; ?>" 
  class="daily-status-title">
  <?php 
    $title = $settings->status_title . " ";
    $title .= date("F j, Y");
    echo $title;
  ?>
</<?php echo $settings->status_title_tag; ?>>
<p id="daily-status-text-<?php echo $id; ?>" class="daily-status-text">
  <?php echo $settings->status_text; ?>
</p>