<?php

class DailyStatusModule extends Castor_Module {
  public function load_module() {
    if (class_exists('FLBuilder')) {
      require_once 'daily-status.php';
      FLBuilder::register_module('DailyStatus', array(
        'tab_1' => array(
          'title' => __('Settings', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Status', 'castor'),
              'fields' => array(
                'status_title' => array(
                  'type' => 'text',
                  'label' => __('Text to go before the current date', 'castor'),
                  'default' => __("Status For", 'castor'),
                  'maxlength' => '50'
                ),
                'status_title_tag' => array(
                  'type' => 'select',
                  'label' => __('Title Tag', 'castor'),
                  'description' => __('The HTML tag for the title', 'castor'),
                  'default' => 'h3',
                  'options' => array(
                    'h1' => __('h1', 'castor'),
                    'h2' => __('h2', 'castor'),
                    'h3' => __('h3', 'castor'),
                    'h4' => __('h4', 'castor'),
                    'h5' => __('h5', 'castor'),
                    'h6' => __('h6', 'castor'),
                    'p' => __('p', 'castor')
                  )
                ),
                'status_text' => array(
                  'type' => 'textarea',
                  'label' => __('The current status', 'castor')
                )
              )
            )
          )
        )
      ));
    }
  }
}