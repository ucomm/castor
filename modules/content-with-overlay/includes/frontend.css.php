<?php
/*
*
*  css to be applied at the per module level (except for height).
*
 */
?>

#cwo_item_container_<?php echo $id ?> {
<?php
if ($settings->background_toggle === 'uconn_cwo_image') {
?>
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
  background-image: url(<?php echo $settings->uconn_cwo_image_src ?>);
<?php
} else {
?>
  background-color: #<?php echo $settings->uconn_cwo_bg_color; ?>;
<?php
}
?>

color: #<?php echo $settings->uconn_cwo_text_color ?>;
height: <?php echo $settings->uconn_cwo_height . $settings->uconn_cwo_height_unit ?>;
justify-content: <?php echo $settings->uconn_cwo_text_horiz_pos; ?>;
align-items: <?php echo $settings->uconn_cwo_text_vert_pos; ?>;
}

#cwo_item_container_<?php echo $id ?> .cwo-item-text {
  color: #<?php echo $settings->uconn_cwo_text_color ?>;
  flex-grow: 1; 
}

#cwo_item_container_<?php echo $id ?>:before {
background-color: #<?php echo $settings->uconn_cwo_hover_color; ?>;
opacity: <?php echo $settings->uconn_cwo_hover_color_opacity_start; ?>;
transition-duration: <?php echo $settings->border_trans_dur; ?>s;
transition-timing-function: <?php echo $settings->border_trans_func; ?>;
}

.cwo-item-link:focus > #cwo_item_container_<?php echo $id ?>:before,
#cwo_item_container_<?php echo $id ?>:hover:before {
opacity: <?php echo $settings->uconn_cwo_hover_color_opacity_end; ?>;
}

#cwo_item_text_<?php echo $id ?> {
font-family: <?php echo $settings->uconn_cwo_font['family']; ?>;
font-size: <?php echo $settings->uconn_cwo_font_size; ?>px;
font-weight: <?php echo $settings->uconn_cwo_font['weight']; ?>;
line-height: <?php echo $settings->uconn_cwo_line_height; ?>;
margin-top: <?php echo $settings->uconn_cwo_text_margin_top; ?>px;
margin-right: <?php echo $settings->uconn_cwo_text_margin_right; ?>px;
margin-bottom: <?php echo $settings->uconn_cwo_text_margin_bottom; ?>px;
margin-left: <?php echo $settings->uconn_cwo_text_margin_left; ?>px;
}

<?php
if ($settings->uconn_cwo_hover_text && $settings->uconn_cwo_text_visibility === "unique-hover") {
?>
  #cwo_item_container_<?php echo $id ?>:hover .cwo-item-hover-text, #cwo_item_container_<?php echo $id ?>:focus .cwo-item-hover-text {
  display: block; 
  font-family: <?php echo $settings->uconn_cwo_hover_font['family']; ?>;
  font-size: <?php echo $settings->uconn_cwo_hover_font_size; ?>px;
  font-weight: <?php echo $settings->uconn_cwo_hover_font['weight']; ?>;
  line-height: <?php echo $settings->uconn_cwo_hover_line_height; ?>;
  color: #<?php echo $settings->uconn_cwo_hover_text_color; ?>;
  margin-top: <?php echo $settings->uconn_cwo_hover_text_margin_top; ?>px;
  margin-right: <?php echo $settings->uconn_cwo_hover_text_margin_right; ?>px;
  margin-bottom: <?php echo $settings->uconn_cwo_hover_text_margin_bottom; ?>px;
  margin-left: <?php echo $settings->uconn_cwo_hover_text_margin_left; ?>px;
  text-align: <?php echo $settings->uconn_cwo_hover_text_alignment; ?>;
  align-self: <?php echo $settings->uconn_cwo_hover_text_vert_pos; ?>;
  z-index: 20; 
  }
  
  #cwo_item_container_<?php echo $id ?>:hover .cwo-item-text, #cwo_item_container_<?php echo $id ?>:focus .cwo-item-text {
  display: none; 
  }
<?php
}
?>


<?php
if ($settings->uconn_cwo_text_visibility === 'hover') {
?>
  #cwo_item_text_<?php echo $id ?> {
  opacity: 0;
  transition: opacity;
  transition-duration: <?php echo $settings->border_trans_dur; ?>s;
  transition-timing-function: <?php echo $settings->border_trans_func; ?>;
  }

  #cwo_item_container_<?php echo $id ?>:hover #cwo_item_text_<?php echo $id ?>,
  #cwo_item_link_<?php echo $id ?>:hover #cwo_item_text_<?php echo $id ?>,
  #cwo_item_link_<?php echo $id ?>:focus #cwo_item_text_<?php echo $id ?> {
  opacity: 1;
  }
<?php
}
?>


<?php

/**
 * 
 * 
 * properties needed
 * - top vs bottom
 * - height in px
 * - top/bottom offset in px
 * - transition duration
 * - transition timing function
 * 
 */

// only apply the following settings if needed.
if ($settings->border_toggle === 'true') {
?>


  #cwo_item_link_<?php echo $id; ?>:after {
  background-color: #<?php echo $settings->border_color; ?>;
  <?php echo $settings->border_position; ?>: <?php echo $settings->border_offset; ?>px;
  height: <?php echo $settings->border_height; ?>px;
  }



  #cwo_item_link_<?php echo $id; ?>:after {
  transform: scaleX(<?php echo $settings->border_starting_scale / 100.0; ?>);
  transition-duration: <?php echo $settings->border_trans_dur; ?>s;
  transition-timing-function: <?php echo $settings->border_trans_func; ?>;
  }

  #cwo_item_link_<?php echo $id; ?>:hover:after,
  #cwo_item_link_<?php echo $id; ?>:focus:after {
  transform: scaleX(<?php echo $settings->border_final_scale / 100.0; ?>);
  }

<?php
}
?>
#cwo_icon-container-<?php echo $id; ?> {
padding-top: <?php echo $settings->icon_background_size_top; ?>px;
padding-right: <?php echo $settings->icon_background_size_right; ?>px;
padding-bottom: <?php echo $settings->icon_background_size_bottom; ?>px;
padding-left: <?php echo $settings->icon_background_size_left; ?>px;
<?php
// put the icon in a corner and give it a potential offset.
?>
<?php echo $settings->icon_position_horiz; ?>: <?php echo $settings->icon_offset_horiz; ?>px;
<?php echo $settings->icon_position_vert; ?>: <?php echo $settings->icon_offset_vert; ?>px;
}

#cwo_icon-container-<?php echo $id; ?>:before {
background-color: #<?php echo $settings->icon_background; ?>;
opacity: <?php echo $settings->icon_background_opacity; ?>;
}

#cwo_icon-container-<?php echo $id; ?> i {
font-size: <?php echo $settings->icon_size; ?>px;
}

<?php

if ($settings->uconn_cwo_text_shadow === 'true') {
?>
  #cwo_item_text_<?php echo $id; ?> {
  text-shadow: 2px 2px 2px #000;
  }
<?php
}

if ($settings->uconn_cwo_hover_text_shadow === 'true') {
?>
  #cwo_item_container_<?php echo $id ?> .cwo-item-hover-text {
  text-shadow: 2px 2px 2px #000;
  }
<?php
}
?>



@media screen and (max-width: 767px) {
  <?php //Hide the overlay (on page load) if we're on a small screen device && the user has opted to not show the overlay on mobile
    if ($settings->uconn_cwo_overlay_on_mobile === 'false') {
  ?>
    #cwo_item_container_<?php echo $id ?>:before {
      opacity: 0;
    }
  <?php
    }
  //Hide the border (on page load) if we're on a small screen device && the user has opted to show the border
    if ($settings->border_on_mobile === 'true' && $settings->border_toggle === 'true') {
  ?>
    #cwo_item_link_<?php echo $id; ?>:after {
    transform: scaleX(<?php echo $settings->border_final_scale / 100.0; ?>);
    }
  <?php
    }
  ?>
}

.fl-module-content-with-overlay.navy-blue-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> { 
  background-color: #000E2F;
}

.fl-module-content-with-overlay.red-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> { 
  background-color: #E4002B;
}

.fl-module-content-with-overlay.grey-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> { 
  background-color: #7C878E;
}

.fl-module-content-with-overlay.white-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> { 
  background-color: #FFF;
}

.fl-module-content-with-overlay.black-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> { 
  background-color: #000;
}

.fl-module-content-with-overlay.navy-blue-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> p, 
.fl-module-content-with-overlay.red-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> p, 
.fl-module-content-with-overlay.black-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> p { 
  color: #FFF; 
}

.fl-module-content-with-overlay.grey-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> p, 
.fl-module-content-with-overlay.white-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> p { 
  color: #000E2F;
}

.fl-module-content-with-overlay.navy-blue-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> > p:first-child, 
.fl-module-content-with-overlay.red-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> > p:first-child, 
.fl-module-content-with-overlay.grey-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> > p:first-child, 
.fl-module-content-with-overlay.white-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> > p:first-child, 
.fl-module-content-with-overlay.black-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> > p:first-child { 
  margin: 10px 0px 5px 16px;
  font-family: <?php echo $settings->uconn_cwo_font['family']; ?>;
  font-size: <?php echo $settings->uconn_cwo_font_size; ?>px;
  font-weight: <?php echo $settings->uconn_cwo_font['weight']; ?>;
  line-height: <?php echo $settings->uconn_cwo_line_height; ?>;
}

.fl-module-content-with-overlay.navy-blue-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> > p:last-child, 
.fl-module-content-with-overlay.red-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> > p:last-child, 
.fl-module-content-with-overlay.grey-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> > p:last-child, 
.fl-module-content-with-overlay.white-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> > p:last-child, 
.fl-module-content-with-overlay.black-bg > .fl-module-content > a > .cwo-item-container > #cwo_item_text_<?php echo $id ?> > p:last-child { 
  margin: 0px 0px 10px 16px;
  font-family: <?php echo $settings->uconn_cwo_font['family']; ?>;
  font-size: smaller;
  font-weight: <?php echo $settings->uconn_cwo_font['weight']; ?>;
  line-height: <?php echo $settings->uconn_cwo_line_height; ?>;
}




