<?php
/*
*
*  Frontend layout for box with overlay effect.
*
 */

$text_tag = 'p';
$text = $settings->uconn_cwo_text;

if ($settings->uconn_cwo_hover_text) { 
  $has_hover_text = true;
  $hover_text = $settings->uconn_cwo_hover_text; 
}

if ($settings->uconn_cwo_text_type === 'advanced') {
  $text_tag = 'div';
  $text = $settings->uconn_cwo_text_advanced;
}

$has_link = $settings->link_toggle !== 'uconn_cwo_no_link' ? true : false;
$link = $settings->uconn_cwo_link;

if ($settings->link_toggle === 'uconn_cwo_custom_link') {
  $link = $settings->uconn_cwo_custom_link;
}

if ($has_link) {
?>
  <a href="<?php echo $link; ?>" class="cwo-item-link" id="cwo_item_link_<?php echo $id ?>">
<?php
}
?>
  <div class="cwo-item-container" id="cwo_item_container_<?php echo $id ?>">
    <<?php echo $text_tag; ?> class="cwo-item-text" id="cwo_item_text_<?php echo $id ?>">
      <?php echo $text; ?>
    </<?php echo $text_tag; ?>>
    <div id="cwo_icon-container-<?php echo $id; ?>" class="cwo-icon-container">
      <i class="<?php echo $settings->icon; ?>"></i>
    </div>

    <?php if ($has_hover_text && $settings->uconn_cwo_text_visibility === "unique-hover") { ?>
      <div class="cwo-item-hover-text">
        <?php echo $hover_text; ?>
    </div>
    <?php } ?>
    
  </div>
<?php
if ($has_link) {
?>
  </a>
<?php
}
?>