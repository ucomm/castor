<?php

/*
*
*  A module to display a box with a photo or background color, text,
*  and hover effect.
*
 */

class ContentWithOverlayModule extends Castor_Module {
  public function load_module() {
    if (class_exists('FLBuilder')) {
      require_once 'content-with-overlay.php';
      FLBuilder::register_module('ContentWithOverlay', array(
        'tab_1' => array(
          'title' => __('Content', 'castor'),
          'sections' => array(
            'section_1' => array(
              'title' => __('Content', 'castor'),
              'fields' => array(
                'background_toggle' => array(
                  'type' => 'select',
                  'label' => __('Background Type', 'castor'),
                  'default' => 'uconn_cwo_image',
                  'options' => array(
                    'uconn_cwo_image' => __('Image', 'castor'),
                    'uconn_cwo_bg_color' => __('Color', 'castor')
                  ),
                  'toggle' => array(
                    'uconn_cwo_image' => array(
                      'fields' => array('uconn_cwo_image')
                    ),
                    'uconn_cwo_bg_color' => array(
                      'fields' => array('uconn_cwo_bg_color')
                    )
                  )
                ),
                'uconn_cwo_image' => array(
                  'type' => 'photo',
                  'label' => __('Background Image', 'castor'),
                  'show_remove' => true
                ),
                'uconn_cwo_bg_color' => array(
                  'type' => 'color',
                  'label' => __('Background Color', 'castor'),
                  'show_remove' => true
                ),
                'link_toggle' => array(
                  'type' => 'select',
                  'label' => __('Link Options', 'castor'),
                  'default' => 'uconn_cwo_link',
                  'options' => array(
                    'uconn_cwo_link' => __('Preset Links', 'castor'),
                    'uconn_cwo_custom_link' => __('Custom Link', 'castor'),
                    'uconn_cwo_no_link' => __('No Link', 'castor')
                  ),
                  'toggle' => array(
                    'uconn_cwo_link' => array(
                      'fields' => array('uconn_cwo_link')
                    ),
                    'uconn_cwo_custom_link' => array(
                      'fields' => array('uconn_cwo_custom_link')
                    )
                  )
                ),
                'uconn_cwo_link' => array(
                  'type' => 'select',
                  'label' => __('Link', 'castor'),
                  'default' => 'http://averypoint.uconn.edu/',
                  'options' => array(
                    'http://averypoint.uconn.edu/' => __('Avery Point', 'castor'),
                    'http://hartford.uconn.edu/' => __('Hartford', 'castor'),
                    'http://stamford.uconn.edu/' => __('Stamford', 'castor'),
                    'http://waterbury.uconn.edu/' => __('Waterbury', 'castor'),
                    'http://health.uconn.edu/' => __('UConn Health', 'castor'),
                    'http://admissions.uconn.edu/' => __('Admissions', 'castor'),
                    'http://uconn.edu/about-us/' => __('About Us', 'castor'),
                    'http://uconn.edu/academics/' => __('Academics', 'castor'),
                    'http://uconn.edu/campus-life/' => __('Campus Life', 'castor'),
                    'http://uconn.edu/research/' => __('Research', 'castor'),
                    'http://uconn.edu/about-us/' => __('About Us', 'castor'),
                    'http://uconn.edu/athletics/' => __('Athletics', 'castor')
                  )
                ),
                'uconn_cwo_custom_link' => array(
                  'type' => 'text',
                  'label' => __('Custom Link', 'castor')
                ),
                'uconn_cwo_text_type' => array(
                  'type' => 'select',
                  'label' => __('Text type (basic/advanced)', 'castor'),
                  'default' => 'basic',
                  'options' => array(
                    'basic' => __('Basic', 'castor'),
                    'advanced' => __('Advanced', 'castor')
                  ),
                  'toggle' => array(
                    'basic' => array(
                      'fields' => array('uconn_cwo_text')
                    ),
                    'advanced' => array(
                      'fields' => array('uconn_cwo_text_advanced')
                    )
                  )
                ),
                'uconn_cwo_text' => array(
                  'type' => 'text',
                  'label' => __('Text Content', 'castor'),
                  'default' => 'UConn',
                ),
                'uconn_cwo_text_advanced' => array(
                  'type' => 'editor',
                  'media_buttons' => false,
                  'wpautop' => true
                )
              )
            ),
            'section_2' => array(
              'title' => __('Text Settings', 'castor'),
              'fields' => array(
                'uconn_cwo_text_visibility' => array(
                  'type' => 'select',
                  'label' => __('Text visibility', 'castor'),
                  'default' => 'always',
                  'options' => array(
                    'always' => __('Always', 'castor'),
                    'hover' => __('Hover/focus only', 'castor'), 
                    'unique-hover' => __('On Load, with unique hover text', 'castor')
                  ),
                  'toggle' => array(
                    'unique-hover' => array(
                      'sections' => array('section_3')
                    )
                  )
                ),
                'uconn_cwo_font' => array(
                  'type' => 'font',
                  'label' => __('Font', 'castor'),
                  'default' => array(
                    'family' => 'Georgia',
                    'weight' => '400'
                  )
                ),
                'uconn_cwo_font_size' => array(
                  'type' => 'unit',
                  'default' => '20',
                  'label' => __('Font Size', 'castor'),
                  'description' => __('px', 'castor')
                ),
                'uconn_cwo_line_height' => array(
                  'type' => 'unit',
                  'default' => '1',
                  'label' => __('Line Height', 'castor')
                ),
                'uconn_cwo_text_shadow' => array(
                  'type' => 'button-group',
                  'label' => 'Text Shadow',
                  'default' => 'false',
                  'options' => array(
                    'false' => __('No', 'castor'),
                    'true' => __('Yes', 'castor')
                  )
                ),
                'uconn_cwo_text_horiz_pos' => array(
                  'type' => 'select',
                  'label' => __('Text Position - Horizontal', 'castor'),
                  'default' => 'center',
                  'options' => array(
                    'center' => __('Center', 'castor'),
                    'flex-start' => __('Left', 'castor'),
                    'flex-end' => __('Right', 'castor')
                  )
                ),
                'uconn_cwo_text_vert_pos' => array(
                  'type' => 'select',
                  'label' => __('Text Position - Vertical', 'castor'),
                  'default' => 'center',
                  'options' => array(
                    'center' => __('Center', 'castor'),
                    'flex-start' => __('Top', 'castor'),
                    'flex-end' => __('Bottom', 'castor')
                  )
                ),
                'uconn_cwo_text_margin' => array(
                  'type' => 'dimension',
                  'label' => __('Text Position Offset', 'castor'),
                  'description' => __('px', 'castor'),
                  'default' => '16',
                )
              )
            ),
            'section_3' => array(
              'title' => __('Hover Text Content & Settings', 'castor'),
              'fields' => array(
                'uconn_cwo_hover_text_type' => array(
                  'type' => 'select',
                  'label' => __('Text type (basic/advanced)', 'castor'),
                  'default' => 'basic',
                  'options' => array(
                    'basic' => __('Basic', 'castor'),
                    'advanced' => __('Advanced', 'castor')
                  ),
                  'toggle' => array(
                    'basic' => array(
                      'fields' => array('uconn_cwo_hover_text')
                    ),
                    'advanced' => array(
                      'fields' => array('uconn_cwo_hover_text_advanced')
                    )
                  )
                ),
                'uconn_cwo_hover_text' => array(
                  'type' => 'textarea',
                  'label' => __('Text Content', 'castor'),
                  'default' => 'UConn',
                ),
                'uconn_cwo_hover_text_advanced' => array(
                  'type' => 'editor',
                  'media_buttons' => false,
                  'wpautop' => true
                ), 
                'uconn_cwo_hover_font' => array(
                  'type' => 'font',
                  'label' => __('Font', 'castor'),
                  'default' => array(
                    'family' => 'Georgia',
                    'weight' => '400'
                  )
                ),
                'uconn_cwo_hover_font_size' => array(
                  'type' => 'unit',
                  'default' => '20',
                  'label' => __('Font Size', 'castor'),
                  'description' => __('px', 'castor')
                ),
                'uconn_cwo_hover_line_height' => array(
                  'type' => 'unit',
                  'default' => '1',
                  'label' => __('Line Height', 'castor')
                ),
                'uconn_cwo_hover_text_shadow' => array(
                  'type' => 'button-group',
                  'label' => 'Hover Text Shadow',
                  'default' => 'false',
                  'options' => array(
                    'false' => __('No', 'castor'),
                    'true' => __('Yes', 'castor')
                  )
                ),
                'uconn_cwo_hover_text_color' => array(
                  'type' => 'color',
                  'label' => __('Hover Text Color', 'castor'),
                  'default' => 'ffffff',
                  'show_reset' => true
                ), 
                'uconn_cwo_hover_text_alignment' => array(
                  'type' => 'select',
                  'label' => __('Hover Text Position - Horizontal', 'castor'),
                  'default' => 'center',
                  'options' => array(
                    'center' => __('Center', 'castor'),
                    'left' => __('Left', 'castor'),
                    'right' => __('Right', 'castor')
                  )
                ),
                'uconn_cwo_hover_text_vert_pos' => array(
                  'type' => 'select',
                  'label' => __('Hover Text Position - Vertical', 'castor'),
                  'default' => 'center',
                  'options' => array(
                    'center' => __('Center', 'castor'),
                    'flex-start' => __('Top', 'castor'),
                    'flex-end' => __('Bottom', 'castor')
                  )
                ),
                'uconn_cwo_hover_text_margin' => array(
                  'type' => 'dimension',
                  'label' => __('Hover Text Position Offset', 'castor'),
                  'description' => __('px', 'castor'),
                  'default' => '16',
                )
              )
            )
          )
        ),
        'tab_2' => array(
          'title' => __('Style', 'castor'),
          'sections' => array(
            'section_0' => array(
              'title' => __('Height', 'castor'),
              'fields' => array(
                'uconn_cwo_height' => array(
                  'type' => 'unit',
                  'label' => __('Height', 'castor'),
                  'units' => array('px', 'em', 'rem'),
                  'default' => '200',
                  'default_unit' => 'px'
                )
              )
            ),
            'section_1' => array(
              'title' => __('Colors', 'castor'),
              'fields' => array(
                'uconn_cwo_hover_color' => array(
                  'type' => 'color',
                  'label' => __('Hover Color', 'castor'),
                  'default' => 'ffffff',
                  'show_reset' => true
                ),
                'uconn_cwo_hover_color_opacity_start' => array(
                  'type' => 'unit',
                  'label' => __('Hover Opacity', 'castor'),
                  'default' => '0.5',
                  'description' => __('Set the starting opacity of the hover effect', 'castor')
                ),
                'uconn_cwo_hover_color_opacity_end' => array(
                  'type' => 'unit',
                  'label' => __('Hover Opacity', 'castor'),
                  'default' => '0',
                  'description' => __('Set the ending opacity of the hover effect', 'castor')
                ),
                'uconn_cwo_overlay_on_mobile' => array(
                  'type' => 'button-group',
                  'label' => 'Should the overlay be present on mobile, on page load?',
                  'default' => 'true',
                  'options' => array(
                    'true' => __('Yes', 'castor'),
                    'false' => __('No', 'castor')
                  )
                ),
                'uconn_cwo_text_color' => array(
                  'type' => 'color',
                  'label' => __('Text Color', 'castor'),
                  'default' => '000000',
                  'show_reset' => true
                ),
              )
            ),
            'section_3' => array(
              'title' => __('Icon', 'castor'),
              'fields' => array(
                'icon' => array(
                  'type' => 'icon',
                  'label' => __('Icon', 'castor'),
                  'show_remove' => true
                ),
                'icon_size' => array(
                  'type' => 'unit',
                  'label' => __('Icon Size', 'castor'),
                  'description' => __('px', 'castor'),
                  'default' => '30'
                ),
                'icon_background' => array(
                  'type' => 'color',
                  'label' => __('Icon Background Color', 'castor'),
                  'show_reset' => true
                ),
                'icon_background_opacity' => array(
                  'type' => 'unit',
                  'label' => __('Icon Background Opacity', 'castor'),
                  'description' => __('0.0 - 1.0', 'castor')
                ),
                'icon_background_size' => array(
                  'type' => 'dimension',
                  'label' => __('Icon Background Dimension', 'castor'),
                  'description' => __('px', 'castor'),
                  'default' => '0'
                ),
                'icon_position_horiz' => array(
                  'type' => 'select',
                  'label' => __('Icon Corner - Right/Left', 'castor'),
                  'defualt' => 'right',
                  'options' => array(
                    'right' => __('Right', 'castor'),
                    'left' => __('Left', 'castor')
                  )
                ),
                'icon_offset_horiz' => array(
                  'type' => 'unit',
                  'label' => __('Icon Offset - Horiz', 'castor'),
                  'description' => __('px', 'castor'),
                  'default' => '0'
                ),
                'icon_position_vert' => array(
                  'type' => 'select',
                  'label' => __('Icon Corner - Top/Bottom', 'castor'),
                  'defualt' => 'bottom',
                  'options' => array(
                    'bottom' => __('Bottom', 'castor'),
                    'top' => __('Top', 'castor')
                  )
                ),
                'icon_offset_vert' => array(
                  'type' => 'unit',
                  'label' => __('Icon Offset - Vert', 'castor'),
                  'description' => __('px', 'castor'),
                  'default' => '0'
                ),
              )
            ),
            'section_4' => array(
              'title' => __('Border Highlight', 'castor'),
              'fields' => array(
                'border_toggle' => array(
                  'type' => 'select',
                  'label' => __('Border Options', 'castor'),
                  'default' => 'false',
                  'options' => array(
                    'false' => __('No border', 'castor'),
                    'true' => __('Border Styles', 'castor')
                  ),
                  'toggle' => array(
                    'true' => array(
                      'sections' => array('section_5')
                    )
                  )
                )
              )
            ),
            'section_5' => array(
              'title' => __('Border Styles', 'castor'),
              'fields' => array(
                'border_color' => array(
                  'type' => 'color',
                  'label' => __('Border Color', 'castor'),
                  'default' => '000e2f',
                  'show_reset' => true
                ),
                'border_on_mobile' => array(
                  'type' => 'button-group',
                  'label' => 'Should the border be present on mobile, on page load?',
                  'default' => 'false',
                  'options' => array(
                    'true' => __('Yes', 'castor'),
                    'false' => __('No', 'castor')
                  )
                ),
                'border_position' => array(
                  'type' => 'select',
                  'label' => __('Border Position', 'castor'),
                  'default' => 'bottom',
                  'options' => array(
                    'bottom' => __('Bottom', 'castor'),
                    'top' => __('Top', 'castor')
                  )
                ),
                'border_height' => array(
                  'type' => 'unit',
                  'label' => __('Border Height', 'castor'),
                  'description' => __('px', 'castor'),
                  'default' => '8'
                ),
                'border_offset' => array(
                  'type' => 'unit',
                  'label' => __('Border Offset', 'castor'),
                  'description' => __('px', 'castor'),
                  'default' => '-4'
                ),
                'border_starting_scale' => array(
                  'type' => 'unit',
                  'label' => __('Border Starting Length', 'castor'),
                  'description' => __('% - Scale the starting length of the border on hover or focus', 'castor'),
                  'default' => '10'
                ),
                'border_final_scale' => array(
                  'type' => 'unit',
                  'label' => __('Border Final Length', 'castor'),
                  'description' => __('% - Scale the final length of the border on hover or focus', 'castor'),
                  'default' => '100'
                ),
                'border_trans_dur' => array(
                  'type' => 'unit',
                  'label' => __('Border Transition Duration', 'castor'),
                  'description' => __('seconds', 'castor'),
                  'default' => '0.3'
                ),
                'border_trans_func' => array(
                  'type' => 'select',
                  'label' => __('Border Transition Function', 'castor'),
                  'default' => 'ease',
                  'options' => array(
                    'ease' => __('ease', 'castor'),
                    'ease-in' => __('ease-in', 'castor'),
                    'ease-out' => __('ease-out', 'castor'),
                    'ease-in-out' => __('ease-in-out', 'castor'),
                    'linear' => __('linear', 'castor'),
                  )
                ),
              )
            )
          )
        )
      ));
    }
  }
}
