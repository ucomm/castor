<?php

class ContentWithOverlay extends FLBuilderModule {
  public function __construct() {
    parent::__construct(array(
      'name' => __('Content With Overlay', 'castor'),
      'description' => __('Display a box with background image or color and overlay effect', 'castor'),
      'category' => __('Castor Modules', 'castor'),
      'dir' => CASTOR_DIR . 'modules/content-with-overlay/',
      'url' => CASTOR_URL . 'modules/content-with-overlay/',
      'editor_export'   => true, // Defaults to true and can be omitted.
      'enabled'         => true, // Defaults to true and can be omitted.
      'partial_refresh' => false, // Defaults to false and can be omitted.
    ));
  }
}
