
$ = jQuery;

$('#locations-select').change(function(){
	
	var location = $('#locations-select option:selected').data();
	$('#location-address').text(location['locationAddress']);
	$('#location-hours').text(location['locationHours']);
	$('#location-phone').text(location['locationPhone']);
	
	var locationUrl = $('<a>', {'href':location['locationUrl']}).text(location['locationUrlText'])
	$('#location-url').empty().append(locationUrl);
	
});
