<?php

class LocationsModule extends Castor_Module {
	
    private $version = '1.0.0';
	
    public function load_module() {
		
        if (class_exists('FLBuilder')){
			
            require_once 'locations.php';
			
            FLBuilder::register_module('Locations', array(
                'settings' => array(
                    'title' => __('Locations', 'castor'),
                    'sections' => array(
                        'settings' => array(
                            'title' => __('Settings', 'castor'),
                            'fields' => array(
                                'locations' => array(
                                    'type' => 'form',
                                    'label' => __('Location', 'castor'),
									'form'	=> 'location_form',
									'preview_text' => 'name',
                                    'multiple' => true
                                )
                            )
                        )
                    )
                )
            ));
			
			FLBuilder::register_settings_form('location_form', array(
                'title' => __('Location Settings', 'castor'),
                'tabs' => array(
                    'locations' => array(
                        'title' => __('Location', 'castor'),
                        'sections' => array(
                            'location' => array(
                                'fields' => array(
                                    'name' => array(
                                        'type' => 'text',
                                        'label' => __('Name', 'castor')
                                    ),
                                    'address' => array(
                                        'type' => 'textarea',
                                        'label' => __('Address', 'castor'),
										'rows' => 3
                                    ),
                                    'hours' => array(
                                        'type' => 'textarea',
                                        'label' => __('Hours', 'castor'),
										'rows' => 3
                                    ),
                                    'phone' => array(
                                        'type' => 'textarea',
                                        'label' => __('Phone', 'castor'),
										'rows' => 3
                                    ),
                                    'url' => array(
                                        'type' => 'text',
                                        'label' => __('Google Maps Link URL', 'castor')
                                    ),
                                    'url_text' => array(
                                        'type' => 'text',
                                        'label' => __('Google Maps Link Text', 'castor')
                                    )
                                )
                            )
                        )
                    )
                )
            ));
			
		}
			
    }
	
}
