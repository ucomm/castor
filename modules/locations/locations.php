<?php

class Locations extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Locations', 'castor' ),
            'description'     => __( 'Displays locations.', 'castor' ),
            'category'        => __( 'Castor Modules', 'castor' ),
            'dir'             => CASTOR_DIR . 'modules/locations/',
            'url'             => CASTOR_URL . 'modules/locations/',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}