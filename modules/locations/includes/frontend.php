<?php if (!empty($settings->locations)): ?>
<div id="location">
	<div class="location-row">
		<div class="location-icon"><img src="<?php echo CASTOR_URL; ?>modules/locations/img/icon-location.png" /></div>
		<div class="location-item" id="location-address"><?php echo $settings->locations[0]->address; ?></div>
		<div class="clear"></div>
	</div>
	<div class="location-row">
		<div class="location-icon"><img src="<?php echo CASTOR_URL; ?>modules/locations/img/icon-hours.png" /></div>
		<div class="location-item" id="location-hours"><?php echo $settings->locations[0]->hours; ?></div>
		<div class="clear"></div>
	</div>
	<div class="location-row">
		<div class="location-icon"><img src="<?php echo CASTOR_URL; ?>modules/locations/img/icon-phone.png" /></div>
		<div class="location-item" id="location-phone"><?php echo $settings->locations[0]->phone; ?></div>
		<div class="clear"></div>
	</div>
<?php if (!empty($settings->locations[0]->url) && !empty($settings->locations[0]->url_text)): ?>
	<div class="location-row">
		<div class="location-item" id="location-url"><a href="<?php echo $settings->locations[0]->url; ?>"><?php echo $settings->locations[0]->url_text; ?></a></div>
		<div class="clear"></div>
	</div>
<?php endif; ?>
</div>
<div id="locations">
	<p>Update Directions with Service Line:</p>
	<select id="locations-select">
<?php foreach ($settings->locations as $location): ?>
	<option value="<?php echo $location->name; ?>"
			data-location-address="<?php echo $location->address; ?>"
			data-location-hours="<?php echo $location->hours; ?>"
			data-location-phone="<?php echo $location->phone; ?>"
			data-location-url="<?php echo $location->url; ?>"
			data-location-url-text="<?php echo $location->url_text; ?>"
	><?php echo $location->name; ?></option>
<?php endforeach; ?>
	</select>
</div>
<?php endif; ?>