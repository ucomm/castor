<?php

/**
 * Defines a base class for castor classes, including vital config data that we want to change cross-plugin.
 *
 * @since       1.0.0
 * @package     Castor
 * @subpackage  Castor/includes
 * @author      Brian Kelleher <bk@uconn.edu>
 */
class Castor_Base {

    const TABLE_NAME = 'castor_site_module';
    
}