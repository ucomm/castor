<?php

/**
 * Handles a castor main instance and configuration.
 *
 * @link       http://uconn.edu
 * @since      1.0.0
 *
 * @package    Castor
 * @subpackage Castor/includes
 */

/**
 * Handles a castor main instance and configuration.
 *
 * This class defines configuration code for a Castor module suite overarching instance.
 *
 * @since      1.0.0
 * @package    Castor
 * @subpackage Castor/includes
 * @author     Brian Kelleher <bk@uconn.edu>
 */
class Castor_Instance extends Castor_Base {

    /**
     * Whether the instance registered is multisite or not.
     *
     * @since    1.0.0
     * @access   private
     * @var      boolean    $multisite    True if the castor instance represents multisite, and false if not.
     */
    private $multisite;

    /**
     * Array of modules registered on the Castor site.
     *
     * @since   1.0.0
     * @access  private
     * @var     array       $modules    The registered modules and their respective class references.
     */
    private $modules;

    private $sites;

    /**
     * Initialize the castor site instance.
     *
     * @since    1.0.0
     */
    public function __construct() {
        $this->modules = array();
    }

    /**
     * Register a module to the castor system.
     *
     * @since   1.0.0
     * @param   string        $name   The string name reference for the module.
     * @param   Castor_Module $module The module which contains code for registering a beaver module.
     */
    public function register_module( $name, $module ) {
        $this->modules[$name] = $module;
    }

    public function load_modules() {
        foreach ( $this->modules as $mod ) {
            $mod->load_module();
        }
    }

}
