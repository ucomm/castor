<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://uconn.edu
 * @since      1.0.0
 *
 * @package    Castor
 * @subpackage Castor/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Castor
 * @subpackage Castor/includes
 * @author     Brian Kelleher <bk@uconn.edu>
 */
class Castor {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Castor_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * The registered module directory names to load custom modules from.
	 *
	 * @since	1.0.0
	 * @access	protected
	 * @var		array		$module_directories		The directories to load custom modules from.
	 */
	protected $module_directories;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'castor';
		$this->version = '1.0.38';
		$this->module_directories = glob(plugin_dir_path( dirname( __FILE__ ) ) . 'modules/*', GLOB_ONLYDIR);

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Castor_Loader. Orchestrates the hooks of the plugin.
	 * - Castor_i18n. Defines internationalization functionality.
	 * - Castor_Admin. Defines all hooks for the admin area.
	 * - Castor_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-castor-base.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-castor-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-castor-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-castor-admin.php';

		/**
		 * Castor core classes
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-castor-module.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-castor-instance.php';

		/**
		 * The class responsible for helper/utility functions to use in castor modules.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-castor-helpers.php';

		/**
		 * Load our Custom Modules here.
		 */
		foreach ( $this->module_directories as $mod ) {
			require_once "$mod/module.php";
		}

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-castor-public.php';

		$this->loader = new Castor_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Castor_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Castor_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}


	private function define_castor_modules( $instance ) {
		// Define our modules here.
		$breadcrumbs = new BreadcrumbsModule();
		$calendar = new UCCalendarModule();
		$content_with_overlay = new ContentWithOverlayModule();
		$circular_callout = new CircularCalloutModule();
		$custom_text = new CustomTextModule();
		$daily_status = new DailyStatusModule();
		$deadline_callout = new DeadlineCalloutModule();
		$random_hero_image = new RandomHeroImageModule();
		$hero_image_with_quote = new HeroImageWithQuoteModule();
		$icon_repeater = new IconRepeaterModule();
 		$location_callout = new LocationCalloutModule();
 		$locations = new LocationsModule();
		$physician_news = new PhysicianNewsModule();
		$search = new UCSuggestionSearchModule();
		$sidebar_navigation = new SidebarNavigationModule();
		$syntax_highlighter = new SyntaxHighlighterModule();
		$table = new CastorTableModule();
		$uconn_today = new UConnTodayModule();

		// Register Modules with Castor
		$instance->register_module('breadcrumbs', $breadcrumbs);
		$instance->register_module('calendar', $calendar);
		$instance->register_module('content-with-overlay', $content_with_overlay);
		$instance->register_module('circular-callout', $circular_callout);
		$instance->register_module('custom-text', $custom_text);
		$instance->register_module('daily-status', $daily_status);
		$instance->register_module('deadline-callout', $deadline_callout);
		$instance->register_module('random-hero-image', $random_hero_image);
		$instance->register_module('hero-image-with-quote', $hero_image_with_quote);
		$instance->register_module('icon-repeater', $icon_repeater);
		$instance->register_module('location-callout', $location_callout);
		$instance->register_module('locations', $locations);
		$instance->register_module('physician-news', $physician_news);
		$instance->register_module('uc-suggestion-search', $search);
		$instance->register_module('sidebar-navigation', $sidebar_navigation);
		$instance->register_module('syntax-highlighter', $syntax_highlighter);
		$instance->register_module('table', $table);
		$instance->register_module('uconn-today', $uconn_today);
		
		return $instance;
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Castor_Admin( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action('admin_menu', $plugin_admin, 'register_settings_page');

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Castor_Public( $this->get_plugin_name(), $this->get_version() );

		$castor_instance = new Castor_Instance();
		$castor_instance = $this->define_castor_modules( $castor_instance );

		$this->loader->add_action( 'init', $castor_instance, 'load_modules' );

		$this->loader->add_filter( 'fl_builder_module_categories', $plugin_public, 'add_castor_beaver_builder_module_category' );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Castor_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
