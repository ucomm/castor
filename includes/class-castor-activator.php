<?php

/**
 * Fired during plugin activation
 *
 * @link       http://uconn.edu
 * @since      1.0.0
 *
 * @package    Castor
 * @subpackage Castor/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Castor
 * @subpackage Castor/includes
 * @author     Brian Kelleher <bk@uconn.edu>
 */
class Castor_Activator extends Castor_Base {

	/**
	 * Fires when plugin activated
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
	}

}
