<?php
/*
*
*  A class to hold helper/utility functions.
* @link         http://uconn.edu
* @since        1.0.7
*
* @package      Castor
* @subpackage   Castor/includes
 */



class Castor_Helpers {
  /*
  *
  *    @since 1.0.7
  *    @param string  $hex   A string to convert from hex to rgb notation.
   */
  static public function convert_hex_to_rgb($hex) {
    list($r, $g, $b) = sscanf($hex, '%2x%2x%2x');
    echo "$r, $g, $b";
  }
  static public function get_bb_settings($settings_array) {
    $requested_settings_array = array();
    $settings_obj = FLBuilderModel::get_global_settings();
    foreach ($settings_array as $index => $setting) {
      $global_value = $settings_obj->$setting;
      $requested_settings_array[$setting] = $global_value;
    }
    return $requested_settings_array;
  }
  static public function set_current_page_class() {
    global $post;
    $class = '.current-menu-item';
    // echo "<pre>";
    // var_dump(is_page());
    // var_dump($post->post_parent);
    // echo "</pre>";
    // if (is_page() && $post->post_parent) {
    //   $class = '.current_page_item';
    // } else {
    //   $class = '.current-menu-parent';
    // }
    return $class;
  }
  /**
   * create_responsive_values sets ensures safe fallbacks.
   *
   * @param [type] $values an nested array of style values
   * expects $values to be a nested array of arrays with keys of
   *  - base
   *  - medium
   *  - responsive 
   * @return void
   */
  static public function create_responsive_values($values) {
      return array_map(function($value) {
          if (!$value['base']) {
                $value['base'] = '';
          }

          if (!$value['medium']) {
              $value['medium'] = $value['base'];
          }

          if (!$value['responsive'] && !$value['medium']) {
              $value['responsive'] = $value['base'];
          } elseif (!$value['responsive'] && $value['medium']) {
              $value['responsive'] = $value['medium'];
          }
          return $value;
      }, $values);
  }
}
