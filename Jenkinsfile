pipeline {
  agent any
  environment {
    PLUGIN_SLUG = "castor"
    // sometimes the project slug and repo slug are different
    BITBUCKET_SLUG = "castor"
    DEV_BRANCH = "develop"
    PROD_BRANCH = "master"
    FEATURE_BRANCH = "feature/*"
  }
  // handle rsync dry runs
  parameters {
    booleanParam(
      name: 'ADMISSIONS_RSYNC_DRY_RUN',
      defaultValue: true,
      description: 'Toggles the rsync --dry-run flag for admissions.uconn.edu'
    )
    booleanParam(
      name: 'RSYNC_DRY_RUN',
      defaultValue: true,
      description: 'Toggles the rsync --dry-run flag'
    )
  }
  stages {
    // checkout from git and skip if the "ci skip" message is present in the commit
    stage('Checkout') {
      steps {
        // do not delete builds. there's a breaking issue there
        // https://issues.jenkins.io/browse/JENKINS-66843
        scmSkip(deleteBuild: false)
        // relies on the global notification library
        // https://bitbucket.org/ucomm/jenkins-send-notifications/src/main/
        sendNotifications 'STARTED'
      }
      post {
        failure {
          sendNotifications("'ci skip' not present in commit")
        }
      }
    }
    stage("Composer") {
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          script {
            //  catch if composer down
            try{
              sh "${WORKSPACE}/ci-scripts/composer.sh"
            } catch (err) {
              echo "failure - $err"
            }
          }
        }
      }
    }
    stage('Dev Pushes') {
      // set environment variables as needed
      environment {
        FILENAME = "$GIT_BRANCH"
      }
      when {
        anyOf {
          branch "${FEATURE_BRANCH}";
          branch "${DEV_BRANCH}"
        }
      }
      parallel {
        stage('Push to staging0') {
            // config files need to be accessed by their IDs. these can be found in Manage Jenkins > Config Files area
            // the ID listed below is the one for the staging-plugin-paths file
            steps {
              catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
                script{
                  //  catch if pushing to staging0 fails
                  try {
                    configFileProvider([configFile(fileId: '723952ef-c9a9-4894-b153-460f3d5afcda', targetLocation: './plugin-paths.sh')]) {
                        sh '${WORKSPACE}/ci-scripts/multisite-dev-push.sh'
                    }
                  } catch (err) {
                    echo "failure - $err"
                  }
                }
              }
            }
        }
        stage('Archive dev to bitbucket') {
          steps {
            //  catch if arching dev fails
            catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
              script {
                try {
                  sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
                } catch (err) {
                  echo "failure - $err"
                }
              }
            }
          }
        }
      }
      post {
        failure {
          sendNotifications "failure - dev push"
        }
      }
    }
    stage('Aurora Sandbox Push') {
      when {
        anyOf {
          branch "${FEATURE_BRANCH}";
          branch "${DEV_BRANCH}"
        }
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          script {
            //  catch if unable to push to aurora sandbox
            try{
              sh "${WORKSPACE}/ci-scripts/aurora-sandbox-push.sh"
            } catch (err) {
              echo "failure - $err"
            }
          }
        }
      }
      post {
        failure {
          sendNotifications "AURORA SANDBOX PUSH FAILED"
        }
      }
    }
    stage('Prod Push') {
      when {
        branch "${PROD_BRANCH}"
      }
      steps {
        //  catch if prod push fails
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          script{
            try {
            // this ID is the one for the prod plugin paths
              configFileProvider([configFile(fileId: 'bc2af336-9f19-4b43-852c-7dc672293373', targetLocation: './plugin_paths.sh')]) {
                // use a prod push script similar to the example. 
                sh "${WORKSPACE}/ci-scripts/multisite-prod-push.sh"
              }
            } catch (err) {
              echo "failure - $err"
            }
          }
        }
      }
      post {
        success {
          // NB - don't use script blocks very much.
          // I'm just adding this so th comm0-updates channel doesn't get flooded on dry runs
          script {
            if (env.RSYNC_DRY_RUN == "false") {
              sendNotifications("SUCCESS", "#comm0-updates", [
                [
                  type: "section",
                  text: [
                    type: "mrkdwn",
                    text: ":tada: *$PLUGIN_SLUG* updated"
                  ]              
                ]
              ])
              slackUploadFile(channel: "#comm0-updates", filePath: "changelog.md", initialComment:  "${PLUGIN_SLUG} Changelog")
            }
          }
        }
      }
    }
    // only push tagged releases to bitbucket
    stage('Archive tag to bitbucket') {
      environment {
        FILENAME = "$TAG_NAME"
      }
      when {
        buildingTag()
      }
      steps {
        //  catch if archiving tag fails
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          script{
            try {
              sh "${WORKSPACE}/ci-scripts/bitbucket-archive.sh"
            } catch (err) {
              echo "failure - $err"
            }
          }
        }
      }
    }
  }
  post {
    // send slack notifications when the project finishes
    success {
      sendNotifications 'SUCCESSFUL'
    }
    failure {
      sendNotifications 'FAILED'
    }
    always {
      echo "======== Cleanup ========"
      sh "rm -rf node_modules"
    }
  }
}