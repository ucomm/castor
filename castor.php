<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://uconn.edu
 * @since             1.9.1
 * @package           Castor
 *
 * @wordpress-plugin
 * Plugin Name:       Castor
 * Plugin URI:        http://uconn.edu
 * Description:       A suite of beaver builder and other helpful modules developed by the UConn Communications Web Team.
 * Version:           1.9.1
 * Author:            UConn Communications
 * Author URI:        http://uconn.edu
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       castor
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'CASTOR_DIR', plugin_dir_path(__FILE__) );
define( 'CASTOR_URL', plugins_url('/', __FILE__) );

// Define the path and url to custom fields. 
define ( 'FIELDS_DIR', plugin_dir_path(__FILE__) );
define ( 'FIELDS_URL', plugins_url('/', __FILE__) );

/**
 * Import the core Castor base for plugin-wide config.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-castor-base.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-castor-activator.php
 */
function activate_castor() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-castor-activator.php';
	Castor_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-castor-deactivator.php
 */
function deactivate_castor() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-castor-deactivator.php';
	Castor_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_castor' );
register_deactivation_hook( __FILE__, 'deactivate_castor' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-castor.php';

function is_multisite_super_admin_user() {
	return is_multisite() && is_super_admin();
}

function is_admin_user() {
	return !is_multisite() && current_user_can('edit_theme_options');
}
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_castor() {

	$plugin = new Castor();
	$plugin->run();

}
run_castor();
